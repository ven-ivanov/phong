var disclosure = require('../standardObjects/contributordisclosure.js');
var genereateResponse = require('../standardObjects/responseFormat.js');
//Invite contributors
module.exports = {
    path: '/PortalServiceV2/contributor/disclosure/:contributorId',
    callback: function( req, res) {
        //return params as is now
        var status = "ok";
        var version = "1.0";
        var reason = 200;
        setTimeout(function(){
          return res.json({
              status: status,
              version: version,
              ts: Date().now,
              reason : reason,
              data: disclosure,
              totalCount: disclosure.length
          });
        },3000);
    }
}
