module.exports = {
    genereateResponse: function(request, response, createEntity) {
        var perPage = request.query.perPage? request.query.perPage: 10;
        var page    = request.query.page? request.query.page: 1;
        var resData = [];

        for (var i = 1; i <= perPage; i ++) {
            resData.push(createEntity());
        }
        var status = (resData.length >0)? "ok" : "failed";
        var reason = null;
        var resourceName = 'test';
        return response.json({
            status: status,
            ts: Date().now,
            reason : reason,
            resourceName: resData,
            totalCount: resData.length
        });
    }
};
