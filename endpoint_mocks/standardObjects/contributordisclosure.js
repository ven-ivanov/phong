'use strict';

var contributorDisclosure = {
  questionOne:{
    hasQuestion: true,
    questions:[
      {
        organizationName: 'Great Western',
        grant            : true,
        paidEmployee    : true,
        personalFee     : true,
        other           : true,
        startDate       : '22/03/2015',
        endDate         : '25/03/2015',
        comment         : 'Spoke at Annual Sales Conference 2004',
        updateDate      : '26/03/2015'
      },
      {
        organizationName: 'Lesser Western',
        grant            : true,
        paidEmployee    : true,
        personalFee     : true,
        other           : true,
        startDate       : '22/03/2015',
        endDate         : '25/03/2015',
        comment         : 'Research endowment for pepticulcer study Aug 2015',
        updateDate      : '26/03/2015'
      },
    ]
  },
  questionTwo:{
    hasQuestion: true,
    questions:[
      {
        organizationName: 'Great Western',
        typeOfRelationship : 'Employment',
        startDate       : '02/01/2012',
        endDate         : '25/03/2015',
        comment         : 'Currently an employee as Research team leader therapeutics division at Middle Northern',
        updateDate      : '09/01/2015'
      },
      {
        organizationName: 'Lesser Western',
        type            : 'Fee',
        startDate       : '22/03/2015',
        endDate         : '25/03/2015',
        comment         : 'Research endowment for',
        updateDate      : '26/03/2015'
      },
    ]
  },
  questionThree:{
    hasQuestion: true,
    questions:[
      {
        patentNumber    : 'US Pat No 23456789',
        pending         : false,
        issued          : true,
        licensed        : true,
        licensee        : false,
        royalties       : false,
        comment         : 'This concerns plasma storage',
        updateDate      : '26/03/2015'
      },
      {
        patentNumber    : 'US Pat No 345435645',
        pending         : false,
        issued          : true,
        licensed        : true,
        licensee        : false,
        royalties       : false,
        comment         : 'This concerns plasma storge',
        updateDate      : '26/03/2015'
      },
    ]
  },
  questionFour:{
    hasQuestion: true,
    questions:[
      {
        comment         : 'Shareholder of Inner Southern Inc',
        updateDate      : '26/03/2015'
      },
      {
        comment         : 'Brother is president of Istra Zonica',
        updateDate      : '26/03/2015'
      }
    ]
  }
};
module.exports = contributorDisclosure;
