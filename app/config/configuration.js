var profmedLearn = {
    "version":"00.90.001-16004",
    "copyright":"&copy; 2014-2015 Roundpoint Inc",
    "learn":{
        "messageStore":{
            "welcomeMessages":{
                "headline":"Welcome, {user.firstName}",
                "bookmarkMessage":"Don't forget to bookmark this page and save it to your home screen for easy access!"
            },
            "generalMessages": {
                "loading": "Loading, please wait..."
            },
            "errorMessages": {
                "serviceFailed": "We're sorry, the page {pageName} you are trying to load failed..."
            },
            "aboutMessages": {
                "title":"About",
                "text":"ya...",
                "version":"Version {application.version}.",
                "url":"about.html"
            }
        },
        "splashPageDelay":200,
        "organisationTitle":"NowCE",
        "organisationAuthentication":true,
        "footer":{
            "template":"",
            "cssClass":"footer",
            "vendor":"NowCE"
        },
        "welcomeViewConfiguration":{
            "WelcomePageContainer":{

            }
        },
        "layoutSizes":[
            { "name":"small", "min":0, "max":900, "callbackMethod":"assembleSmallLayout" },
            { "name":"large", "min":900, "max":5000, "callbackMethod":"assembleLargeLayout" }
        ]
    },
    "owner":{
        "messageStore":{
            "welcomeMessages":{
                "headline":"Welcome, {user.firstName}",
                "bookmarkMessage":"Don't forget to bookmark this page and save it to your home screen for easy access!"
            },
            "generalMessages": {
                "loading": "Loading, please wait..."
            },
            "errorMessages": {
                "serviceFailed": "We're sorry, the page {pageName} you are trying to load failed..."
            },
            "aboutMessages": {
                "title":"About",
                "text":"ya...",
                "version":"Version {application.version}.",
                "url":"about.html"
            }
        },
        "splashPageDelay":200,
        "organisationTitle":"Owner",
        "organisationAuthentication":true,
        "documentTitle":"Profmed Bookreader",
        "footer":{
            "template":"",
            "cssClass":"footer",
            "vendor":"ProfMed /Question Editor"
        },
        "welcomeViewConfiguration":{
            "WelcomePageContainer":{

            }
        },
        "layoutSizes":[
            { "name":"small", "min":0, "max":900, "callbackMethod":"assembleSmallLayout" },
            { "name":"large", "min":900, "max":5000, "callbackMethod":"assembleLargeLayout" }
        ]
    },
    "user":{
        "messageStore":{
            "welcomeMessages":{
                "headline":"Welcome, {user.firstName}",
                "bookmarkMessage":"Don't forget to bookmark this page and save it to your home screen for easy access!"
            },
            "generalMessages": {
                "loading": "Loading, please wait..."
            },
            "errorMessages": {
                "serviceFailed": "We're sorry, the page {pageName} you are trying to load failed..."
            },
            "aboutMessages": {
                "title":"About",
                "text":"ya...",
                "version":"Version {application.version}.",
                "url":"about.html"
            }
        },
        "splashPageDelay":200,
        "organisationTitle":"User",
        "organisationAuthentication":true,
        "documentTitle":"Profmed Bookreader",
        "footer":{
            "template":"",
            "cssClass":"footer",
            "vendor":"ProfMed /Question Editor"
        },
        "welcomeViewConfiguration":{
            "WelcomePageContainer":{

            }
        },
        "layoutSizes":[
            { "name":"small", "min":0, "max":900, "callbackMethod":"assembleSmallLayout" },
            { "name":"large", "min":900, "max":5000, "callbackMethod":"assembleLargeLayout" }
        ],
        "specialities":[{
            "name": "Allergy and Immunology",
            "id": 1
        }, {
            "name": "Anesthesiology",
            "id": 2
        }, {
            "name": "Colon and Rectal Surgery",
            "id": 3
        }, {
            "name": "Dermatology",
            "id": 4
        }, {
            "name": "Emergency Medicine",
            "id": 5
        }, {
            "name": "Family Medicine",
            "id": 6
        }, {
            "name": "Internal Medicine",
            "id": 7
        }, {
            "name": "Learning support and teaching initiatives",
            "id": 8
        }, {
            "name": "Medical Genetics and Genomics",
            "id": 9
        }, {
            "name": "Neurological Surgery",
            "id": 10
        }, {
            "name": "Nuclear Medicine",
            "id": 11
        }, {
            "name": "Obstetrics and Gynecology",
            "id": 12
        }, {
            "name": "Ophthalmology",
            "id": 13
        }, {
            "name": "Orthopaedic Surgery",
            "id": 14
        }, {
            "name": "Otolaryngology",
            "id": 15
        }, {
            "name": "Pathology",
            "id": 16
        }, {
            "name": "Pediatrics",
            "id": 17
        }, {
            "name": "Physical Medicine and Rehabilitation",
            "id": 18
        }, {
            "name": "Plastic Surgery",
            "id": 19
        }, {
            "name": "Preventive Medicine",
            "id": 20
        }, {
            "name": "Psychiatry and Neurology",
            "id": 21
        }, {
            "name": "Radiology",
            "id": 22
        }, {
            "name": "Surgery",
            "id": 23
        }, {
            "name": "Thoracic Surgery",
            "id": 24
        }, {
            "name": "Urology",
            "id": 25
        }],
        "countries":[{
                "name": "USA",
                "id": 1
            },
            {
                "name": "Canada",
                "id": 2
            },
            {
                "name": "Afghanistan",
                "id": 3
            },
            {
                "name": "Albania",
                "id": 3
            },
            {
                "name": "Algeria",
                "id": 3
            },
            {
                "name": "Andorra",
                "id": 3
            },
            {
                "name": "Angola",
                "id": 3
            },
            {
                "name": "Argentina",
                "id": 3
            },
            {
                "name": "Armenia",
                "id": 3
            },
            {
                "name": "Australia",
                "id": 3
            },
            {
                "name": "Austria",
                "id": 3
            },
            {
                "name": "Azerbaijan",
                "id": 3
            },
            {
                "name": "Bahamas",
                "id": 3
            },
            {
                "name": "Bahrain",
                "id": 3
            },
            {
                "name": "Bangladesh",
                "id": 3
            },
            {
                "name": "Barbados",
                "id": 3
            },
            {
                "name": "Belarus",
                "id": 3
            },
            {
                "name": "Belgium",
                "id": 3
            },
            {
                "name": "Belize",
                "id": 3
            },
            {
                "name": "Benin",
                "id": 3
            },
            {
                "name": "Bhutan",
                "id": 3
            },
            {
                "name": "Bolivia",
                "id": 3
            },
            {
                "name": "Bosnia and Herzegovina",
                "id": 3
            },
            {
                "name": "Botswana",
                "id": 3
            },
            {
                "name": "Brazil",
                "id": 3
            },
            {
                "name": "Brunei",
                "id": 3
            },
            {
                "name": "Bulgaria",
                "id": 3
            },
            {
                "name": "Burkina",
                "id": 3
            },

            {
                "name": "Burundi",
                "id": 3
            },
            {
                "name": "Cambodia",
                "id": 3
            },
            {
                "name": "Cameroon",
                "id": 3
            },
            {
                "name": "Canada",
                "id": 3
            },
            {
                "name": "Cape Verde",
                "id": 3
            },
            {
                "name": "Central African Republic",
                "id": 3
            },
            {
                "name": "Chad",
                "id": 3
            },
            {
                "name": "Chile",
                "id": 3
            },
            {
                "name": "China",
                "id": 3
            },
            {
                "name": "Colombia",
                "id": 3
            },
            {
                "name": "Comoros",
                "id": 3
            },
            {
                "name": "Congo, Republic of",
                "id": 3
            },
            {
                "name": "Congo, Democratic Republic of",
                "id": 3
            },
            {
                "name": "Cote d/'Ivoire",
                "id": 3
            },
            {
                "name": "Cyprus",
                "id": 3
            },
            {
                "name": "Czech Republic",
                "id": 3
            },
            {
                "name": "Denmark",
                "id": 3
            },
            {
                "name": "Djibouti",
                "id": 3
            },
            {
                "name": "Dominica",
                "id": 3
            },
            {
                "name": "East Timor",
                "id": 3
            },
            {
                "name": "Ecuador",
                "id": 3
            },
            {
                "name": "Egypt",
                "id": 3
            },
            {
                "name": "El Salvador",
                "id": 3
            },
            {
                "name": "Equatorial Guinea",
                "id": 3
            },
            {
                "name": "Eritrea",
                "id": 3
            },
            {
                "name": "Estonia",
                "id": 3
            },
            {
                "name": "Ethiopia",
                "id": 3
            },
            {
                "name": "Fiji",
                "id": 3
            },
            {
                "name": "Finland",
                "id": 3
            },
            {
                "name": "France",
                "id": 3
            },
            {
                "name": "Gabon",
                "id": 3
            },
            {
                "name": "Gambia",
                "id": 3
            },
            {
                "name": "Georgia",
                "id": 3
            },
            {
                "name": "Germany",
                "id": 3
            },
            {
                "name": "Ghana",
                "id": 3
            },
            {
                "name": "Greece",
                "id": 3
            },
            {
                "name": "Guatemala",
                "id": 3
            },
            {
                "name": "Guinea",
                "id": 3
            },
            {
                "name": "Guinea-Bissau",
                "id": 3
            },
            {
                "name": "Guyana",
                "id": 3
            },
            {
                "name": "Haiti",
                "id": 3
            },
            {
                "name": "Honduras",
                "id": 3
            },
            {
                "name": "Hungary",
                "id": 3
            },
            {
                "name": "Iceland",
                "id": 3
            },
            {
                "name": "India",
                "id": 3
            },
            {
                "name": "Indonesia",
                "id": 3
            },
            {
                "name": "Iran",
                "id": 3
            },
            {
                "name": "Iraq",
                "id": 3
            },
            {
                "name": "Ireland",
                "id": 3
            },
            {
                "name": "Israel",
                "id": 3
            },
            {
                "name": "Italy",
                "id": 3
            },
            {
                "name": "Jamaica",
                "id": 3
            },
            {
                "name": "Japan",
                "id": 3
            },
            {
                "name": "Jordan",
                "id": 3
            },
            {
                "name": "Kenya",
                "id": 3
            },
            {
                "name": "Kiribati",
                "id": 3
            },
            {
                "name": "Korea, North",
                "id": 3
            },
            {
                "name": "Korea, South",
                "id": 3
            },
            {
                "name": "Kosovo",
                "id": 3
            },
            {
                "name": "Kuwait",
                "id": 3
            },
            {
                "name": "Kyrgyzstan",
                "id": 3
            },
            {
                "name": "Laos",
                "id": 3
            },
            {
                "name": "Latvia",
                "id": 3
            },
            {
                "name": "Lebanon",
                "id": 3
            },

            {
                "name": "Lesotho",
                "id": 3
            },
            {
                "name": "Liberia",
                "id": 3
            },
            {
                "name": "Libya",
                "id": 3
            },
            {
                "name": "Liechtenstein",
                "id": 3
            },
            {
                "name": "Lithuania",
                "id": 3
            },
            {
                "name": "Luxembourg",
                "id": 3
            },
            {
                "name": "Macedonia",
                "id": 3
            },
            {
                "name": "Madagascar",
                "id": 3
            },
            {
                "name": "Malawi",
                "id": 3
            },
            {
                "name": "Malaysia",
                "id": 3
            },
            {
                "name": "Maldives",
                "id": 3
            },
            {
                "name": "Malta",
                "id": 3
            },
            {
                "name": "Marshall Islands",
                "id": 3
            },
            {
                "name": "Mauritius",
                "id": 3
            },
            {
                "name": "Mexico",
                "id": 3
            },
            {
                "name": "Micronesia",
                "id": 3
            },
            {
                "name": "Moldova",
                "id": 3
            },
            {
                "name": "Monaco",
                "id": 3
            },
            {
                "name": "Mongolia",
                "id": 3
            },
            {
                "name": "Montenegro",
                "id": 3
            },
            {
                "name": "Mozambique",
                "id": 3
            },
            {
                "name": "Myanmar",
                "id": 3
            },

            {
                "name": "Namibia",
                "id": 3
            },
            {
                "name": "Nauru",
                "id": 3
            },
            {
                "name": "Nepal",
                "id": 3
            },
            {
                "name": "New Zealand",
                "id": 3
            },
            {
                "name": "Niger",
                "id": 3
            },
            {
                "name": "Nigeria",
                "id": 3
            },
            {
                "name": "Norway",
                "id": 3
            },
            {
                "name": "Oman",
                "id": 3
            },
            {
                "name": "Pakistan",
                "id": 3
            },
            {
                "name": "Panama",
                "id": 3
            },
            {
                "name": "Papua New Guinea",
                "id": 3
            },
            {
                "name": "Paraguay",
                "id": 3
            },
            {
                "name": "Peru",
                "id": 3
            },
            {
                "name": "Poland",
                "id": 3
            },
            {
                "name": "Portugal",
                "id": 3
            },
            {
                "name": "Qatar",
                "id": 3
            },
            {
                "name": "Romania",
                "id": 3
            },
            {
                "name": "Russia",
                "id": 3
            },
            {
                "name": "Rwanda",
                "id": 3
            },
            {
                "name": "Saint Kitts and Nevis",
                "id": 3
            },
            {
                "name": "Saint Lucia",
                "id": 3
            },
            {
                "name": "Saint Vincent and the Grenadines",
                "id": 3
            },
            {
                "name": "Samoa",
                "id": 3
            },
            {
                "name": "San Marino",
                "id": 3
            },
            {
                "name": "Sao Tome and Principe",
                "id": 3
            },
            {
                "name": "Portugal",
                "id": 3
            },
            {
                "name": "Qatar",
                "id": 3
            },
            {
                "name": "Romania",
                "id": 3
            },
            {
                "name": "Russia",
                "id": 3
            },
            {
                "name": "Rwanda",
                "id": 3
            },
            {
                "name": "Saint Kitts and Nevis",
                "id": 3
            },
            {
                "name": "Saint Lucia",
                "id": 3
            },
            {
                "name": "Saint Vincent and the Grenadines",
                "id": 3
            },
            {
                "name": "Samoa",
                "id": 3
            },
            {
                "name": "San Marino",
                "id": 3
            },
            {
                "name": "Sao Tome and Principe",
                "id": 3
            },
            {
                "name": "Saudi Arabia",
                "id": 3
            },
            {
                "name": "Senegal",
                "id": 3
            },
            {
                "name": "Serbia",
                "id": 3
            },
            {
                "name": "Seychelles",
                "id": 3
            },
            {
                "name": "Sierra Leone",
                "id": 3
            },
            {
                "name": "Singapore",
                "id": 3
            },
            {
                "name": "Slovakia",
                "id": 3
            },
            {
                "name": "Slovenia",
                "id": 3
            },
            {
                "name": "Solomon Islands",
                "id": 3
            },
            {
                "name": "Somalia",
                "id": 3
            },
            {
                "name": "South Africa",
                "id": 3
            },
            {
                "name": "Spain",
                "id": 3
            },
            {
                "name": "Sri Lanka",
                "id": 3
            },
            {
                "name": "Sudan",
                "id": 3
            },
            {
                "name": "Suriname",
                "id": 3
            },
            {
                "name": "Swaziland",
                "id": 3
            },
            {
                "name": "Sweden",
                "id": 3
            },
            {
                "name": "Switzerland",
                "id": 3
            },
            {
                "name": "Syria",
                "id": 3
            },
            {
                "name": "Taiwan",
                "id": 3
            },
            {
                "name": "Tajikistan",
                "id": 3
            },
            {
                "name": "Tanzania",
                "id": 3
            },
            {
                "name": "Tanzania",
                "id": 3
            },
            {
                "name": "Thailand",
                "id": 3
            },
            {
                "name": "Togo",
                "id": 3
            },
            {
                "name": "Tonga",
                "id": 3
            },
            {
                "name": "Trinidad and Tobago",
                "id": 3
            },
            {
                "name": "Tunisia",
                "id": 3
            },
            {
                "name": "Turkey",
                "id": 3
            },
            {
                "name": "Turkmenistan",
                "id": 3
            },
            {
                "name": "Tuvalu",
                "id": 3
            },
            {
                "name": "Uganda",
                "id": 3
            },
            {
                "name": "Ukraine",
                "id": 3
            },
            {
                "name": "United Arab Emirates",
                "id": 3
            },
            {
                "name": "United Kingdom",
                "id": 3
            },
            {
                "name": "Uruguay",
                "id": 3
            },
            {
                "name": "Uzbekistan",
                "id": 3
            },
            {
                "name": "Vanuatu",
                "id": 3
            },
            {
                "name": "Vatican City",
                "id": 3
            },
            {
                "name": "Turkmenistan",
                "id": 3
            },
            {
                "name": "Venezuela",
                "id": 3
            },
            {
                "name": "Vietnam",
                "id": 3
            },
            {
                "name": "Yemen",
                "id": 3
            },
            {
                "name": "Zambia",
                "id": 3
            },
            {
                "name": "Zimbabwe",
                "id": 3
            }],
        "states":[{
            "name": "Alabama",
            "id": 1,
            "countryId": 1
        }, {
            "name": "Alaska",
            "id": 2,
            "countryId": 1
        }, {
            "name": "Arizona",
            "id": 3,
            "countryId": 1
        }, {
            "name": "Arkansas",
            "id": 4,
            "countryId": 1
        }, {
            "name": "California",
            "id": 5,
            "countryId": 1
        }, {
            "name": "Colorado",
            "id": 6,
            "countryId": 1
        }, {
            "name": "Connecticut",
            "id": 7,
            "countryId": 1
        }, {
            "name": "Delaware",
            "id": 8,
            "countryId": 1
        }, {
            "name": "Florida",
            "id": 9,
            "countryId": 1
        }, {
            "name": "Georgia",
            "id": 10,
            "countryId": 1
        }, {
            "name": "Hawaii",
            "id": 11,
            "countryId": 1
        }, {
            "name": "Idaho",
            "id": 12,
            "countryId": 1
        }, {
            "name": "Illinois",
            "id": 13,
            "countryId": 1
        }, {
            "name": "Indiana",
            "id": 14,
            "countryId": 1
        }, {
            "name": "Iowa",
            "id": 15,
            "countryId": 1
        }, {
            "name": "Kansas",
            "id": 16,
            "countryId": 1
        }, {
            "name": "Kentucky",
            "id": 17,
            "countryId": 1
        }, {
            "name": "Louisiana",
            "id": 18,
            "countryId": 1
        }, {
            "name": "Maine",
            "id": 19,
            "countryId": 1
        }, {
            "name": "Maryland",
            "id": 20,
            "countryId": 1
        }, {
            "name": "Massachusetts",
            "id": 21,
            "countryId": 1
        }, {
            "name": "Michigan",
            "id": 22,
            "countryId": 1
        }, {
            "name": "Minnesota",
            "id": 23,
            "countryId": 1
        }, {
            "name": "Mississippi",
            "id": 24,
            "countryId": 1
        }, {
            "name": "Missouri",
            "id": 25,
            "countryId": 1
        }, {
            "name": "Montana",
            "id": 26,
            "countryId": 1
        }, {
            "name": "Nebraska",
            "id": 27,
            "countryId": 1
        }, {
            "name": "Nevada",
            "id": 28,
            "countryId": 1
        }, {
            "name": "New Hampshire",
            "id": 29,
            "countryId": 1
        }, {
            "name": "New Jersey",
            "id": 30,
            "countryId": 1
        }, {
            "name": "New Mexico",
            "id": 31,
            "countryId": 1
        }, {
            "name": "New York ",
            "id": 32,
            "countryId": 1
        }, {
            "name": "North Carolina",
            "id": 33,
            "countryId": 1
        }, {
            "name": "North Dakota",
            "id": 34,
            "countryId": 1
        }, {
            "name": "Ohio",
            "id": 35,
            "countryId": 1
        }, {
            "name": "Oklahoma",
            "id": 36,
            "countryId": 1
        }, {
            "name": "Oregon",
            "id": 37,
            "countryId": 1
        }, {
            "name": "Pennsylvania",
            "id": 38,
            "countryId": 1
        }, {
            "name": "Rhode Island",
            "id": 39,
            "countryId": 1
        }, {
            "name": "South Carolina",
            "id": 40,
            "countryId": 1
        }, {
            "name": "South Dakota",
            "id": 41,
            "countryId": 1
        }, {
            "name": "Tennessee",
            "id": 42,
            "countryId": 1
        }, {
            "name": "Texas",
            "id": 43,
            "countryId": 1
        }, {
            "name": "Utah",
            "id": 44,
            "countryId": 1
        }, {
            "name": "Vermont",
            "id": 45,
            "countryId": 1
        }, {
            "name": "Virginia",
            "id": 46,
            "countryId": 1
        }, {
            "name": "Washington",
            "id": 47,
            "countryId": 1
        }, {
            "name": "West Virginia",
            "id": 48,
            "countryId": 1
        }, {
            "name": "Wisconsin ",
            "id": 49,
            "countryId": 1
        }, {
            "name": "Wyoming",
            "id": 51,
            "countryId": 1
        }, {
            "name": "Alberta",
            "id": 52,
            "countryId": 2
        }, {
            "name": "British columbia",
            "id": 53,
            "countryId": 2
        }, {
            "name": "N/A",
            "id": 54,
            "countryId": 3
        }]
    }
}

