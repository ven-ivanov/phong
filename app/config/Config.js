/**
 *
 * @created: 7/16/14 8:13 PM
 * @author: bh@roundpoint.com
 */

var Config = function(env) {
    'use strict';

    _.extend(this, window.deploymentConfig);

    if (!env) {
        env = 'development';
    }

    var origin = [ location.protocol, '//', location.host, location.pathname ].join('').replace('/index.html', ''),
        vendor = this.vendor || 'profmed';

    // TODO: remove necessity of having extra files for each vendor
    if(_.contains(['apsa', 'aacc'], vendor)) {
        this.vendor = vendor = 'nowce';
    }

    // make this available globally to enable console debugging
    if (window) {
        window.config = this;
    }

    // set the environment name
    this.environment = env;

    // set the version number
    //this.version = '00.90.001-16001';

    this.appItemKey = [ vendor, 'Portal' ].join('');

    // set the copyright notice
    this.copyright = '2014-2015 Roundpoint Inc';


    // set the vendor to control remote configuration
    this.vendorKey = vendor;

    this.host = [ location.protocol, '//', location.host ].join('');

    this.dataHostURL = this.host;
    this.dataURI = this.portalDataURI || '/PortalServiceV2';
    this.analyticsHostURL = this.host;
    this.analyticsURI = this.analyticsDataURI || '/ProfmedAnalyticsService';

    // point to the remote configuration url
    this.configurationHostURL = origin.replace('app', 'config');
    if (this.configurationHostURL.indexOf('config') < 0) {
        this.configurationHostURL = [ origin, 'config/' ].join('');
    }

    // the page title for this application
    this.pageTitle = 'ProfMed Portal';

    var _gaq = [];  // default/fallback codes for development mode analytics
    _gaq.push(['_setAccount', 'UA-45078417-2' ]);
    _gaq.push(['_setDomainName', 'none']);
    _gaq.push(['_setAllowLinker', true]);
    _gaq.push(['_trackPageview']);

    // set default if not set by deployment config
    if(!this._gaq) {
        this._gaq = _gaq;
    }

    // this is the default access pathway
    this.baseuri = '/gateway';

    // this is the provider access pathway
    this.ownerPath = '/owner/';

    // this is the provider users access pathway
    this.userPath = '/user/';

    // this is the trial subdomain for uses who wish to try the service it has a special routing consequence it is
    // used to provide an exception path
    this.trialSubdomain = 'trial';

};

Config.development = function() {
    "use strict";
    var config = new Config('development');

    return config;
};

Config.staging = function() {
    'use strict';
    var config = new Config('staging');

    return config;
};

Config.test = function() {
    "use strict";
    var config = new Config('test');

    return config;
};

Config.production = function() {
    "use strict";
    var config = new Config('production');

    return config;
};


