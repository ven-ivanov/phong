# common styles for profmed frontend projects

this repo is supposed to be used as a submodule in profmed browser clients. This enables sharing style code among all front-end projects.

## rules

changes to styles should be made in master branch of styles repo, be careful not to commit in DETACHED HEAD state

changes to styles should be always made in a way that it does not break any other projects.

testing the new styles with all front-end projects should prevent most of issues.

extra care has to be taken when upgrading project that uses old styles version.

## using submodule

example:

```
git submodule add git@codebasehq.com:roundpoint/profmed/profmed-common-styles.git client/app/assets/vendor/common
```

Gruntfile.js should contain 'update_submodules' task as a first item in 'build' task and following config:
```
"update_submodules": {
    default: {}
},
```

and project should have the submodule update module
```
npm install --save-dev grunt-update-submodules
```

### keeping files in sync

when we commit changes to styles we should also commit to a parent project that is using it updating the submodule commit refarence
its very easy when using SourceTree.
