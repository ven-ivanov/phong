'use strict';

/**
 * @ngdoc function
 * @name PortalBrowserV2.controller:Homev2Ctrl
 * @description
 * # Homev2Ctrl
 * Controller of the PortalBrowserV2
 */
angular.module('PortalBrowserV2')
.controller('Homev2Ctrl', function ($scope, $location, sessionv2) {
    if(!sessionv2.isActive()) {
        $location.path('/signinv2');
    }

    $scope.goToTeam = function() {
        $location.path('/teamv2');
    };

    $scope.goToAdmin = function() {
        $location.path('/adminv2');
    };

    $scope.panels = [
        {title: 'Team Management', click: $scope.goToTeam},
        {title: 'Content Editor'},
        {title: 'Admin & accounts', click: $scope.goToAdmin},
    ];
});
