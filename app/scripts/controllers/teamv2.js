'use strict';

/**
 * @ngdoc function
 * @name PortalBrowserV2.controller:Teamv2Ctrl
 * @description
 * # Teamv2Ctrl
 * Controller of the PortalBrowserV2
 */
angular.module('PortalBrowserV2')
.controller('Teamv2Ctrl', function ($scope) {
    $scope.panels = [
        {title: 'Invite new authors to your team'},
        {title: 'Invite new reviewers to your team'},
        {title: 'Invite helpers to work with you'},
        {title: 'Manage disclosures'},
        {title: 'Manage team changes'},
        {title: 'View your team\'s details'},
    ];
});
