'use strict';

/**
 * @ngdoc function
 * @name PortalBrowserV2.controller:ContributorInvitationCtrl
 * @description
 * # ContributorInvitationCtrl
 * Controller of the PortalBrowserV2
 */
angular.module('PortalBrowserV2')
    .controller('ContributorInvitationController',['$scope','$http','$location','sessionv2','ContributorResourceService', function ($scope, $http, $location, sessionv2,ContributorResourceService) {
        //as of now we're ignoring login
        /*if(!sessionv2.isActive()) {
            $location.path('/signinv2');
        }*/
        $scope.panels = [
            {title: 'Team Management', click: $scope.goToTeam},
            {title: 'Content Editor'},
            {title: 'Admin & accounts', click: $scope.goToAdmin},
        ];

        $scope.accessLevels =[
            {title: 'Provider'},
            {title: 'Media Admin'},
            {title: 'Reviewer'},
            {title: 'Author'},
            {title: 'Editor'},
            {title: 'Publisher'},
            {title: 'Helper'}
        ];

        $scope.contributorInfo = {
            firstName: '',
            familyName: '',
            emailAddress: '',
            accessLevel: ''
        };

        $scope.inviteContributor = function () {
            return ContributorResourceService.contributor.invite($scope.contributorInfo)
        };

        $scope.onSubmitForm = function(isValid) {
            // check to make sure the form is completely valid

            if (isValid) {
                $scope.loading = true;
                $scope.inviteContributor()
                    .then(function(response){
                        //check success
                        if(response.success || true) {
                            //show success modal
                            $scope.$broadcast('show-modal','invitation-success');
                            $scope.loading = false;
                        }
                        //check error
                    },function(err){
                        $scope.loading = false;
                        $scope.$broadcast('show-modal','invitation-fail');
                        console.log(err);
                    });
            } else{
                //show fail modal

            }

        };
    }]);

