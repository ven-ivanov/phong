'use strict';

/**
 * @ngdoc function
 * @name PortalBrowserV2.controller:ContributorDisclosureController
 * @description
 * # ContributorDisclosureController
 * Controller of the PortalBrowserV2
 */
angular.module('PortalBrowserV2')
    .controller('ContributorDisclosureController',['$scope','$location','sessionv2','ContributorResourceService', function ($scope, $location, sessionv2,ContributorResourceService) {
        //as of now we're ignoring login
        /*if(!sessionv2.isActive()) {
            $location.path('/signinv2');
        }*/

        $scope.title = {};
        $scope.titles = [ // Taken from https://gist.github.com/unceus/6501985
            {name: 'Mr.', code: 'AF'},
            {name: 'Miss.', code: 'AX'},
            {name: 'Mrs.', code: 'AL'},
            {name: 'Ms.', code: 'AL'}
        ];

        $scope.otherTitle = {};
        $scope.otherTitles = [ // Taken from https://gist.github.com/unceus/6501985
            'Dr.',
            'Ph D.',
            'Sir.'
        ];

        $scope.contributor = {
            title: '',
            familyName: '',
            email: '',
            otherTitle: '',
            postNomLetter: '',
            address1: '',
            address2: '',
            state: '',
            phoneNo: '',
            reminderHint: '',
            zipCode: ''
        };
        var in10Days = new Date();
        in10Days.setDate(in10Days.getDate() + 10);
        $scope.openCalendar = function(e, date) {
            date = true;
        };
        $scope.dates = {
            date1: new Date(),
            date2: new Date(),
            date3: new Date(),
            date4: new Date(),
            date5: in10Days,
            date6: new Date(),
            date7: new Date(),
            date8: new Date()
        };
        //question 1 model
        $scope.questionOne = {
            isEditing: false,
            index: -1,
            selected :{
                organizationName:   '',
                grant:              true,
                personalFee:        true,
                paidEmployee:       true,
                other:              true,
                comment:            '',
                startDate:          new Date(),
                endDate:            new Date(),
                startDateOpen:      false,
                endDateOpen:        false
            }
        };
        $scope.hasQuestionOne = true;
        $scope.hasQuestionTwo = true;
        $scope.hasQuestionThree = true;
        $scope.hasQuestionFour = true;
        //question 2 model
        $scope.questionTwo = {
            isEditing: false,
            index: -1,
            selected :{
                organizationName:   '',
                relationshipType:   '',
                comment:            '',
                startDate:          new Date(),
                endDate:            new Date(),
                startDateOpen:      false,
                endDateOpen:        false
            }
        };

        $scope.questionThree = {
            isEditing: false,
            index: -1,
            selected :{
                patentNumber:   '',
                pending:                false,
                licensed:               false,
                royalties:              false,
                issued:                 false,
                licensee:               false,
                comment:            ''
            }
        };


        $scope.questionFour = {
            isEditing: false,
            index: -1,
            selected :{
                disClosure:            ''
            }
        };

        $scope.date = '';

        //load existingd disclsoure...
        $scope.loading = true;
        ContributorResourceService.contributor.disclosure({contributorId:3})
        .then(function(response){
            $scope.loading = false;
            console.log(response.data);
            $scope.disclosure = response.data;
        });

        //real time save the disclosure
        $scope.$watchCollection( function(){
          return $scope.disclosure;
        },function(newVal){
            console.log('updated');
        });

        //show/hide edit panels for questions1
        $scope.$watch(function(){
          return $scope.hasQuestionOne
        }, function(newVal,oldVal){
          if(newVal === false){
            $scope.questionOne.isEditing = false;
            $scope.questionOne.index = -1;

          }
        });

        //show/hide edit panels for questions2
        $scope.$watch(function(){
          return $scope.hasQuestionTwo
        }, function(newVal,oldVal){
          if(newVal === false){
            $scope.questionTwo.isEditing = false;
            $scope.questionTwo.index = -1;

          }
        });

        //show/hide edit panels for questions3
        $scope.$watch(function(){
          return $scope.hasQuestionThree
        }, function(newVal,oldVal){
          if(newVal === false){
            $scope.questionThree.isEditing = false;
            $scope.questionThree.index = -1;

          }
        });

        //show/hide edit panels for questions4
        $scope.$watch(function(){
          return $scope.hasQuestionFour
        }, function(newVal,oldVal){
          if(newVal === false){
            $scope.questionFour.isEditing = false;
            $scope.questionFour.index = -1;

          }
        });

        //set contributor ID 30 by default
        $scope.contributorId = 10;

        $scope.registerDisclosure  = function (params, body) {
            return ContributorResourceService.contributor.saveDisclosure(params,body);
        };


        $scope.onSubmitForm = function(isValid) {
            // check to make sure the form is completely valid
            if (isValid) {
                $scope.loading = true;
                $scope.registerDisclosure({contributorId: $scope.contributorId}, $scope.disclosure)
                    .then(function(response){
                        //check success
                        if(response.success || true){
                            //show success modal
                            $scope.$broadcast('show-modal','disclosure-success');
                            $scope.loading = false;
                        }
                    },function(err){
                        $scope.loading = false;
                        $scope.$broadcast('show-modal','disclosure-fail');
                        console.log(err);
                    });

            } else{
                //show fail modal
                $scope.$broadcast('show-modal','invitation-fail');
            }

        };

        //edit question
        $scope.addQuestion = function(questionIndex){
          //init questions....
          $scope.editQuestion(null, -1,questionIndex);
        };
        //edit question
        $scope.editQuestion = function(item, index, questionIndex){
          //init questions....
          var selected = $scope.questionOne.selected;
          var questions = $scope.disclosure.questionOne.questions;
          switch(questionIndex){
            case 1:
              $scope.questionOne.selected = angular.copy(item);
              $scope.questionOne.index = index;
              $scope.questionOne.isEditing = true;
              questions = $scope.disclosure.questionOne.questions;
              break;
            case 2:
              $scope.questionTwo.selected = angular.copy(item);
              $scope.questionTwo.index = index;
              $scope.questionTwo.isEditing = true;
              questions = $scope.disclosure.questionTwo.questions;
              break;
            case 3:
              $scope.questionThree.selected = angular.copy(item);
              $scope.questionThree.index = index;
              $scope.questionThree.isEditing = true;
              questions = $scope.disclosure.questionThree.questions;
              break;
            case 4:
              $scope.questionFour.selected = angular.copy(item);
              $scope.questionFour.index = index;
              $scope.questionFour.isEditing = true;
              questions = $scope.disclosure.questionFour.questions;
              break;
            default:
              break;
          }
          //bind editing item

        };

        //save question
        // $scope.saveQuestion = function(item, index,questions){
        $scope.saveQuestion = function(questionIndex){
          //content in form
          var selected = $scope.questionOne.selected;
          //buffer to save content
          var questions = $scope.disclosure.questionOne.questions;
          //update index in  buffer
          var index = $scope.questionOne.index;

          switch(questionIndex){
            case 1:
              selected = $scope.questionOne.selected;
              index = $scope.questionOne.index;
              questions = $scope.disclosure.questionOne.questions;
              break;
            case 2:
              selected = $scope.questionTwo.selected;
              index = $scope.questionTwo.index;
              questions = $scope.disclosure.questionTwo.questions;
              break;
            case 3:
              selected = $scope.questionThree.selected;
              index = $scope.questionThree.index;
              questions = $scope.disclosure.questionThree.questions;
              break;
            case 4:
              selected = $scope.questionFour.selected;
              index = $scope.questionFour.index;
              questions = $scope.disclosure.questionFour.questions;
              break;
            default:
              break;
          }
          //now save questions in buffer..
          //update time is set automatically
          var today = new Date();
          selected.updateDate = moment().format("M/D/YYYY");
          // selected.startDate = moment(angular.copy(selected.startDate),"M/D/YYYY");
          // selected.endDate = moment(angular.copy(selected.endDate), "M/D/YYYY");
          if(index === -1){ //new item
              questions.push(selected);
          }else{ //edit item
              questions[index] = selected;
          }
        };

        //delete question
        $scope.deleteQuestion = function(item, index,questions){
            if(index === -1){
              return false;
            }
            questions.splice(index,1);
        }

        //cancel question add or edit
        $scope.cancelQuestion = function(questionIndex){
          //init questions....
          switch(questionIndex){
            case 1:
              $scope.questionOne.selected = null;
              $scope.questionOne.index = -1;
              $scope.questionOne.isEditing = false;
              break;
            case 2:
              $scope.questionTwo.selected = null;
              $scope.questionTwo.index = -1;
              $scope.questionTwo.isEditing = false;
              break;
            case 3:
              $scope.questionThree.selected = null;
              $scope.questionThree.index = -1;
              $scope.questionThree.isEditing = false;
              break;
            case 4:
              $scope.questionFour.selected = null;
              $scope.questionFour.index = -1;
              $scope.questionFour.isEditing = false;
              break;
            default:
              break;
          }
          //bind editing item
        };
    }]);
