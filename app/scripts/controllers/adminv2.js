'use strict';

/**
 * @ngdoc function
 * @name PortalBrowserV2.controller:Adminv2Ctrl
 * @description
 * # Adminv2Ctrl
 * Controller of the PortalBrowserV2
 */
angular.module('PortalBrowserV2')
.controller('Adminv2Ctrl', function ($scope) {
    $scope.panels = [
        {title: 'Manage & Update my Profile', click: $scope.goToTeam},
        {title: 'My Transactions with NowCE'},
        {title: 'Users Transaction Records'},
        {title: 'Users Performance Metrics'},
        {title: 'Courses & Performance Metrics'},
    ];
});
