'use strict';

angular.module('PortalBrowserV2')
.controller('Signinv2Ctrl', function ($scope, $log, $location, authenticationv2, sessionv2) {

    $scope.passwordStep = false;

    // TODO: if session exists then redirect to home
    if(sessionv2.isActive()) {
        $location.path('/');
    }

    var signInCallback = function(err, data) {
        $log.info('signInCallback', data);
        if(err) {
            return $log.warn('signInCallback error', err.name, err.message);
        }

        if(data.UUID) {
            $scope.passwordStep = true;
        } else {
            $scope.passwordStep = false;
        }

        if(data.authentication) {
            // TODO: save authentication details
            sessionv2.start(data);
            $location.path('/');
        }
    };

    $scope.signIn = function() {
        authenticationv2.authenticate($scope.signinModel, signInCallback);
    };

    $scope.stepBack = function() {
        $scope.passwordStep = false;
    };

});
