'use strict';

/**
 * @ngdoc function
 * @name PortalBrowserV2.controller:Profilev2Ctrl
 * @description
 * # Profilev2Ctrl
 * Controller of the PortalBrowserV2
 */
angular.module('PortalBrowserV2')
.controller('Profilev2Ctrl', function ($scope, sessionv2, userv2) {

    // TODO: get user model
    $scope.userModel = sessionv2.getUser();

    $scope.countries = [];
    $scope.states = [];
    $scope.availableStates = [];
    $scope.countrySelectUSA = false;

    $scope.userFormFields = [
        {
            title: 'Title',
            name: 'title',
            required: true
        },
        {
            title: 'First name(s)',
            name: 'firstName',
            minlength: 2,
            required: true
        },
        {
            title: 'Family name',
            name: 'lastName',
            minlength: 2,
            required: true
        },
        {
            title: 'Post nominal letters',
            name: 'postNominal'
        },
        {
            title: 'Address 1',
            name: 'streetAddress'
        },
        {
            title: 'Address 2',
            name: ''
        },
        {
            title: 'Country',
            name: 'country',
            select: {
                change: $scope.updateCountry
            }
        },
        {
            title: 'State/Province',
            name: 'state',
            select: {
                change: $scope.updateState,
                options: $scope.states
            }
        },
        {
            title: 'Zip/Post code',
            name: 'zipcodePostcode'
        },
        {
            title: 'Phone number',
            name: 'phoneAccount'
        },
        {
            title: 'Email Address',
            name: 'email',
            type: 'email',
            required: true
        },
        {
            title: 'A security word',
            name: 'securityWord',
            required: true
        },
    ];

    // TODO: clean up imported code

    $scope.updateOwnerDetails = function () {

        var userUpdateCallback = function(err, data) {
            if(err){
                console.log('error');
            }
            if(data){
                console.log('ok');
            }
        };

        userv2.save($scope.userModel, userUpdateCallback);

    };

    $scope.updateCountry = function () {
        $scope.availableStates = [];

        $scope.countryState = $scope.country.name;
        if ($scope.country.name === 'USA') {
            $scope.countrySelectUSA = true;
        } else {
            $scope.countrySelectUSA = false;
        }
        angular.forEach($scope.states, function (value) {
            if (value.countryId === $scope.country.id) {
                $scope.availableStates.push(value);
            }
        });

        $log.info(' The AvailableStates ');
    };

    $scope.updateCounty = function () {

        $scope.userModel.countyState = $scope.state.name;

    };

    $scope.initCountry = function (countryName, stateName) {

        var country = {};
        $scope.countries.forEach(function (value) {
            if (value.name === countryName) {
                country = value;
            }
        });

        if (country.name === 'USA') {
            $scope.countrySelectUSA = true;
        } else {
            $scope.countrySelectUSA = false;
        }

        $scope.countrySelect = country.id - 1;
        $scope.availableStates = [];
        angular.forEach($scope.states, function (value) {
            if (value.countryId === country.id) {
                $scope.availableStates.push(value);
            }
        });

        var state = {};
        $scope.states.forEach(function (value) {
            if (value.name === stateName) {
                state = value;
            }
        });
        $scope.stateSelect = state.id - 1;

    };

    $scope.initCountry($scope.userModel.countryState, $scope.userModel.countyState);
});
