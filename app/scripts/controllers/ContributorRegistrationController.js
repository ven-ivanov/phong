'use strict';

/**
 * @ngdoc function
 * @name PortalBrowserV2.controller:ContributorRegistrationController
 * @description
 * # ContributorRegistrationController
 * Controller of the PortalBrowserV2
 */
angular.module('PortalBrowserV2')
    .controller('ContributorRegistrationController',['$scope','$location','sessionv2','ContributorResourceService', function ($scope, $location, sessionv2,ContributorResourceService) {
        //as of now we're ignoring login
        /*if(!sessionv2.isActive()) {
            $location.path('/signinv2');
        }*/

        $scope.title = {};
        $scope.titles = [ // Taken from https://gist.github.com/unceus/6501985
            {name: 'Mr.', code: 'AF'},
            {name: 'Miss.', code: 'AX'},
            {name: 'Mrs.', code: 'AL'},
            {name: 'Ms.', code: 'AL'}
        ];

        $scope.otherTitle = {};
        $scope.otherTitles = [ // Taken from https://gist.github.com/unceus/6501985
            'Dr.',
            'Ph D.',
            'Sir.'
        ];
        $scope.contributor = {
            title: '',
            familyName: '',
            email: '',
            otherTitle: '',
            postNomLetter: '',
            address1: '',
            address2: '',
            state: '',
            phoneNo: '',
            reminderHint: '',
            zipCode: ''
        };

        $scope.registerContributor  = function () {
            return ContributorResourceService.contributor.register($scope.contributor)

        };


        $scope.onSubmitForm = function(isValid) {
            // check to make sure the form is completely valid
            if (isValid) {
                $scope.loading = true;
                $scope.registerContributor()
                    .then(function(response){
                        //check success
                        if(response.success || true){
                            //show success modal
                            $scope.$broadcast('show-modal','invitation-success');
                            $scope.loading = false;
                        }
                    },function(err){
                        $scope.loading = false;
                        $scope.$broadcast('show-modal','invitation-fail');
                        console.log(err);
                    });

            } else{
                //show fail modal
                $scope.$broadcast('show-modal','invitation-fail');
            }

        };
    }]);
