'use strict';

angular.module('PortalBrowserV2')
.controller('Navbarv2Ctrl', function ($scope, $log, $location, sessionv2) {
    $scope.signOut = function() {
        sessionv2.end();
        $location.path('/');
    };

    $scope.isSessionTerminated = function() {
        return !sessionv2.isActive();
    };

    $scope.isSessionActive = function() {
        return sessionv2.isActive();
    };

    $scope.buttons = [
        {title: "Home", link: "#/"},
        {title: "Sign out", link: "#/root", click: $scope.signOut, hide: $scope.isSessionTerminated, right: true},
        {title: "Profile", link: "#/profilev2", right: true},
    ];

});
