'use strict';

angular.module('PortalBrowserV2')
.controller('NavbarCtrl',['$rootScope','$scope','$location','sessionv2', function ($rootScope,$scope, $log, $location, sessionv2) {
    $scope.signOut = function() {
        sessionv2.end();
        $location.path('/');
    };

    $scope.isSessionTerminated = function() {
        return !sessionv2.isActive();
    };

    $scope.isSessionActive = function() {
        return sessionv2.isActive();
    };

    $scope.buttons = [

        {title: "Navigator", link: "#/contributor/signup", class:"flaticon stroke menu-list-3", afterClass:"flaticon stroke arrow-down-1",
          dropdowns: [
            {title: "P01", link: "#/provider/signup", class:"flaticon stroke add-user-1"},
            {title: "P31", link: "#/contributor/invitation", class:"flaticon stroke add-user-1"},
            {title: "P32", link: "#/contributor/signup", class:"flaticon stroke help-2"},
            {title: "P33", link: "#/contributor/disclosure", click: $scope.signOut, hide: $scope.isSessionTerminated, right: true, class:"flaticon stroke logout"},
          ]},
        {title: "Help", link: "#/provider/signup", class:"flaticon stroke help-2"},
        {title: "Logout", link: "#/logout", click: $scope.signOut, hide: $scope.isSessionTerminated, right: true, class:"flaticon stroke logout"},
    ];

    $scope.dropdowns = [
      {title: "P01", link: "#/contributor/signup", class:"flaticon stroke add-user-1"},
      {title: "P31", link: "#/contributor/invitation", class:"flaticon stroke add-user-1"},
      {title: "P32", link: "#/provider/signup", class:"flaticon stroke help-2"},
      {title: "P33", link: "#/contributor/disclosure", click: $scope.signOut, hide: $scope.isSessionTerminated, right: true, class:"flaticon stroke logout"},
    ];

    $rootScope.showPageMenu = false;
}]);
