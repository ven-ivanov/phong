'use strict';

/**
 * @ngdoc directive
 * @name PortalBrowserV2.directive:showFocus
 * @description
 * # showFocus
 * source: http://stackoverflow.com/questions/22776662/catch-ng-show-event-and-focus-input-field
 */
angular.module('PortalBrowserV2')
.directive('showFocus', function($timeout/*, $log*/) {
    return function(scope, element, attrs) {
        scope.$watch(attrs.showFocus,
            function(newValue) {
                $timeout(function() {
                    if(newValue){
                        element.focus();
                    }
                });
            }, true);
    };
})
.directive("passwordVerify", function() {
   return {
      require: "ngModel",
      scope: {
        passwordVerify: '='
      },
      link: function(scope, element, attrs, ctrl) {
        scope.$watch(function() {
            var combined;

            if (scope.passwordVerify || ctrl.$viewValue) {
               combined = scope.passwordVerify + '_' + ctrl.$viewValue;
            }
            return combined;
        }, function(value) {
            if (value) {
                ctrl.$parsers.unshift(function(viewValue) {
                    var origin = scope.passwordVerify;
                    if (origin !== viewValue) {
                        ctrl.$setValidity("passwordVerify", false);
                        return undefined;
                    } else {
                        ctrl.$setValidity("passwordVerify", true);
                        return viewValue;
                    }
                });
            }
        });
     }
   };
});
