'use strict';

var app = angular.module('PortalBrowserV2');

/**
 * Display an information modal - does not have a submit button, only "OK"
 */
app.directive('infoModal', ['ModalInheritanceService', function (ModalInheritanceService) {
  return {
    templateUrl: '/scripts/directives/info-modal/views/modal.html',
    transclude: true,
    scope: {
      modalTitle: '@',
      message: '@',
      modalId: '@',
      buttonText: '@',
      modalClass: '@',
      modalWidth: '@',
      modalHeight: '@'
    },
    link: function (scope, element, attrs) {
      // Dynamically set class
      scope.modalClass = scope.modalClass || 'new-window-overlay';
      // Apply the hide and show modal methods to the scope
      ModalInheritanceService.inheritModal.call(null, scope, element, attrs);
    }
  };
}]);
