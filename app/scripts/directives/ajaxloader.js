'use strict';

/**
 * @ngdoc directive
 * @name PortalBrowserV2.directive:showFocus
 * @description
 * # showFocus
 * source: http://stackoverflow.com/questions/22776662/catch-ng-show-event-and-focus-input-field
 */
angular.module('PortalBrowserV2')
    .directive('ajaxLoader', ['requestNotificationChannel', function (requestNotificationChannel) {
        return {
            restrict: "E",
            replace:true,
            template: '<div class="loading-box" style="z-index:9999; width: 100%; height:100%;"><div class="spinner loader-box"></div></div>',
            link: function (scope, element) {
                // hide the element initially
                element.hide();
                scope.$watch('loading', function (val) {
                    if (val){
                        console.log('show');
                        element.show();
                    }else{
                        console.log('hide');
                        element.hide();
                    }
                });

                /*var startRequestHandler = function() {
                    // got the request start notification, show the element
                    console.log('showing');
                    element.show();
                };

                var endRequestHandler = function() {
                    // got the request start notification, show the element
                    console.log('hiding');
                    element.hide();
                };
                // register for the request start notification
                requestNotificationChannel.onRequestStarted(scope, startRequestHandler);
                // register for the request end notification
                requestNotificationChannel.onRequestEnded(scope, endRequestHandler);*/
            }
        };
    }]);
