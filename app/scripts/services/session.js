/**
 * SessionService
 *
 * @author: korzec
 * @created: 23/09/2014
 */
'use strict';

var SessionService = function() {
    this.getUserSessionToken = function() {
        return sessionStorage.getItem('session.token');
    };
    this.getPortalToken = function() {
        return sessionStorage.getItem('session.tokenPortal');
    };
};

angular.module('PortalBrowserV2').service('sessionService', ['configuration', SessionService]);