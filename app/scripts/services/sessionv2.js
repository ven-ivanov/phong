'use strict';

/**
 * @author: korzec
 * @created: 2015-06-28
 *
 */
angular.module('PortalBrowserV2')
.service('sessionv2', function () {
// TODO:
// check session storage, if token exists set state to signed-in
// call authentication resource
//
//
//

    // @params: data - response payload from API POST /authentication
    this.start = function(data) {
        sessionStorage.setItem('session.token', data.profmedToken);
        sessionStorage.setItem('session.tokenPortal', data.token);
        sessionStorage.setItem('session.user', JSON.stringify(data));
    };

    this.end = function() {
        sessionStorage.clear();
    };

    this.isActive = function() {
        var token = sessionStorage.getItem('session.tokenPortal');
        return !!token;
    };

    this.getUser = function() {
        var json = sessionStorage.getItem('session.user');
        if(json) {
            var user = JSON.parse(json);
            user.id = user.UUID;
            return user;
        }
    };

});
