/**
 * user API
 *
 * @author: korzec
 * @created: 23/09/2014
 */
'use strict';

angular.module('PortalBrowserV2').factory('Userv2',
    [
        'configuration',
        '$http',
        'sessionService',
        'GenericDataService',
        function (configuration, $http, sessionService, GenericDataService) {

            var Userv2 = function () {

                GenericDataService.apply(this, arguments);
                this.serviceUrl = configuration.dataHostURL + configuration.dataURI + '/user'
                this.dataAttribute = 'user';
            };

            Userv2.prototype.save = function(params, callback) {
                var self = this;

                var requestData = _.cloneDeep(params);

                $http({
                    method: 'POST',
                    url: this.serviceUrl + '/save',
                    data: requestData
                }).success(function(data /*, status*/ ) {
                    if (callback) {
                        callback(null, data[self.dataAttribute]);
                    }
                }).error(function(data /*, status*/ ) {
                    if (callback) {
                        callback('service error', data);
                    }
                });
            };

            return Userv2;

        }
    ]);

angular.module('PortalBrowserV2').service('userv2',
    [
        'Userv2',
        function (Userv2) {
            return new Userv2();
        }
    ]);
