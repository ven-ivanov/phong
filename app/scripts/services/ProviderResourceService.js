'use strict';

var app = angular.module('PortalBrowserV2');

/**
 * Service for constructing $resource URLs, parameters, methods, etc
 */
app.factory('ProviderResourceService', ['$resource',function ($resource) {

    var API_PREFIX = '//127.0.0.1:3000';
    /**
     * Perform a query, expect array response
     * @TODO Fix this JSHint error and remove the ignore:line
     */
    var query = function (url, params) {  // jshint ignore:line
        return $resource(API_PREFIX + url).query(params).$promise;
    };

    /**
     * Get request, non-array expected
     */
    var getRequest = function (url, params) {
        return $resource(API_PREFIX + url ).get(params).$promise;
    };

    /**
     * General post request
     */
    var post = function (url, params, body, headers) {
        if (!headers) {
            return new ($resource(API_PREFIX + url, params))(body).$save();
        }
        // Specify put request with potential custom headers
        return new ($resource(API_PREFIX + url, params, {
            postWithHeaders: {method: 'POST', headers: headers}
        }))(body).$postWithHeaders();
    };

    /**
     * General PUT request
     * @TODO Fix this JSHint error and remove the ignore:line
     */
    var put = function (url, params, body, headers) { // jshint ignore:line
        // Specify put request with potential custom headers
        return new ($resource(API_PREFIX + url, params, {
            update: {method: 'PUT', headers: headers}
        }))(body).$update();
    };

    /**
     * Update request via POST
     */
 /*   var patch = function (url, params, body, headers) {
        return new ($resource(API_PREFIX + url, params, {
            update: {method: 'PATCH', headers: headers}
        }))(body).$update();
    };*/

    /**
     * Post with array body
     * @TODO Fix this JSHint error and remove the ignore:line
     */
    var postArray = function (url, params, body) { // jshint ignore:line
        return new ($resource(API_PREFIX + url, params, {
            postArray: {
                method: 'POST',
                transformRequest: function (data) {
                    var thisData = angular.copy(data), response = [];
                    // Iterate and send as array
                    angular.forEach(thisData, function (item) {
                        response.push(item);
                    });
                    return JSON.stringify(response);
                }
            }
        }))(body).$postArray();
    };

    /**
     * Initiate a delete request
     */
    /*var deleteRequest = function (url, params, headers) {
        return new ($resource(API_PREFIX + url, params, {
            doDelete: {method: 'DELETE', headers: headers}
        }))().$doDelete();
    };*/
    /**
     * ProviderResourceService
     */
    var Service = {
        // General resource object
        resource: {type: '', url: '', params: '', methods: ''},
        /**
         * Provider Subview
         */
        provider: {
            // Standard request URLs
            requests: {
                registration: '/PortalServiceV2/provider/save',
                emailDuplicate: '/PortalServiceV2/registration/unique/email',
            },
            /**
             * create new staff
             * @param params
             */
            register: function (params) {
                return post(Service.provider.requests.registration, {}, params);
            }
        }
    };
    return Service;
}]);
