/**
 * examPerformance
 *
 * @author: korzec
 * @created: 23/09/2014
 */

'use strict';

angular.module('PortalBrowserV2').factory('ExamPerformanceService',
    [
        'configuration',
        '$http',
        'sessionService',
        'GenericDataService',
        function (configuration, $http, sessionService, GenericDataService) {

            var ExamPerformanceService = function () {

                GenericDataService.apply(this, arguments);
                this.serviceUrl = configuration.analyticsHostURL + configuration.analyticsURI + '/questionPerformance';
                this.dataAttribute = 'questionPerformance';
            };

            ExamPerformanceService.prototype = Object.create(GenericDataService.prototype);

            if (window.mockData) {
                ExamPerformanceService.prototype.mockData = window.mockData.examPerformance;
            }

            ExamPerformanceService.prototype.getReport = ExamPerformanceService.prototype.query;

            ExamPerformanceService.prototype.getReportDownloadLink = function (params) {
                return this.serviceUrl + '/download?' + jQuery.param(params);
            };

            return ExamPerformanceService;

        }
    ]);

angular.module('PortalBrowserV2').service('examPerformanceService',
    [
        'ExamPerformanceService',
        function (ExamPerformanceService) {
            return new ExamPerformanceService();
        }
    ]);
