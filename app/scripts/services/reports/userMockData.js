
window.mockData = window.mockData || {};

window.mockData['users']  = [
    {
        "token": "a04d8c3e-1f25-4f2d-bcc4-542895c5b207",
        "status": "active",
        "dateCreated": "2014-01-22T09:55:38+0000",
        "id": "862886620e224334bf7bbcf83eef160c",
        "version": 0,
        "attributes": {
            "lastName": "Bussey",
            "title": "",
            "productCodes": "TOKEN:*,SLC:0",
            "email": "jgbussey@mac.com",
            "roles": [
                "reader"
            ],
            "paidThru": "",
            "subscriberId": "65939",
            "firstName": "Joseph",
            "memberTypeCode": "M",
            "balances": {
                "tokensPurchased": 0,
                "tokensConsumed": 0
            }
        },
        "lastUpdated": "2014-01-22T09:55:38+0000",
        "username": "65939",
        "ip": "212.44.25.28"
    },
    {
        "token": "9faf468f-198f-403c-b4b5-d6596ea3193b",
        "status": "active",
        "dateCreated": "2014-01-31T16:53:03+0000",
        "id": "be5a7d28e9cd4166a5302dd38f342f15",
        "version": 53,
        "attributes": {
            "lastName": "Hartwich",
            "productCodes": "TOKEN:*,SLC:1",
            "email": "jhartwich@lifespan.org",
            "roles": [
                "reader"
            ],
            "paidThru": "2014-12-31 00:00:00.0",
            "subscriberId": "67717",
            "firstName": "Joseph",
            "memberTypeCode": "GSR",
            "balances": {
                "tokensPurchased": "*",
                "tokensConsumed": 495
            }
        },
        "lastUpdated": "2014-05-14T19:32:59+0000",
        "username": "67717",
        "ip": "68.109.228.34"
    },
    {
        "token": "03f2ce25-bc4b-4bc0-af5d-73574fbeaba2",
        "status": "active",
        "dateCreated": "2014-01-22T09:55:35+0000",
        "id": "ee3586cf4c054cd8afbb7a604b8ce2c2",
        "version": 0,
        "attributes": {
            "lastName": "Lelli",
            "title": "",
            "productCodes": "TOKEN:*,SLC:0",
            "email": "jlelli@dmc.org",
            "roles": [
                "reader"
            ],
            "paidThru": "",
            "subscriberId": "49884",
            "firstName": "Joseph",
            "memberTypeCode": "M",
            "balances": {
                "tokensPurchased": 0,
                "tokensConsumed": 0
            }
        },
        "lastUpdated": "2014-01-22T09:55:35+0000",
        "username": "49884",
        "ip": "212.44.25.28"
    }
];