/**
 * ExamService
 *
 * @author: korzec
 * @created: 23/09/2014
 */

'use strict';

angular.module('PortalBrowserV2').factory('ExamService',
    [
        'configuration',
        '$http',
        'sessionService',
        'GenericDataService',
        function (configuration, $http, sessionService, GenericDataService) {

            var ExamService = function () {

                GenericDataService.apply(this, arguments);
                this.serviceUrl = configuration.dataHostURL + configuration.dataURI + '/examProxy';
                this.dataAttribute = 'exams';
            };

            ExamService.prototype = Object.create(GenericDataService.prototype);

            if (window.mockData) {
                ExamService.prototype.mockData = window.mockData.exams;
            }

            return ExamService;

        }
    ]);

angular.module('PortalBrowserV2').service('examService',
    [
        'ExamService',
        function (ExamService) {
            return new ExamService();
        }
    ]);
