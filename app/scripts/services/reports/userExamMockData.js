window.mockData = window.mockData || {};

window.mockData['userExams']   = [
    {
        "userId": "be5a7d28e9cd4166a5302dd38f342f15",
        "status": "closed",
        "dateCreated": "2014-04-07T15:58:15+0000",
        "id": "c6aa010d439940ef87c18e8666723b0d",
        "version": 4,
        "attributes": {
            "feedback": [
                {
                    "questionNumber": 1,
                    "answerGiven": "5"
                },
                {
                    "questionNumber": 2,
                    "answerGiven": "5"
                },
                {
                    "questionNumber": 3,
                    "answerGiven": "5"
                },
                {
                    "extraFeedbackGiven": ""
                }
            ],
            "title": "Spaced Learning week 9",
            "elapsedSeconds": 35,
            "score": "100",
            "questions": [
                "1d61dbe4894f4b5eb662c2eda30c2341",
                "ecfea919cf5c42afa98a3bd495d189dd"
            ],
            "examId": "27d0458e9139472d81a91872d0817e2a",
            "programType": "slc",
            "custom": false,
            "dateCompleted": "2014-04-07T15:59:02.298Z"
        },
        "lastUpdated": "2014-04-07T15:59:02+0000"
    },
    {
        "userId": "be5a7d28e9cd4166a5302dd38f342f15",
        "status": "closed",
        "dateCreated": "2014-03-17T12:38:29+0000",
        "id": "91339c76ac7c40fcbc7725adabadf789",
        "version": 3,
        "attributes": {
            "feedback": [
                {
                    "questionNumber": 1,
                    "answerGiven": "5"
                },
                {
                    "questionNumber": 2,
                    "answerGiven": "5"
                },
                {
                    "questionNumber": 3,
                    "answerGiven": "5"
                },
                {
                    "extraFeedbackGiven": ""
                }
            ],
            "title": "Spaced Learning week 6",
            "elapsedSeconds": 46,
            "score": "100",
            "questions": [
                "bf50619c11a04c1592677de15c5fa6d2",
                "11a3b0aaf0624e6ea0156ecb422cfc1f"
            ],
            "examId": "abadb3180ace4c868bba2380e4290ccb",
            "programType": "slc",
            "custom": false,
            "dateCompleted": "2014-03-17T12:39:24.169Z"
        },
        "lastUpdated": "2014-03-17T12:39:24+0000"
    },
    {
        "userId": "be5a7d28e9cd4166a5302dd38f342f15",
        "status": "open",
        "dateCreated": "2014-02-21T21:18:32+0000",
        "id": "2ef753a3d6be4ac1a0cd9fd82f5e455a",
        "version": 7,
        "attributes": {
            "title": "Custom Course, testicular,  21 Feb  2014 4:18 ",
            "elapsedSeconds": 116,
            "score": "5/8",
            "questions": [
                {
                    "id": "fc439dc9f93a48c2a797329264ef852d",
                    "response": {
                        "questionId": "fc439dc9f93a48c2a797329264ef852d",
                        "count": null,
                        "status": "correct",
                        "lastUpdated": "2014-02-21T21:19:07+0000",
                        "parent": {
                            "id": "bacf0bf547b3402f9bbc9e52e99973a1",
                            "dateCreated": "2014-02-21T21:19:07+0000",
                            "lastUpdated": "2014-02-21T21:19:07+0000",
                            "version": 0
                        },
                        "firstResponse": true,
                        "version": 0,
                        "id": "bacf0bf547b3402f9bbc9e52e99973a1",
                        "session": "2ef753a3d6be4ac1a0cd9fd82f5e455a",
                        "userId": "be5a7d28e9cd4166a5302dd38f342f15",
                        "dateCreated": "2014-02-21T21:19:07+0000",
                        "claimed": false,
                        "attributes": {
                            "claimed": false,
                            "answerGiven": "3",
                            "firstResponse": true
                        },
                        "answerGiven": "3"
                    }
                },
                {
                    "id": "fb4d247f4b814d338a60385ce7be9040",
                    "response": {
                        "questionId": "fb4d247f4b814d338a60385ce7be9040",
                        "count": null,
                        "status": "incorrect",
                        "lastUpdated": "2014-02-21T21:19:17+0000",
                        "parent": {
                            "id": "b4606c4fd5ca4383b8d0f4d9efd18072",
                            "dateCreated": "2014-02-21T21:19:17+0000",
                            "lastUpdated": "2014-02-21T21:19:17+0000",
                            "version": 0
                        },
                        "firstResponse": true,
                        "version": 0,
                        "id": "b4606c4fd5ca4383b8d0f4d9efd18072",
                        "session": "2ef753a3d6be4ac1a0cd9fd82f5e455a",
                        "userId": "be5a7d28e9cd4166a5302dd38f342f15",
                        "dateCreated": "2014-02-21T21:19:17+0000",
                        "claimed": false,
                        "attributes": {
                            "claimed": false,
                            "answerGiven": "1",
                            "firstResponse": true
                        },
                        "answerGiven": "1"
                    }
                },
                {
                    "id": "dab1c0781e924330ab3325bc51d8f3d5",
                    "response": {
                        "questionId": "dab1c0781e924330ab3325bc51d8f3d5",
                        "count": null,
                        "status": "correct",
                        "lastUpdated": "2014-02-21T21:19:39+0000",
                        "parent": {
                            "id": "c23fabba618b4e48bdb41cdc53a72081",
                            "dateCreated": "2014-02-21T21:19:39+0000",
                            "lastUpdated": "2014-02-21T21:19:39+0000",
                            "version": 0
                        },
                        "firstResponse": true,
                        "version": 0,
                        "id": "c23fabba618b4e48bdb41cdc53a72081",
                        "session": "2ef753a3d6be4ac1a0cd9fd82f5e455a",
                        "userId": "be5a7d28e9cd4166a5302dd38f342f15",
                        "dateCreated": "2014-02-21T21:19:39+0000",
                        "claimed": false,
                        "attributes": {
                            "claimed": false,
                            "answerGiven": "3",
                            "firstResponse": true
                        },
                        "answerGiven": "3"
                    }
                },
                {
                    "id": "67f82b739de34b8c9767ee1a01984f26",
                    "response": {
                        "questionId": "67f82b739de34b8c9767ee1a01984f26",
                        "count": null,
                        "status": "correct",
                        "lastUpdated": "2014-02-21T21:19:49+0000",
                        "parent": {
                            "id": "6c9bf567e1df46658664d534f4ea6ddf",
                            "dateCreated": "2014-02-21T21:19:49+0000",
                            "lastUpdated": "2014-02-21T21:19:49+0000",
                            "version": 0
                        },
                        "firstResponse": true,
                        "version": 0,
                        "id": "6c9bf567e1df46658664d534f4ea6ddf",
                        "session": "2ef753a3d6be4ac1a0cd9fd82f5e455a",
                        "userId": "be5a7d28e9cd4166a5302dd38f342f15",
                        "dateCreated": "2014-02-21T21:19:49+0000",
                        "claimed": false,
                        "attributes": {
                            "claimed": false,
                            "answerGiven": "1",
                            "firstResponse": true
                        },
                        "answerGiven": "1"
                    }
                },
                {
                    "id": "580a542798ba4d65ac96a71ac8c5c9fe",
                    "response": {
                        "questionId": "580a542798ba4d65ac96a71ac8c5c9fe",
                        "count": null,
                        "status": "correct",
                        "lastUpdated": "2014-02-21T21:20:00+0000",
                        "parent": {
                            "id": "9de739a871cf46d4aa93a3710db80dde",
                            "dateCreated": "2014-02-21T21:20:00+0000",
                            "lastUpdated": "2014-02-21T21:20:00+0000",
                            "version": 0
                        },
                        "firstResponse": true,
                        "version": 0,
                        "id": "9de739a871cf46d4aa93a3710db80dde",
                        "session": "2ef753a3d6be4ac1a0cd9fd82f5e455a",
                        "userId": "be5a7d28e9cd4166a5302dd38f342f15",
                        "dateCreated": "2014-02-21T21:20:00+0000",
                        "claimed": false,
                        "attributes": {
                            "claimed": false,
                            "answerGiven": "4",
                            "firstResponse": true
                        },
                        "answerGiven": "4"
                    }
                },
                {
                    "id": "133d38a24e69465d8cd6cd3051080d71",
                    "response": {
                        "questionId": "133d38a24e69465d8cd6cd3051080d71",
                        "count": null,
                        "status": "correct",
                        "lastUpdated": "2014-02-21T21:20:35+0000",
                        "parent": {
                            "id": "6570ba9c61934817b16a353731c7dc80",
                            "dateCreated": "2014-02-21T21:20:35+0000",
                            "lastUpdated": "2014-02-21T21:20:35+0000",
                            "version": 0
                        },
                        "firstResponse": true,
                        "version": 0,
                        "id": "6570ba9c61934817b16a353731c7dc80",
                        "session": "2ef753a3d6be4ac1a0cd9fd82f5e455a",
                        "userId": "be5a7d28e9cd4166a5302dd38f342f15",
                        "dateCreated": "2014-02-21T21:20:35+0000",
                        "claimed": false,
                        "attributes": {
                            "claimed": false,
                            "answerGiven": "3",
                            "firstResponse": true
                        },
                        "answerGiven": "3"
                    }
                },
                {
                    "id": "05f3fe41a3fc48d7a00cb7422a24ca50"
                },
                {
                    "id": "05b35a19f9e7405f84f311f64e5b74eb"
                }
            ],
            "examId": "a8a6413b-7c72-4025-adaf-7eb2f2bba3f0",
            "programType": "custom",
            "custom": true
        },
        "lastUpdated": "2014-02-21T21:20:52+0000"
    },
    {
        "userId": "be5a7d28e9cd4166a5302dd38f342f15",
        "status": "closed",
        "dateCreated": "2014-02-20T20:38:45+0000",
        "id": "463ea70206244f87a612393e4ad1647e",
        "version": 16,
        "attributes": {
            "feedback": [
                {
                    "answerGiven": "5"
                },
                {
                    "answerGiven": "5"
                },
                {
                    "answerGiven": "5"
                },
                {
                    "extraFeedbackGiven": ""
                }
            ],
            "title": "Custom Course, necrotizing,  20 Feb  2014 3:38 ",
            "elapsedSeconds": 340,
            "score": "93.3",
            "questions": [
                {
                    "id": "3a105f27d55a4f9e8015ff7fe089b26c",
                    "response": {
                        "questionId": "3a105f27d55a4f9e8015ff7fe089b26c",
                        "count": null,
                        "status": "correct",
                        "lastUpdated": "2014-02-20T20:39:03+0000",
                        "parent": {
                            "id": "548226d7d6af4395879f6599d83e919b",
                            "dateCreated": "2014-02-20T20:39:03+0000",
                            "lastUpdated": "2014-02-20T20:39:03+0000",
                            "version": 0
                        },
                        "firstResponse": true,
                        "version": 0,
                        "id": "548226d7d6af4395879f6599d83e919b",
                        "session": "463ea70206244f87a612393e4ad1647e",
                        "userId": "be5a7d28e9cd4166a5302dd38f342f15",
                        "dateCreated": "2014-02-20T20:39:03+0000",
                        "claimed": false,
                        "attributes": {
                            "claimed": false,
                            "answerGiven": "1",
                            "firstResponse": true
                        },
                        "answerGiven": "1"
                    }
                },
                {
                    "id": "f409940007bf41308bb331eab324e0d7",
                    "response": {
                        "questionId": "f409940007bf41308bb331eab324e0d7",
                        "count": null,
                        "status": "correct",
                        "lastUpdated": "2014-02-20T20:39:16+0000",
                        "parent": {
                            "id": "ef829dd6884e4d6b93cda0d144df3087",
                            "dateCreated": "2014-02-20T20:39:16+0000",
                            "lastUpdated": "2014-02-20T20:39:16+0000",
                            "version": 0
                        },
                        "firstResponse": true,
                        "version": 0,
                        "id": "ef829dd6884e4d6b93cda0d144df3087",
                        "session": "463ea70206244f87a612393e4ad1647e",
                        "userId": "be5a7d28e9cd4166a5302dd38f342f15",
                        "dateCreated": "2014-02-20T20:39:16+0000",
                        "claimed": false,
                        "attributes": {
                            "claimed": false,
                            "answerGiven": "3",
                            "firstResponse": true
                        },
                        "answerGiven": "3"
                    }
                },
                {
                    "id": "c415eac47c3a419bbbbed60a716b3056",
                    "response": {
                        "questionId": "c415eac47c3a419bbbbed60a716b3056",
                        "count": null,
                        "status": "correct",
                        "lastUpdated": "2014-02-20T20:40:12+0000",
                        "parent": {
                            "id": "9b6ae8869dd545469f45ae79edf71af1",
                            "dateCreated": "2014-02-20T20:40:12+0000",
                            "lastUpdated": "2014-02-20T20:40:12+0000",
                            "version": 0
                        },
                        "firstResponse": true,
                        "version": 0,
                        "id": "9b6ae8869dd545469f45ae79edf71af1",
                        "session": "463ea70206244f87a612393e4ad1647e",
                        "userId": "be5a7d28e9cd4166a5302dd38f342f15",
                        "dateCreated": "2014-02-20T20:40:12+0000",
                        "claimed": false,
                        "attributes": {
                            "claimed": false,
                            "answerGiven": "3",
                            "firstResponse": true
                        },
                        "answerGiven": "3"
                    }
                },
                {
                    "id": "bd18562c40264befa258c951edafa244",
                    "response": {
                        "questionId": "bd18562c40264befa258c951edafa244",
                        "count": null,
                        "status": "incorrect",
                        "lastUpdated": "2014-02-20T20:40:33+0000",
                        "parent": {
                            "id": "00e0a0c6dbe340a280dd49360cadc985",
                            "dateCreated": "2014-02-20T20:40:33+0000",
                            "lastUpdated": "2014-02-20T20:40:33+0000",
                            "version": 0
                        },
                        "firstResponse": true,
                        "version": 0,
                        "id": "00e0a0c6dbe340a280dd49360cadc985",
                        "session": "463ea70206244f87a612393e4ad1647e",
                        "userId": "be5a7d28e9cd4166a5302dd38f342f15",
                        "dateCreated": "2014-02-20T20:40:33+0000",
                        "claimed": false,
                        "attributes": {
                            "claimed": false,
                            "answerGiven": "1",
                            "firstResponse": true
                        },
                        "answerGiven": "1"
                    }
                },
                {
                    "id": "961d3f14b24b4a0fbd70fc7b0bd609b7",
                    "response": {
                        "questionId": "961d3f14b24b4a0fbd70fc7b0bd609b7",
                        "count": null,
                        "status": "correct",
                        "lastUpdated": "2014-02-20T20:40:58+0000",
                        "parent": {
                            "id": "0c0f0c2677c449dea7d0821c37ce4bc1",
                            "dateCreated": "2014-02-20T20:40:58+0000",
                            "lastUpdated": "2014-02-20T20:40:58+0000",
                            "version": 0
                        },
                        "firstResponse": true,
                        "version": 0,
                        "id": "0c0f0c2677c449dea7d0821c37ce4bc1",
                        "session": "463ea70206244f87a612393e4ad1647e",
                        "userId": "be5a7d28e9cd4166a5302dd38f342f15",
                        "dateCreated": "2014-02-20T20:40:58+0000",
                        "claimed": false,
                        "attributes": {
                            "claimed": false,
                            "answerGiven": "1",
                            "firstResponse": true
                        },
                        "answerGiven": "1"
                    }
                },
                {
                    "id": "7b47c8a7157a4eebb5da004903ae6219",
                    "response": {
                        "questionId": "7b47c8a7157a4eebb5da004903ae6219",
                        "count": null,
                        "status": "correct",
                        "lastUpdated": "2014-02-20T20:41:15+0000",
                        "parent": {
                            "id": "be606767155a4f4a8969950a8d319e4a",
                            "dateCreated": "2014-02-20T20:41:15+0000",
                            "lastUpdated": "2014-02-20T20:41:15+0000",
                            "version": 0
                        },
                        "firstResponse": true,
                        "version": 0,
                        "id": "be606767155a4f4a8969950a8d319e4a",
                        "session": "463ea70206244f87a612393e4ad1647e",
                        "userId": "be5a7d28e9cd4166a5302dd38f342f15",
                        "dateCreated": "2014-02-20T20:41:15+0000",
                        "claimed": false,
                        "attributes": {
                            "claimed": false,
                            "answerGiven": "5",
                            "firstResponse": true
                        },
                        "answerGiven": "5"
                    }
                },
                {
                    "id": "79d40d8a98a04ad0904983180b19315e",
                    "response": {
                        "questionId": "79d40d8a98a04ad0904983180b19315e",
                        "count": null,
                        "status": "correct",
                        "lastUpdated": "2014-02-20T20:42:24+0000",
                        "parent": {
                            "id": "cb8337ad63d844c096477224ae64f3d8",
                            "dateCreated": "2014-02-20T20:42:24+0000",
                            "lastUpdated": "2014-02-20T20:42:24+0000",
                            "version": 0
                        },
                        "firstResponse": true,
                        "version": 0,
                        "id": "cb8337ad63d844c096477224ae64f3d8",
                        "session": "463ea70206244f87a612393e4ad1647e",
                        "userId": "be5a7d28e9cd4166a5302dd38f342f15",
                        "dateCreated": "2014-02-20T20:42:24+0000",
                        "claimed": false,
                        "attributes": {
                            "claimed": false,
                            "answerGiven": "3",
                            "firstResponse": true
                        },
                        "answerGiven": "3"
                    }
                },
                {
                    "id": "e2af5965fe6542ad9f99ba0f3cfe84a4",
                    "response": {
                        "questionId": "e2af5965fe6542ad9f99ba0f3cfe84a4",
                        "count": null,
                        "status": "correct",
                        "lastUpdated": "2014-02-20T20:42:41+0000",
                        "parent": {
                            "id": "3a1df0ee012d4a6e835b9694b7c85bb4",
                            "dateCreated": "2014-02-20T20:42:41+0000",
                            "lastUpdated": "2014-02-20T20:42:41+0000",
                            "version": 0
                        },
                        "firstResponse": true,
                        "version": 0,
                        "id": "3a1df0ee012d4a6e835b9694b7c85bb4",
                        "session": "463ea70206244f87a612393e4ad1647e",
                        "userId": "be5a7d28e9cd4166a5302dd38f342f15",
                        "dateCreated": "2014-02-20T20:42:41+0000",
                        "claimed": false,
                        "attributes": {
                            "claimed": false,
                            "answerGiven": "3",
                            "firstResponse": true
                        },
                        "answerGiven": "3"
                    }
                },
                {
                    "id": "217f7aa6d76949f29e881a7f707d3d75",
                    "response": {
                        "questionId": "217f7aa6d76949f29e881a7f707d3d75",
                        "count": null,
                        "status": "correct",
                        "lastUpdated": "2014-02-20T20:43:18+0000",
                        "parent": {
                            "id": "7592a078b0ef48f4aefaefc897e9b86d",
                            "dateCreated": "2014-02-20T20:43:18+0000",
                            "lastUpdated": "2014-02-20T20:43:18+0000",
                            "version": 0
                        },
                        "firstResponse": true,
                        "version": 0,
                        "id": "7592a078b0ef48f4aefaefc897e9b86d",
                        "session": "463ea70206244f87a612393e4ad1647e",
                        "userId": "be5a7d28e9cd4166a5302dd38f342f15",
                        "dateCreated": "2014-02-20T20:43:18+0000",
                        "claimed": false,
                        "attributes": {
                            "claimed": false,
                            "answerGiven": "3",
                            "firstResponse": true
                        },
                        "answerGiven": "3"
                    }
                },
                {
                    "id": "dc2b7a7487a44c64963258691be51933",
                    "response": {
                        "questionId": "dc2b7a7487a44c64963258691be51933",
                        "count": null,
                        "status": "correct",
                        "lastUpdated": "2014-02-20T20:43:35+0000",
                        "parent": {
                            "id": "ea9589d2c5724bb7b1fe8285f93b82de",
                            "dateCreated": "2014-02-20T20:43:35+0000",
                            "lastUpdated": "2014-02-20T20:43:35+0000",
                            "version": 0
                        },
                        "firstResponse": true,
                        "version": 0,
                        "id": "ea9589d2c5724bb7b1fe8285f93b82de",
                        "session": "463ea70206244f87a612393e4ad1647e",
                        "userId": "be5a7d28e9cd4166a5302dd38f342f15",
                        "dateCreated": "2014-02-20T20:43:35+0000",
                        "claimed": false,
                        "attributes": {
                            "claimed": false,
                            "answerGiven": "3",
                            "firstResponse": true
                        },
                        "answerGiven": "3"
                    }
                },
                {
                    "id": "c0b4f1b354cd4e298f4ead6ac8b1f47c",
                    "response": {
                        "questionId": "c0b4f1b354cd4e298f4ead6ac8b1f47c",
                        "count": null,
                        "status": "correct",
                        "lastUpdated": "2014-02-20T20:43:47+0000",
                        "parent": {
                            "id": "60c14908edc445a2b644c4922e808b61",
                            "dateCreated": "2014-02-20T20:43:47+0000",
                            "lastUpdated": "2014-02-20T20:43:47+0000",
                            "version": 0
                        },
                        "firstResponse": true,
                        "version": 0,
                        "id": "60c14908edc445a2b644c4922e808b61",
                        "session": "463ea70206244f87a612393e4ad1647e",
                        "userId": "be5a7d28e9cd4166a5302dd38f342f15",
                        "dateCreated": "2014-02-20T20:43:47+0000",
                        "claimed": false,
                        "attributes": {
                            "claimed": false,
                            "answerGiven": "5",
                            "firstResponse": true
                        },
                        "answerGiven": "5"
                    }
                },
                {
                    "id": "aeaa2ab0bab441ef96579ea925eea34a",
                    "response": {
                        "questionId": "aeaa2ab0bab441ef96579ea925eea34a",
                        "count": null,
                        "status": "correct",
                        "lastUpdated": "2014-02-20T20:43:54+0000",
                        "parent": {
                            "id": "483143a82f4d437db6095fb9f2cb7b8f",
                            "dateCreated": "2014-02-20T20:43:54+0000",
                            "lastUpdated": "2014-02-20T20:43:54+0000",
                            "version": 0
                        },
                        "firstResponse": true,
                        "version": 0,
                        "id": "483143a82f4d437db6095fb9f2cb7b8f",
                        "session": "463ea70206244f87a612393e4ad1647e",
                        "userId": "be5a7d28e9cd4166a5302dd38f342f15",
                        "dateCreated": "2014-02-20T20:43:54+0000",
                        "claimed": false,
                        "attributes": {
                            "claimed": false,
                            "answerGiven": "1",
                            "firstResponse": true
                        },
                        "answerGiven": "1"
                    }
                },
                {
                    "id": "6f95b3533e0d402b84885630129f771d",
                    "response": {
                        "questionId": "6f95b3533e0d402b84885630129f771d",
                        "count": null,
                        "status": "correct",
                        "lastUpdated": "2014-02-20T20:44:03+0000",
                        "parent": {
                            "id": "2a3820d026e249fc8c37bd1fa0428208",
                            "dateCreated": "2014-02-20T20:44:03+0000",
                            "lastUpdated": "2014-02-20T20:44:03+0000",
                            "version": 0
                        },
                        "firstResponse": true,
                        "version": 0,
                        "id": "2a3820d026e249fc8c37bd1fa0428208",
                        "session": "463ea70206244f87a612393e4ad1647e",
                        "userId": "be5a7d28e9cd4166a5302dd38f342f15",
                        "dateCreated": "2014-02-20T20:44:03+0000",
                        "claimed": false,
                        "attributes": {
                            "claimed": false,
                            "answerGiven": "3",
                            "firstResponse": true
                        },
                        "answerGiven": "3"
                    }
                },
                {
                    "id": "50e4d47a8fc84a46ac61c116691d7ebe",
                    "response": {
                        "questionId": "50e4d47a8fc84a46ac61c116691d7ebe",
                        "count": null,
                        "status": "correct",
                        "lastUpdated": "2014-02-20T20:44:12+0000",
                        "parent": {
                            "id": "835589e0349e429fb25faca582376d57",
                            "dateCreated": "2014-02-20T20:44:12+0000",
                            "lastUpdated": "2014-02-20T20:44:12+0000",
                            "version": 0
                        },
                        "firstResponse": true,
                        "version": 0,
                        "id": "835589e0349e429fb25faca582376d57",
                        "session": "463ea70206244f87a612393e4ad1647e",
                        "userId": "be5a7d28e9cd4166a5302dd38f342f15",
                        "dateCreated": "2014-02-20T20:44:12+0000",
                        "claimed": false,
                        "attributes": {
                            "claimed": false,
                            "answerGiven": "4",
                            "firstResponse": true
                        },
                        "answerGiven": "4"
                    }
                },
                {
                    "id": "24b77fb46ce34b5b92017f1fa80d0e01"
                }
            ],
            "examId": "7b2be084-4830-4ac9-a2f4-5180539e82a3",
            "programType": "custom",
            "custom": true,
            "dateCompleted": "2014-02-20T20:44:38.006Z"
        },
        "lastUpdated": "2014-02-20T20:44:38+0000"
    },
    {
        "userId": "be5a7d28e9cd4166a5302dd38f342f15",
        "status": "closed",
        "dateCreated": "2014-02-20T20:30:51+0000",
        "id": "8b1ff8f4c14e4b2abe79a80183ecc1c8",
        "version": 24,
        "attributes": {
            "feedback": [
                {
                    "answerGiven": "5"
                },
                {
                    "answerGiven": "5"
                },
                {
                    "answerGiven": "5"
                },
                {
                    "extraFeedbackGiven": ""
                }
            ],
            "title": "Custom Course, gerd,  20 Feb  2014 3:30 ",
            "elapsedSeconds": 423,
            "score": "91.3",
            "questions": [
                {
                    "id": "9a363ac6640d463b885194888c894d31",
                    "response": {
                        "questionId": "9a363ac6640d463b885194888c894d31",
                        "count": null,
                        "status": "correct",
                        "lastUpdated": "2014-02-20T20:30:56+0000",
                        "parent": {
                            "id": "f6039c2841744fb1bc0aa2f8086de478",
                            "dateCreated": "2014-02-20T20:30:56+0000",
                            "lastUpdated": "2014-02-20T20:30:56+0000",
                            "version": 0
                        },
                        "firstResponse": true,
                        "version": 0,
                        "id": "f6039c2841744fb1bc0aa2f8086de478",
                        "session": "8b1ff8f4c14e4b2abe79a80183ecc1c8",
                        "userId": "be5a7d28e9cd4166a5302dd38f342f15",
                        "dateCreated": "2014-02-20T20:30:56+0000",
                        "claimed": false,
                        "attributes": {
                            "claimed": false,
                            "answerGiven": "5",
                            "firstResponse": true
                        },
                        "answerGiven": "5"
                    }
                },
                {
                    "id": "ee6107aaeea34aa0a64cac54ff86feb5",
                    "response": {
                        "questionId": "ee6107aaeea34aa0a64cac54ff86feb5",
                        "count": null,
                        "status": "correct",
                        "lastUpdated": "2014-02-20T20:31:14+0000",
                        "parent": {
                            "id": "4b6ac99938bb443ca32f05e8a495f068",
                            "dateCreated": "2014-02-20T20:31:14+0000",
                            "lastUpdated": "2014-02-20T20:31:14+0000",
                            "version": 0
                        },
                        "firstResponse": true,
                        "version": 0,
                        "id": "4b6ac99938bb443ca32f05e8a495f068",
                        "session": "8b1ff8f4c14e4b2abe79a80183ecc1c8",
                        "userId": "be5a7d28e9cd4166a5302dd38f342f15",
                        "dateCreated": "2014-02-20T20:31:14+0000",
                        "claimed": false,
                        "attributes": {
                            "claimed": false,
                            "answerGiven": "3",
                            "firstResponse": true
                        },
                        "answerGiven": "3"
                    }
                },
                {
                    "id": "dd057ea7f9d14d7c8df380dd005bf8c6",
                    "response": {
                        "questionId": "dd057ea7f9d14d7c8df380dd005bf8c6",
                        "count": null,
                        "status": "correct",
                        "lastUpdated": "2014-02-20T20:31:32+0000",
                        "parent": {
                            "id": "b341fe7fe85d47d4bb356036a0fd790e",
                            "dateCreated": "2014-02-20T20:31:32+0000",
                            "lastUpdated": "2014-02-20T20:31:32+0000",
                            "version": 0
                        },
                        "firstResponse": true,
                        "version": 0,
                        "id": "b341fe7fe85d47d4bb356036a0fd790e",
                        "session": "8b1ff8f4c14e4b2abe79a80183ecc1c8",
                        "userId": "be5a7d28e9cd4166a5302dd38f342f15",
                        "dateCreated": "2014-02-20T20:31:32+0000",
                        "claimed": false,
                        "attributes": {
                            "claimed": false,
                            "answerGiven": "4",
                            "firstResponse": true
                        },
                        "answerGiven": "4"
                    }
                },
                {
                    "id": "db2a33fb103f46c1b99a1597f8bd0747",
                    "response": {
                        "questionId": "db2a33fb103f46c1b99a1597f8bd0747",
                        "count": null,
                        "status": "correct",
                        "lastUpdated": "2014-02-20T20:31:44+0000",
                        "parent": {
                            "id": "93fc01239da84b79b7950a1d314abcc2",
                            "dateCreated": "2014-02-20T20:31:44+0000",
                            "lastUpdated": "2014-02-20T20:31:44+0000",
                            "version": 0
                        },
                        "firstResponse": true,
                        "version": 0,
                        "id": "93fc01239da84b79b7950a1d314abcc2",
                        "session": "8b1ff8f4c14e4b2abe79a80183ecc1c8",
                        "userId": "be5a7d28e9cd4166a5302dd38f342f15",
                        "dateCreated": "2014-02-20T20:31:44+0000",
                        "claimed": false,
                        "attributes": {
                            "claimed": false,
                            "answerGiven": "5",
                            "firstResponse": true
                        },
                        "answerGiven": "5"
                    }
                },
                {
                    "id": "d9b72e731a7a4294a5c8588c5c6557f7",
                    "response": {
                        "questionId": "d9b72e731a7a4294a5c8588c5c6557f7",
                        "count": null,
                        "status": "correct",
                        "lastUpdated": "2014-02-20T20:31:53+0000",
                        "parent": {
                            "id": "9c671018f64c442ba7e74a6262414d11",
                            "dateCreated": "2014-02-20T20:31:53+0000",
                            "lastUpdated": "2014-02-20T20:31:53+0000",
                            "version": 0
                        },
                        "firstResponse": true,
                        "version": 0,
                        "id": "9c671018f64c442ba7e74a6262414d11",
                        "session": "8b1ff8f4c14e4b2abe79a80183ecc1c8",
                        "userId": "be5a7d28e9cd4166a5302dd38f342f15",
                        "dateCreated": "2014-02-20T20:31:53+0000",
                        "claimed": false,
                        "attributes": {
                            "claimed": false,
                            "answerGiven": "2",
                            "firstResponse": true
                        },
                        "answerGiven": "2"
                    }
                },
                {
                    "id": "d7ed3840a98541c7aaf2471ca3cfcbfe",
                    "response": {
                        "questionId": "d7ed3840a98541c7aaf2471ca3cfcbfe",
                        "count": null,
                        "status": "correct",
                        "lastUpdated": "2014-02-20T20:32:09+0000",
                        "parent": {
                            "id": "2679d3027dbe47dd8783f081e137be96",
                            "dateCreated": "2014-02-20T20:32:09+0000",
                            "lastUpdated": "2014-02-20T20:32:09+0000",
                            "version": 0
                        },
                        "firstResponse": true,
                        "version": 0,
                        "id": "2679d3027dbe47dd8783f081e137be96",
                        "session": "8b1ff8f4c14e4b2abe79a80183ecc1c8",
                        "userId": "be5a7d28e9cd4166a5302dd38f342f15",
                        "dateCreated": "2014-02-20T20:32:09+0000",
                        "claimed": false,
                        "attributes": {
                            "claimed": false,
                            "answerGiven": "3",
                            "firstResponse": true
                        },
                        "answerGiven": "3"
                    }
                },
                {
                    "id": "c6311ab06045402499f07d41e954e5a2",
                    "response": {
                        "questionId": "c6311ab06045402499f07d41e954e5a2",
                        "count": null,
                        "status": "correct",
                        "lastUpdated": "2014-02-20T20:32:16+0000",
                        "parent": {
                            "id": "6bc960df2a86480291c5095b368c8a94",
                            "dateCreated": "2014-02-20T20:32:16+0000",
                            "lastUpdated": "2014-02-20T20:32:16+0000",
                            "version": 0
                        },
                        "firstResponse": true,
                        "version": 0,
                        "id": "6bc960df2a86480291c5095b368c8a94",
                        "session": "8b1ff8f4c14e4b2abe79a80183ecc1c8",
                        "userId": "be5a7d28e9cd4166a5302dd38f342f15",
                        "dateCreated": "2014-02-20T20:32:16+0000",
                        "claimed": false,
                        "attributes": {
                            "claimed": false,
                            "answerGiven": "5",
                            "firstResponse": true
                        },
                        "answerGiven": "5"
                    }
                },
                {
                    "id": "be2843d2073248198a14e736215a0c90",
                    "response": {
                        "questionId": "be2843d2073248198a14e736215a0c90",
                        "count": null,
                        "status": "correct",
                        "lastUpdated": "2014-02-20T20:32:39+0000",
                        "parent": {
                            "id": "22c870b7de134056abd4fb8db65e96c1",
                            "dateCreated": "2014-02-20T20:32:39+0000",
                            "lastUpdated": "2014-02-20T20:32:39+0000",
                            "version": 0
                        },
                        "firstResponse": true,
                        "version": 0,
                        "id": "22c870b7de134056abd4fb8db65e96c1",
                        "session": "8b1ff8f4c14e4b2abe79a80183ecc1c8",
                        "userId": "be5a7d28e9cd4166a5302dd38f342f15",
                        "dateCreated": "2014-02-20T20:32:39+0000",
                        "claimed": false,
                        "attributes": {
                            "claimed": false,
                            "answerGiven": "4",
                            "firstResponse": true
                        },
                        "answerGiven": "4"
                    }
                },
                {
                    "id": "a3d72fb0de4c4c7cb4e44bb3e8559d3b",
                    "response": {
                        "questionId": "a3d72fb0de4c4c7cb4e44bb3e8559d3b",
                        "count": null,
                        "status": "correct",
                        "lastUpdated": "2014-02-20T20:32:57+0000",
                        "parent": {
                            "id": "20240d7b50a0406e8e25839ae25941c7",
                            "dateCreated": "2014-02-20T20:32:57+0000",
                            "lastUpdated": "2014-02-20T20:32:57+0000",
                            "version": 0
                        },
                        "firstResponse": true,
                        "version": 0,
                        "id": "20240d7b50a0406e8e25839ae25941c7",
                        "session": "8b1ff8f4c14e4b2abe79a80183ecc1c8",
                        "userId": "be5a7d28e9cd4166a5302dd38f342f15",
                        "dateCreated": "2014-02-20T20:32:57+0000",
                        "claimed": false,
                        "attributes": {
                            "claimed": false,
                            "answerGiven": "5",
                            "firstResponse": true
                        },
                        "answerGiven": "5"
                    }
                },
                {
                    "id": "9d1b03bc6b534ac0a43015ef32c10d0f",
                    "response": {
                        "questionId": "9d1b03bc6b534ac0a43015ef32c10d0f",
                        "count": null,
                        "status": "correct",
                        "lastUpdated": "2014-02-20T20:33:04+0000",
                        "parent": {
                            "id": "a8e8336b12d0492a84a3be889dc596c0",
                            "dateCreated": "2014-02-20T20:33:04+0000",
                            "lastUpdated": "2014-02-20T20:33:04+0000",
                            "version": 0
                        },
                        "firstResponse": true,
                        "version": 0,
                        "id": "a8e8336b12d0492a84a3be889dc596c0",
                        "session": "8b1ff8f4c14e4b2abe79a80183ecc1c8",
                        "userId": "be5a7d28e9cd4166a5302dd38f342f15",
                        "dateCreated": "2014-02-20T20:33:04+0000",
                        "claimed": false,
                        "attributes": {
                            "claimed": false,
                            "answerGiven": "1",
                            "firstResponse": true
                        },
                        "answerGiven": "1"
                    }
                },
                {
                    "id": "9ca05268882b413ab3fcf6157197cef8",
                    "response": {
                        "questionId": "9ca05268882b413ab3fcf6157197cef8",
                        "count": null,
                        "status": "correct",
                        "lastUpdated": "2014-02-20T20:33:17+0000",
                        "parent": {
                            "id": "73fb07382c30430bacdf2e5c53f65c35",
                            "dateCreated": "2014-02-20T20:33:17+0000",
                            "lastUpdated": "2014-02-20T20:33:17+0000",
                            "version": 0
                        },
                        "firstResponse": true,
                        "version": 0,
                        "id": "73fb07382c30430bacdf2e5c53f65c35",
                        "session": "8b1ff8f4c14e4b2abe79a80183ecc1c8",
                        "userId": "be5a7d28e9cd4166a5302dd38f342f15",
                        "dateCreated": "2014-02-20T20:33:17+0000",
                        "claimed": false,
                        "attributes": {
                            "claimed": false,
                            "answerGiven": "2",
                            "firstResponse": true
                        },
                        "answerGiven": "2"
                    }
                },
                {
                    "id": "ec3af1d35ca1428dab615830b6ac5732",
                    "response": {
                        "questionId": "ec3af1d35ca1428dab615830b6ac5732",
                        "count": null,
                        "status": "correct",
                        "lastUpdated": "2014-02-20T20:33:26+0000",
                        "parent": {
                            "id": "f6204176232242e4a5b152832e3ca136",
                            "dateCreated": "2014-02-20T20:33:26+0000",
                            "lastUpdated": "2014-02-20T20:33:26+0000",
                            "version": 0
                        },
                        "firstResponse": true,
                        "version": 0,
                        "id": "f6204176232242e4a5b152832e3ca136",
                        "session": "8b1ff8f4c14e4b2abe79a80183ecc1c8",
                        "userId": "be5a7d28e9cd4166a5302dd38f342f15",
                        "dateCreated": "2014-02-20T20:33:26+0000",
                        "claimed": false,
                        "attributes": {
                            "claimed": false,
                            "answerGiven": "1",
                            "firstResponse": true
                        },
                        "answerGiven": "1"
                    }
                },
                {
                    "id": "92dbdc60fea641c5a1c6bbb510e351dd",
                    "response": {
                        "questionId": "92dbdc60fea641c5a1c6bbb510e351dd",
                        "count": null,
                        "status": "correct",
                        "lastUpdated": "2014-02-20T20:33:33+0000",
                        "parent": {
                            "id": "48f4625b28d846e4aa0e543bc38c9024",
                            "dateCreated": "2014-02-20T20:33:33+0000",
                            "lastUpdated": "2014-02-20T20:33:33+0000",
                            "version": 0
                        },
                        "firstResponse": true,
                        "version": 0,
                        "id": "48f4625b28d846e4aa0e543bc38c9024",
                        "session": "8b1ff8f4c14e4b2abe79a80183ecc1c8",
                        "userId": "be5a7d28e9cd4166a5302dd38f342f15",
                        "dateCreated": "2014-02-20T20:33:33+0000",
                        "claimed": false,
                        "attributes": {
                            "claimed": false,
                            "answerGiven": "3",
                            "firstResponse": true
                        },
                        "answerGiven": "3"
                    }
                },
                {
                    "id": "90bd7c790ec840a1a20ceb08fdbab273",
                    "response": {
                        "questionId": "90bd7c790ec840a1a20ceb08fdbab273",
                        "count": null,
                        "status": "correct",
                        "lastUpdated": "2014-02-20T20:33:38+0000",
                        "parent": {
                            "id": "a47f3de6e93844fb88a45ab88599e568",
                            "dateCreated": "2014-02-20T20:33:38+0000",
                            "lastUpdated": "2014-02-20T20:33:38+0000",
                            "version": 0
                        },
                        "firstResponse": true,
                        "version": 0,
                        "id": "a47f3de6e93844fb88a45ab88599e568",
                        "session": "8b1ff8f4c14e4b2abe79a80183ecc1c8",
                        "userId": "be5a7d28e9cd4166a5302dd38f342f15",
                        "dateCreated": "2014-02-20T20:33:38+0000",
                        "claimed": false,
                        "attributes": {
                            "claimed": false,
                            "answerGiven": "5",
                            "firstResponse": true
                        },
                        "answerGiven": "5"
                    }
                },
                {
                    "id": "5bbf3fab9fdc42d499b8f643701ff037",
                    "response": {
                        "questionId": "5bbf3fab9fdc42d499b8f643701ff037",
                        "count": null,
                        "status": "correct",
                        "lastUpdated": "2014-02-20T20:33:44+0000",
                        "parent": {
                            "id": "033b19ffacd24519980a6e467d4b2b84",
                            "dateCreated": "2014-02-20T20:33:44+0000",
                            "lastUpdated": "2014-02-20T20:33:44+0000",
                            "version": 0
                        },
                        "firstResponse": true,
                        "version": 0,
                        "id": "033b19ffacd24519980a6e467d4b2b84",
                        "session": "8b1ff8f4c14e4b2abe79a80183ecc1c8",
                        "userId": "be5a7d28e9cd4166a5302dd38f342f15",
                        "dateCreated": "2014-02-20T20:33:44+0000",
                        "claimed": false,
                        "attributes": {
                            "claimed": false,
                            "answerGiven": "4",
                            "firstResponse": true
                        },
                        "answerGiven": "4"
                    }
                },
                {
                    "id": "4d078917529c45dcafbe1085ce084240",
                    "response": {
                        "questionId": "4d078917529c45dcafbe1085ce084240",
                        "count": null,
                        "status": "correct",
                        "lastUpdated": "2014-02-20T20:34:33+0000",
                        "parent": {
                            "id": "4982a4be34034cefadff89206f05cc4d",
                            "dateCreated": "2014-02-20T20:34:33+0000",
                            "lastUpdated": "2014-02-20T20:34:33+0000",
                            "version": 0
                        },
                        "firstResponse": true,
                        "version": 0,
                        "id": "4982a4be34034cefadff89206f05cc4d",
                        "session": "8b1ff8f4c14e4b2abe79a80183ecc1c8",
                        "userId": "be5a7d28e9cd4166a5302dd38f342f15",
                        "dateCreated": "2014-02-20T20:34:33+0000",
                        "claimed": false,
                        "attributes": {
                            "claimed": false,
                            "answerGiven": "3",
                            "firstResponse": true
                        },
                        "answerGiven": "3"
                    }
                },
                {
                    "id": "4b40f2e284924f3eba086d06eba18253",
                    "response": {
                        "questionId": "4b40f2e284924f3eba086d06eba18253",
                        "count": null,
                        "status": "incorrect",
                        "lastUpdated": "2014-02-20T20:35:08+0000",
                        "parent": {
                            "id": "6d7c03f7319e450792c170ce13b3532c",
                            "dateCreated": "2014-02-20T20:35:08+0000",
                            "lastUpdated": "2014-02-20T20:35:08+0000",
                            "version": 0
                        },
                        "firstResponse": true,
                        "version": 0,
                        "id": "6d7c03f7319e450792c170ce13b3532c",
                        "session": "8b1ff8f4c14e4b2abe79a80183ecc1c8",
                        "userId": "be5a7d28e9cd4166a5302dd38f342f15",
                        "dateCreated": "2014-02-20T20:35:08+0000",
                        "claimed": false,
                        "attributes": {
                            "claimed": false,
                            "answerGiven": "4",
                            "firstResponse": true
                        },
                        "answerGiven": "4"
                    }
                },
                {
                    "id": "41c94e9cf25743d4954abad7376ac79b",
                    "response": {
                        "questionId": "41c94e9cf25743d4954abad7376ac79b",
                        "count": null,
                        "status": "incorrect",
                        "lastUpdated": "2014-02-20T20:35:52+0000",
                        "parent": {
                            "id": "8ae98daf9a3e4d739ad4a67cb935e8da",
                            "dateCreated": "2014-02-20T20:35:52+0000",
                            "lastUpdated": "2014-02-20T20:35:52+0000",
                            "version": 0
                        },
                        "firstResponse": true,
                        "version": 0,
                        "id": "8ae98daf9a3e4d739ad4a67cb935e8da",
                        "session": "8b1ff8f4c14e4b2abe79a80183ecc1c8",
                        "userId": "be5a7d28e9cd4166a5302dd38f342f15",
                        "dateCreated": "2014-02-20T20:35:52+0000",
                        "claimed": false,
                        "attributes": {
                            "claimed": false,
                            "answerGiven": "3",
                            "firstResponse": true
                        },
                        "answerGiven": "3"
                    }
                },
                {
                    "id": "312665758c0b4a3ebd06576b83013670",
                    "response": {
                        "questionId": "312665758c0b4a3ebd06576b83013670",
                        "count": null,
                        "status": "correct",
                        "lastUpdated": "2014-02-20T20:36:43+0000",
                        "parent": {
                            "id": "9f0d35108ea24626ae574ded92eebf02",
                            "dateCreated": "2014-02-20T20:36:43+0000",
                            "lastUpdated": "2014-02-20T20:36:43+0000",
                            "version": 0
                        },
                        "firstResponse": true,
                        "version": 0,
                        "id": "9f0d35108ea24626ae574ded92eebf02",
                        "session": "8b1ff8f4c14e4b2abe79a80183ecc1c8",
                        "userId": "be5a7d28e9cd4166a5302dd38f342f15",
                        "dateCreated": "2014-02-20T20:36:43+0000",
                        "claimed": false,
                        "attributes": {
                            "claimed": false,
                            "answerGiven": "4",
                            "firstResponse": true
                        },
                        "answerGiven": "4"
                    }
                },
                {
                    "id": "169175037ab04f6a927045a2e106cb46",
                    "response": {
                        "questionId": "169175037ab04f6a927045a2e106cb46",
                        "count": null,
                        "status": "correct",
                        "lastUpdated": "2014-02-20T20:36:53+0000",
                        "parent": {
                            "id": "bfc66ebd1a444b5fa32e684f2357135b",
                            "dateCreated": "2014-02-20T20:36:53+0000",
                            "lastUpdated": "2014-02-20T20:36:53+0000",
                            "version": 0
                        },
                        "firstResponse": true,
                        "version": 0,
                        "id": "bfc66ebd1a444b5fa32e684f2357135b",
                        "session": "8b1ff8f4c14e4b2abe79a80183ecc1c8",
                        "userId": "be5a7d28e9cd4166a5302dd38f342f15",
                        "dateCreated": "2014-02-20T20:36:53+0000",
                        "claimed": false,
                        "attributes": {
                            "claimed": false,
                            "answerGiven": "1",
                            "firstResponse": true
                        },
                        "answerGiven": "1"
                    }
                },
                {
                    "id": "111850cb269f4c2badae0fb04d35d727",
                    "response": {
                        "questionId": "111850cb269f4c2badae0fb04d35d727",
                        "count": null,
                        "status": "correct",
                        "lastUpdated": "2014-02-20T20:37:18+0000",
                        "parent": {
                            "id": "3959299d1e3e47bfb60b1f8d0c079833",
                            "dateCreated": "2014-02-20T20:37:18+0000",
                            "lastUpdated": "2014-02-20T20:37:18+0000",
                            "version": 0
                        },
                        "firstResponse": true,
                        "version": 0,
                        "id": "3959299d1e3e47bfb60b1f8d0c079833",
                        "session": "8b1ff8f4c14e4b2abe79a80183ecc1c8",
                        "userId": "be5a7d28e9cd4166a5302dd38f342f15",
                        "dateCreated": "2014-02-20T20:37:18+0000",
                        "claimed": false,
                        "attributes": {
                            "claimed": false,
                            "answerGiven": "3",
                            "firstResponse": true
                        },
                        "answerGiven": "3"
                    }
                },
                {
                    "id": "10c99ea4140f45acb096c190692b5de9",
                    "response": {
                        "questionId": "10c99ea4140f45acb096c190692b5de9",
                        "count": null,
                        "status": "correct",
                        "lastUpdated": "2014-02-20T20:37:51+0000",
                        "parent": {
                            "id": "52bb7a3e56d6414cb9c36ccdcafada86",
                            "dateCreated": "2014-02-20T20:37:51+0000",
                            "lastUpdated": "2014-02-20T20:37:51+0000",
                            "version": 0
                        },
                        "firstResponse": true,
                        "version": 0,
                        "id": "52bb7a3e56d6414cb9c36ccdcafada86",
                        "session": "8b1ff8f4c14e4b2abe79a80183ecc1c8",
                        "userId": "be5a7d28e9cd4166a5302dd38f342f15",
                        "dateCreated": "2014-02-20T20:37:51+0000",
                        "claimed": false,
                        "attributes": {
                            "claimed": false,
                            "answerGiven": "4",
                            "firstResponse": true
                        },
                        "answerGiven": "4"
                    }
                },
                {
                    "id": "0030ce39f4814189bf15768a05c6c965"
                }
            ],
            "examId": "df992f4d-af16-44ac-84ad-c1c1fb82edd4",
            "programType": "custom",
            "custom": true,
            "dateCompleted": "2014-02-20T20:38:07.276Z"
        },
        "lastUpdated": "2014-02-20T20:38:07+0000"
    },
    {
        "userId": "be5a7d28e9cd4166a5302dd38f342f15",
        "status": "closed",
        "dateCreated": "2014-02-20T20:24:21+0000",
        "id": "79a3057392e54ec0bea0a548ee512376",
        "version": 15,
        "attributes": {
            "feedback": [
                {
                    "answerGiven": "5"
                },
                {
                    "answerGiven": "5"
                },
                {
                    "answerGiven": "5"
                },
                {
                    "extraFeedbackGiven": ""
                }
            ],
            "title": "Random Course, 20 Feb  2014 3:24 ",
            "elapsedSeconds": 232,
            "score": "100",
            "questions": [
                {
                    "id": "35997405f7ae4654aaf02efdf5a3e814",
                    "response": {
                        "questionId": "35997405f7ae4654aaf02efdf5a3e814",
                        "count": null,
                        "status": "correct",
                        "lastUpdated": "2014-02-20T20:28:05+0000",
                        "parent": {
                            "id": "f65323210f614c288efb8f3095c1819c",
                            "dateCreated": "2014-02-20T20:28:05+0000",
                            "lastUpdated": "2014-02-20T20:28:05+0000",
                            "version": 0
                        },
                        "firstResponse": false,
                        "version": 0,
                        "id": "f65323210f614c288efb8f3095c1819c",
                        "session": "79a3057392e54ec0bea0a548ee512376",
                        "userId": "be5a7d28e9cd4166a5302dd38f342f15",
                        "dateCreated": "2014-02-20T20:28:05+0000",
                        "claimed": false,
                        "attributes": {
                            "claimed": false,
                            "answerGiven": "2",
                            "firstResponse": false
                        },
                        "answerGiven": "2"
                    }
                },
                {
                    "id": "ccfe13ee8725486484ba11a59ff023a7",
                    "response": {
                        "questionId": "ccfe13ee8725486484ba11a59ff023a7",
                        "count": null,
                        "status": "correct",
                        "lastUpdated": "2014-02-20T20:24:43+0000",
                        "parent": {
                            "id": "a9c08e32e2544bc683d08212e608dc50",
                            "dateCreated": "2014-02-20T20:24:43+0000",
                            "lastUpdated": "2014-02-20T20:24:43+0000",
                            "version": 0
                        },
                        "firstResponse": true,
                        "version": 0,
                        "id": "a9c08e32e2544bc683d08212e608dc50",
                        "session": "79a3057392e54ec0bea0a548ee512376",
                        "userId": "be5a7d28e9cd4166a5302dd38f342f15",
                        "dateCreated": "2014-02-20T20:24:43+0000",
                        "claimed": false,
                        "attributes": {
                            "claimed": false,
                            "answerGiven": "1",
                            "firstResponse": true
                        },
                        "answerGiven": "1"
                    }
                },
                {
                    "id": "a3d72fb0de4c4c7cb4e44bb3e8559d3b",
                    "response": {
                        "questionId": "a3d72fb0de4c4c7cb4e44bb3e8559d3b",
                        "count": null,
                        "status": "correct",
                        "lastUpdated": "2014-02-20T20:26:28+0000",
                        "parent": {
                            "id": "486d8878f2b24700b11cabb4bd697e5c",
                            "dateCreated": "2014-02-20T20:26:28+0000",
                            "lastUpdated": "2014-02-20T20:26:28+0000",
                            "version": 0
                        },
                        "firstResponse": false,
                        "version": 0,
                        "id": "486d8878f2b24700b11cabb4bd697e5c",
                        "session": "79a3057392e54ec0bea0a548ee512376",
                        "userId": "be5a7d28e9cd4166a5302dd38f342f15",
                        "dateCreated": "2014-02-20T20:26:28+0000",
                        "claimed": false,
                        "attributes": {
                            "claimed": false,
                            "answerGiven": "5",
                            "firstResponse": false
                        },
                        "answerGiven": "5"
                    }
                },
                {
                    "id": "92dbdc60fea641c5a1c6bbb510e351dd",
                    "response": {
                        "questionId": "92dbdc60fea641c5a1c6bbb510e351dd",
                        "count": null,
                        "status": "correct",
                        "lastUpdated": "2014-02-20T20:25:08+0000",
                        "parent": {
                            "id": "2a976c4028144ed282d9e84423e2f790",
                            "dateCreated": "2014-02-20T20:25:08+0000",
                            "lastUpdated": "2014-02-20T20:25:08+0000",
                            "version": 0
                        },
                        "firstResponse": true,
                        "version": 0,
                        "id": "2a976c4028144ed282d9e84423e2f790",
                        "session": "79a3057392e54ec0bea0a548ee512376",
                        "userId": "be5a7d28e9cd4166a5302dd38f342f15",
                        "dateCreated": "2014-02-20T20:25:08+0000",
                        "claimed": false,
                        "attributes": {
                            "claimed": false,
                            "answerGiven": "3",
                            "firstResponse": true
                        },
                        "answerGiven": "3"
                    }
                },
                {
                    "id": "e1aaf3bc0b7e4984a696475dcdf11397",
                    "response": {
                        "questionId": "e1aaf3bc0b7e4984a696475dcdf11397",
                        "count": null,
                        "status": "correct",
                        "lastUpdated": "2014-02-20T20:27:02+0000",
                        "parent": {
                            "id": "553b3875fc624dc0b47e0ae6be577c5d",
                            "dateCreated": "2014-02-20T20:27:02+0000",
                            "lastUpdated": "2014-02-20T20:27:02+0000",
                            "version": 0
                        },
                        "firstResponse": false,
                        "version": 0,
                        "id": "553b3875fc624dc0b47e0ae6be577c5d",
                        "session": "79a3057392e54ec0bea0a548ee512376",
                        "userId": "be5a7d28e9cd4166a5302dd38f342f15",
                        "dateCreated": "2014-02-20T20:27:02+0000",
                        "claimed": false,
                        "attributes": {
                            "claimed": false,
                            "answerGiven": "4",
                            "firstResponse": false
                        },
                        "answerGiven": "4"
                    }
                },
                {
                    "id": "a29ddced3d20440596c371a1ba0162ee",
                    "response": {
                        "questionId": "a29ddced3d20440596c371a1ba0162ee",
                        "count": null,
                        "status": "incorrect",
                        "lastUpdated": "2014-02-20T20:27:56+0000",
                        "parent": {
                            "id": "7f86ea65972f4e2a905bc9c51f4041dd",
                            "dateCreated": "2014-02-20T20:27:56+0000",
                            "lastUpdated": "2014-02-20T20:27:56+0000",
                            "version": 0
                        },
                        "firstResponse": false,
                        "version": 0,
                        "id": "7f86ea65972f4e2a905bc9c51f4041dd",
                        "session": "79a3057392e54ec0bea0a548ee512376",
                        "userId": "be5a7d28e9cd4166a5302dd38f342f15",
                        "dateCreated": "2014-02-20T20:27:56+0000",
                        "claimed": false,
                        "attributes": {
                            "claimed": false,
                            "answerGiven": "5",
                            "firstResponse": false
                        },
                        "answerGiven": "5"
                    }
                }
            ],
            "examId": "Random Course, 20 Feb  2014 3:24",
            "programType": "custom",
            "custom": true,
            "dateCompleted": "2014-02-20T20:28:46.842Z"
        },
        "lastUpdated": "2014-02-20T20:28:47+0000"
    },
    {
        "userId": "be5a7d28e9cd4166a5302dd38f342f15",
        "status": "closed",
        "dateCreated": "2014-02-20T20:20:12+0000",
        "id": "b3a13c7e11bd477b9488be600ea75152",
        "version": 18,
        "attributes": {
            "feedback": [
                {
                    "answerGiven": "5"
                },
                {
                    "answerGiven": "5"
                },
                {
                    "answerGiven": "5"
                },
                {
                    "extraFeedbackGiven": ""
                }
            ],
            "title": "Random Course, 20 Feb  2014 3:20 ",
            "elapsedSeconds": 213,
            "score": "75",
            "questions": [
                {
                    "id": "312665758c0b4a3ebd06576b83013670",
                    "response": {
                        "questionId": "312665758c0b4a3ebd06576b83013670",
                        "count": null,
                        "status": "correct",
                        "lastUpdated": "2014-02-20T20:20:20+0000",
                        "parent": {
                            "id": "4a499b30f2c44e488bfa87250b853647",
                            "dateCreated": "2014-02-20T20:20:20+0000",
                            "lastUpdated": "2014-02-20T20:20:20+0000",
                            "version": 0
                        },
                        "firstResponse": true,
                        "version": 0,
                        "id": "4a499b30f2c44e488bfa87250b853647",
                        "session": "b3a13c7e11bd477b9488be600ea75152",
                        "userId": "be5a7d28e9cd4166a5302dd38f342f15",
                        "dateCreated": "2014-02-20T20:20:20+0000",
                        "claimed": false,
                        "attributes": {
                            "claimed": false,
                            "answerGiven": "4",
                            "firstResponse": true
                        },
                        "answerGiven": "4"
                    }
                },
                {
                    "id": "25a566d8d4174009861d374c84443e51",
                    "response": {
                        "questionId": "25a566d8d4174009861d374c84443e51",
                        "count": null,
                        "status": "correct",
                        "lastUpdated": "2014-02-20T20:20:34+0000",
                        "parent": {
                            "id": "bf968cf1393e4ab1a1223167cfa1b486",
                            "dateCreated": "2014-02-20T20:20:34+0000",
                            "lastUpdated": "2014-02-20T20:20:34+0000",
                            "version": 0
                        },
                        "firstResponse": true,
                        "version": 0,
                        "id": "bf968cf1393e4ab1a1223167cfa1b486",
                        "session": "b3a13c7e11bd477b9488be600ea75152",
                        "userId": "be5a7d28e9cd4166a5302dd38f342f15",
                        "dateCreated": "2014-02-20T20:20:34+0000",
                        "claimed": false,
                        "attributes": {
                            "claimed": false,
                            "answerGiven": "4",
                            "firstResponse": true
                        },
                        "answerGiven": "4"
                    }
                },
                {
                    "id": "04a9fadaa9d941fab6fc8fb7855390ad",
                    "response": {
                        "questionId": "04a9fadaa9d941fab6fc8fb7855390ad",
                        "count": null,
                        "status": "correct",
                        "lastUpdated": "2014-02-20T20:22:45+0000",
                        "parent": {
                            "id": "d3218011b0e84fad8280fcc106ee0d3e",
                            "dateCreated": "2014-02-20T20:22:45+0000",
                            "lastUpdated": "2014-02-20T20:22:45+0000",
                            "version": 0
                        },
                        "firstResponse": false,
                        "version": 0,
                        "id": "d3218011b0e84fad8280fcc106ee0d3e",
                        "session": "b3a13c7e11bd477b9488be600ea75152",
                        "userId": "be5a7d28e9cd4166a5302dd38f342f15",
                        "dateCreated": "2014-02-20T20:22:45+0000",
                        "claimed": false,
                        "attributes": {
                            "claimed": false,
                            "answerGiven": "3",
                            "firstResponse": false
                        },
                        "answerGiven": "3"
                    }
                },
                {
                    "id": "a3d72fb0de4c4c7cb4e44bb3e8559d3b",
                    "response": {
                        "questionId": "a3d72fb0de4c4c7cb4e44bb3e8559d3b",
                        "count": null,
                        "status": "incorrect",
                        "lastUpdated": "2014-02-20T20:23:41+0000",
                        "parent": {
                            "id": "ba54e47fa7384ebd9ad4692bfa4dfd9e",
                            "dateCreated": "2014-02-20T20:23:41+0000",
                            "lastUpdated": "2014-02-20T20:23:41+0000",
                            "version": 0
                        },
                        "firstResponse": false,
                        "version": 0,
                        "id": "ba54e47fa7384ebd9ad4692bfa4dfd9e",
                        "session": "b3a13c7e11bd477b9488be600ea75152",
                        "userId": "be5a7d28e9cd4166a5302dd38f342f15",
                        "dateCreated": "2014-02-20T20:23:41+0000",
                        "claimed": false,
                        "attributes": {
                            "claimed": false,
                            "answerGiven": "1",
                            "firstResponse": false
                        },
                        "answerGiven": "1"
                    }
                },
                {
                    "id": "8c0461b6774f443e90a542844919b3cd",
                    "response": {
                        "questionId": "8c0461b6774f443e90a542844919b3cd",
                        "count": null,
                        "status": "correct",
                        "lastUpdated": "2014-02-20T20:23:24+0000",
                        "parent": {
                            "id": "72463376196640f4bcc1fe5abe3ad6cc",
                            "dateCreated": "2014-02-20T20:23:24+0000",
                            "lastUpdated": "2014-02-20T20:23:24+0000",
                            "version": 0
                        },
                        "firstResponse": false,
                        "version": 0,
                        "id": "72463376196640f4bcc1fe5abe3ad6cc",
                        "session": "b3a13c7e11bd477b9488be600ea75152",
                        "userId": "be5a7d28e9cd4166a5302dd38f342f15",
                        "dateCreated": "2014-02-20T20:23:24+0000",
                        "claimed": false,
                        "attributes": {
                            "claimed": false,
                            "answerGiven": "4",
                            "firstResponse": false
                        },
                        "answerGiven": "4"
                    }
                },
                {
                    "id": "e918a00abc3c420ab2832bd962781ea2",
                    "response": {
                        "questionId": "e918a00abc3c420ab2832bd962781ea2",
                        "count": null,
                        "status": "correct",
                        "lastUpdated": "2014-02-20T20:22:02+0000",
                        "parent": {
                            "id": "6063701deef2435696fb247dc1261ab5",
                            "dateCreated": "2014-02-20T20:22:02+0000",
                            "lastUpdated": "2014-02-20T20:22:02+0000",
                            "version": 0
                        },
                        "firstResponse": true,
                        "version": 0,
                        "id": "6063701deef2435696fb247dc1261ab5",
                        "session": "b3a13c7e11bd477b9488be600ea75152",
                        "userId": "be5a7d28e9cd4166a5302dd38f342f15",
                        "dateCreated": "2014-02-20T20:22:02+0000",
                        "claimed": false,
                        "attributes": {
                            "claimed": false,
                            "answerGiven": "4",
                            "firstResponse": true
                        },
                        "answerGiven": "4"
                    }
                },
                {
                    "id": "4f5028e59f6f4a56878f2bcd731bafd5",
                    "response": {
                        "questionId": "4f5028e59f6f4a56878f2bcd731bafd5",
                        "count": null,
                        "status": "correct",
                        "lastUpdated": "2014-02-20T20:22:28+0000",
                        "parent": {
                            "id": "df16f57f22a94819b3e1561ffcbc7a05",
                            "dateCreated": "2014-02-20T20:22:28+0000",
                            "lastUpdated": "2014-02-20T20:22:28+0000",
                            "version": 0
                        },
                        "firstResponse": true,
                        "version": 0,
                        "id": "df16f57f22a94819b3e1561ffcbc7a05",
                        "session": "b3a13c7e11bd477b9488be600ea75152",
                        "userId": "be5a7d28e9cd4166a5302dd38f342f15",
                        "dateCreated": "2014-02-20T20:22:28+0000",
                        "claimed": false,
                        "attributes": {
                            "claimed": false,
                            "answerGiven": "4",
                            "firstResponse": true
                        },
                        "answerGiven": "4"
                    }
                },
                {
                    "id": "3ca1e2a91b8a4b2f859339cc151c952b",
                    "response": {
                        "questionId": "3ca1e2a91b8a4b2f859339cc151c952b",
                        "count": null,
                        "status": "incorrect",
                        "lastUpdated": "2014-02-20T20:23:30+0000",
                        "parent": {
                            "id": "ea321dcae88d4513a575743be60ff435",
                            "dateCreated": "2014-02-20T20:23:30+0000",
                            "lastUpdated": "2014-02-20T20:23:30+0000",
                            "version": 0
                        },
                        "firstResponse": false,
                        "version": 0,
                        "id": "ea321dcae88d4513a575743be60ff435",
                        "session": "b3a13c7e11bd477b9488be600ea75152",
                        "userId": "be5a7d28e9cd4166a5302dd38f342f15",
                        "dateCreated": "2014-02-20T20:23:30+0000",
                        "claimed": false,
                        "attributes": {
                            "claimed": false,
                            "answerGiven": "1",
                            "firstResponse": false
                        },
                        "answerGiven": "1"
                    }
                }
            ],
            "examId": "Random Course, 20 Feb  2014 3:20",
            "programType": "custom",
            "custom": true,
            "dateCompleted": "2014-02-20T20:23:55.685Z"
        },
        "lastUpdated": "2014-02-20T20:23:56+0000"
    },
    {
        "userId": "be5a7d28e9cd4166a5302dd38f342f15",
        "status": "closed",
        "dateCreated": "2014-02-20T20:14:09+0000",
        "id": "e432e09ed65946c0ae1121ffa957142e",
        "version": 12,
        "attributes": {
            "feedback": [
                {
                    "answerGiven": "5"
                },
                {
                    "answerGiven": "5"
                },
                {
                    "answerGiven": "5"
                },
                {
                    "extraFeedbackGiven": ""
                }
            ],
            "title": "Evidence Review GERD",
            "elapsedSeconds": 300,
            "score": "81.8",
            "questions": [
                "92dbdc60fea641c5a1c6bbb510e351dd",
                "c6311ab06045402499f07d41e954e5a2",
                "d9b72e731a7a4294a5c8588c5c6557f7",
                "312665758c0b4a3ebd06576b83013670",
                "0030ce39f4814189bf15768a05c6c965",
                "169175037ab04f6a927045a2e106cb46",
                "9d1b03bc6b534ac0a43015ef32c10d0f",
                "5bbf3fab9fdc42d499b8f643701ff037",
                "90bd7c790ec840a1a20ceb08fdbab273",
                "9ca05268882b413ab3fcf6157197cef8",
                "ec3af1d35ca1428dab615830b6ac5732"
            ],
            "examId": "e0f0ea57168743f9af8e73ec31f24d15",
            "programType": "evidence",
            "custom": false,
            "dateCompleted": "2014-02-20T20:19:20.284Z"
        },
        "lastUpdated": "2014-02-20T20:19:20+0000"
    },
    {
        "userId": "be5a7d28e9cd4166a5302dd38f342f15",
        "status": "closed",
        "dateCreated": "2014-02-20T19:04:28+0000",
        "id": "fbe7c4cba75f40f8b458faef9a9a41ea",
        "version": 60,
        "attributes": {
            "feedback": [
                {
                    "answerGiven": "5"
                },
                {
                    "answerGiven": "5"
                },
                {
                    "answerGiven": "5"
                },
                {
                    "extraFeedbackGiven": ""
                }
            ],
            "title": "PSSAP VIII",
            "elapsedSeconds": 1674,
            "score": "90",
            "questions": [
                "3ec2f3992f6043439a085b6aca3d496c",
                "ccfe13ee8725486484ba11a59ff023a7",
                "248b91cca21c4870a0636bba28302231",
                "8da703f1a80f4c2d8a6f9b71a9936127",
                "99e0c4b35e474d3caa94e4f839ee3cc7",
                "d70098687a914f14ad133c7bb0b0efab",
                "9734ceeb66004ceead386c702675a75e",
                "45f79779c67b4bf5b452747995598008",
                "0292f1c8ae83496b865cc44b3c9cc02a",
                "930f227b3bf94d92b500fadef3db0e01",
                "a3972964fab0456f9857fb5f103a48a0",
                "9a363ac6640d463b885194888c894d31",
                "22eeb1a86e43419da5b1dbd47d457e60",
                "fe1b714f3c184e1d908abd0db853883a",
                "67f82b739de34b8c9767ee1a01984f26",
                "3feec8d493444d6d97a495225ef0b41e",
                "02c3f4936e94452c80fa6d3925b8dd53",
                "890893eb1e7447008ca3b2a697078f4b",
                "226bde4c0c6442a3bdbed45cba0f79c4",
                "7e91abd6fc594db5a651911bd637f44f",
                "5942ebba2ec3400d93ded2eeb91d238c",
                "96691cffce08432c9ef188c96d191b81",
                "6dbcda2431c94e85a0dfc9ba65edc98c",
                "7ddced3226c0407ebdeaf6aa3190aa39",
                "343f3560dd9f4852b53ed75165cea7b0",
                "4074bd74ebd04cc9824ab4db18938bf0",
                "96365b90392945c9b4798b1434407392",
                "da1b72b918bc46e29cbe39872f147e01",
                "cefa1f78d6a64a24ad31d48f374faf87",
                "d138bf2559d849f686fb374dc8336868",
                "a6facfa567194bc69a07c3f8d4957757",
                "bf6511df879048fc9aec71a362ae7073",
                "a21e968fafee47b7b2b3bb91c8dc3faf",
                "90c8f91fe0434d8d9a85d498d9c9409f",
                "86146574f208473b83f9e23198235256",
                "2cdb86309a5b4e47adb540b8b337a44e",
                "e528cc258a804ce7a5427ecf4a4498dc",
                "5b29b0e163f04f19b9d03eeb77539677",
                "4314092e600940a5843e575c11f3cb1f",
                "9a87faee02a141d3a17ac7bb84cad701",
                "f7233284432d416baae13edead6c86d7",
                "59b7714477234a40a4e8969e8a959570",
                "f919f89db3614c8b93fd8c7c42b1fcbd",
                "732d796777404cb3a25f7b62a73c5425",
                "e533d5556b144500a58092945e3efe20",
                "05f3fe41a3fc48d7a00cb7422a24ca50",
                "af06dd151ae04e41bb35250bd809e907",
                "3ca1e2a91b8a4b2f859339cc151c952b",
                "b206b8224c794590b762b10683e27503",
                "f68ba41929c243e0920fb54dfb6e062f",
                "1dba6359a83f4633acd2fbf8675b7b53",
                "34c1441a06964a579f2e3e512031ec3c",
                "c32fc94bb35547989147c2706afe1f80",
                "6c27069f1dd848dfb13f8e20c8acb6ac",
                "770f209b8784400eb15b7d6c3d144985",
                "1649f65a86a24926a667771720fb569b",
                "55109c6ada3247099fb1a2065f5172fa",
                "81f3a547c413442dbd536b763f27ef6f",
                "b1cd95e5f2484315b15aed1a736b3bf7",
                "261ac2c8051944a0bd1675ede0bab21d"
            ],
            "examId": "477cc48c7ac2426a9b194352f21bebae",
            "programType": "sap",
            "custom": false,
            "dateCompleted": "2014-02-20T19:32:33.341Z"
        },
        "lastUpdated": "2014-02-20T19:32:33+0000"
    },
    {
        "userId": "be5a7d28e9cd4166a5302dd38f342f15",
        "status": "closed",
        "dateCreated": "2014-02-20T17:01:28+0000",
        "id": "a769ec211ee64cb5b3364e9e1b127ea4",
        "version": 62,
        "attributes": {
            "feedback": [
                {
                    "answerGiven": "5"
                },
                {
                    "answerGiven": "5"
                },
                {
                    "answerGiven": "5"
                },
                {
                    "extraFeedbackGiven": ""
                }
            ],
            "title": "PSSAP V",
            "elapsedSeconds": 2280,
            "score": "88.5",
            "questions": [
                "d7ed3840a98541c7aaf2471ca3cfcbfe",
                "0beb0298d9e04866a760dffeaf34d5ee",
                "6486c904f102445393e7ca127ad299f4",
                "4d078917529c45dcafbe1085ce084240",
                "a29ddced3d20440596c371a1ba0162ee",
                "c46e57c6e4a34be5a06f2638735f2ab8",
                "8355825e357046788bb9fcedc9131168",
                "086efe5dfeae45309d6abc58441a43c5",
                "72043de98f414ee0b4f63987d1db523c",
                "05fbb4d33a8d4af7b96e5280e0b3bf7f",
                "a3d72fb0de4c4c7cb4e44bb3e8559d3b",
                "4517000e6fdd4846b941035785d309c8",
                "788386b56c5242aeadb7f450da2bd805",
                "c42dc043b23a4154adfe94ee1546daa6",
                "11c1b8d5acb947de8b1bd0e81744c915",
                "453ea751c2d742d68dd2fd3f771ac613",
                "fd005645b92e42fc9fed50060efa96d0",
                "2a1395c667134cb9bb82b321b912e63f",
                "9f4e1ddd71ce4ce39727b2f2e6bf69bb",
                "d948801edc684a6cbebdb7d002127f95",
                "8947bff250df49a2933b18fc1b2f9883",
                "72f6a785e7d445248c1642add42c55a6",
                "e002aa94fc5c4fdea8751420efe9b064",
                "287a0c6152b640729977c2263d9d0f69",
                "269b558b50cb4495bad023ceba70383a",
                "bbdad397574a4d9abf94bf04ce017376",
                "be2843d2073248198a14e736215a0c90",
                "05b35a19f9e7405f84f311f64e5b74eb",
                "cc6bb3583fc04962ab48308e3d6c4a3f",
                "65214cb74a8d4a889b93f1da713731ed",
                "580a542798ba4d65ac96a71ac8c5c9fe",
                "969180a8eed146e98702e8fc97f0ea8e",
                "6c1898b04b584788a5ce758b0e7a5002",
                "f1eb3b7b9fff4c40a2666605a3ea00ca",
                "19a481a9796a41db8bfd41ccb09191ba",
                "c222b32f0377424ebc3d91d32b14e79c",
                "0cadea043f5d4b46ad2a60b7b28c9b48",
                "9ecd75570a4e4469aa5430577342c736",
                "b60bfbcc1a7945ea989f1b9363e5a76e",
                "46246766af384181bf0ccee353fb4189",
                "4b40f2e284924f3eba086d06eba18253",
                "690f0b92cafe4482b53c1482feed76a1",
                "0b34314bc44a4f36992f165c2ccf6f82",
                "640736120480455189e7582a6c8df4a1",
                "f8a328105f7644e88f01c930f6731012",
                "9343f91234304647af41b68fd946b82d",
                "40e127c6dc3e4af3830a9f6823143092",
                "75e07e1b964f4c498ee56146aa532dfd",
                "1c5108e22adb4857813f75c17fa263a5",
                "fb0603a518c246f781ec91e118a8b65a",
                "a2268a327bf6413d966d4cdb59a1c35f",
                "e29c86e09dc84024884dca725c4ac8ef",
                "cab379a6b3d4403fa1a05f8b92a42efe",
                "406296e0b1fb45d39357f6599dbe2978",
                "d9e4698bdada43d08d60e2d76a6c49bd",
                "c5726770674945beb8410af0562f8719",
                "7394b7f35c9d4f49aef74599b3af573c",
                "ea3052c2fdf74155977b0eb3cfb6cf42",
                "2460a512ce684afea422597a40ed300a",
                "03ba389245e8443b8a4add3a14145a4e",
                "f8dd15559ff44de6890eaedebf4b534b"
            ],
            "examId": "035e5a282ec948cb9c44a1e1a638c561",
            "programType": "sap",
            "custom": false,
            "dateCompleted": "2014-02-20T17:39:39.423Z"
        },
        "lastUpdated": "2014-02-20T17:39:39+0000"
    },
    {
        "userId": "be5a7d28e9cd4166a5302dd38f342f15",
        "status": "closed",
        "dateCreated": "2014-02-19T20:23:41+0000",
        "id": "2ead173f3a204ae89d06095f606a044f",
        "version": 66,
        "attributes": {
            "feedback": [
                {
                    "answerGiven": "5"
                },
                {
                    "answerGiven": "5"
                },
                {
                    "answerGiven": "5"
                },
                {
                    "extraFeedbackGiven": ""
                }
            ],
            "title": "PSSAP IX",
            "elapsedSeconds": 1226,
            "score": "86.1",
            "questions": [
                "6843164031e94fa0a84e6d68be734542",
                "8c3827692e064f299dd822987d3aa7ef",
                "b040d5e66e99409389abe2434c682492",
                "a17ef03a0ddf45f1972bd518fe692fe4",
                "6481a8d2c14e48c593fa32ee592c7334",
                "23ebcc74b51b4ca9bffe15a20bc1c3cd",
                "40f8fae16a384d88bf477e08bd03666f",
                "c1cd3be5d4dc45648b8ce85fd74cdbab",
                "025aaba98ffc432abaa9a172e9c6c701",
                "d40ae1612d7c4034b408b3fa83638c15",
                "f409940007bf41308bb331eab324e0d7",
                "bf631fb9f91e4c75bceae5f612459d9b",
                "8c5da26d0cb94da88d79a8cb072e0738",
                "7949989375044210bf4dd5eda1ab0f83",
                "33a48d7f18814f05b958e61b61aa9d52",
                "ce92002901aa4fb0aca0b1a278b7def0",
                "0aa75027eecb4a7c95521a8832b4e08c",
                "27bd3612638c4ef7bdf95eadd0ab1461",
                "c5c895ee9ce245ca90577589f83dd2bc",
                "2e6dd772fcaf46468e87317ca5fd8215",
                "03f67cf4b873492da0341432db38cd44",
                "4739942283964202a580fe7d0924051c",
                "ddb3baf2d0f14658a0073176a799681f",
                "640b82c77433440587c737476fc3e001",
                "a3aefa5c80d7403ba285e2578a3bd165",
                "1d4124d8cdb245ae845a99b0682d8414",
                "62c630011efe466883f1c3443734cfeb",
                "ec7ed35525c243b5848703a6119dc609",
                "298bfe121768449ca3857e05c824f08b",
                "345b7df2404545d28d1b10d1af1437b2",
                "e9597766062745f2b639215c7ec6b7a6",
                "1244e8576496437a9fb2f4aaecbb42a3",
                "b761e170861440358a6b523dc25187ef",
                "41e41f0003b04d93a09425b26c06f813",
                "724530543feb4eeda01f1e17d2441f18",
                "122735c7a8284e65961209f0c9f1a1f6",
                "8c0461b6774f443e90a542844919b3cd",
                "571dcb1601644215b2f90da2b3fbb1b8",
                "4e58abdaa0f84c189a28b5fcd0a9ce50",
                "8615cc8655fd48f6b3d05c43caf71188",
                "e8c5be58b95041ca91a6840ccdf61862",
                "117620da4bb2438b8d3374e3a3b65253",
                "dc5d1dba3dc240fc99bb736012827a4d",
                "6d38bcdc9ded428a83b07a38e461fd9d",
                "57fbb4975e5d4260afb548df460ebb01",
                "afc937b91d374f46a5199b557aab1e50",
                "87284b6fb78d4480bdcb0a60655f3e27",
                "46d5bd3bb65546c9ae6bafb163fc3149",
                "8660d21a540047fdbcc34e00cd500425",
                "e914fcf2393d4448804a150459a1c743",
                "6fff8655ac944999a55421acdd1b8da1",
                "943205df5def4873b0862bb8597410d3",
                "12198e01f3944b6e9f7273f43c38c1a7",
                "1d3fb9b6e47741c0b8f33b6a437a0051",
                "3755221afb844e5aabc1a6059dcc6b43",
                "2d391eac8e134ec0b984e232983971f1",
                "9d18d08108f941f5bab4ad3f92b1af47",
                "f94648ffe7f04796a547a8d28411d20c",
                "9830eec6b9c54d68b86632a9ab438717",
                "17264cee23844c63b32a4077f483fcea",
                "ca097485f5204a9590edde16305aca9f",
                "d58c9b0ebe2f4c04bbd278e63e5b6521",
                "ad9107250bc64f7082fcff5626e60db7",
                "375dfe324c1345d484c3799bc03191eb",
                "693944f783924f07b4b4f1250b4365ae"
            ],
            "examId": "8361f1c8cf34417e8333f281ffe93f41",
            "programType": "sap",
            "custom": false,
            "dateCompleted": "2014-02-19T21:57:20.756Z"
        },
        "lastUpdated": "2014-02-19T21:57:20+0000"
    },
    {
        "userId": "be5a7d28e9cd4166a5302dd38f342f15",
        "status": "closed",
        "dateCreated": "2014-02-18T21:13:47+0000",
        "id": "e64c1b7178764fdeb3b059837b193c98",
        "version": 71,
        "attributes": {
            "feedback": [
                {
                    "answerGiven": "5"
                },
                {
                    "answerGiven": "5"
                },
                {
                    "answerGiven": "5"
                },
                {
                    "extraFeedbackGiven": ""
                }
            ],
            "title": "PSSAP III",
            "elapsedSeconds": 113,
            "score": "76.8",
            "questions": [
                "a50c6fc1da9a41cfaa92398f41f914c4",
                "161b13b46ff84bebad30a854c126bfc7",
                "25479b22928c4d7c89f30e9ca99783e0",
                "fc439dc9f93a48c2a797329264ef852d",
                "e2d7bc1c99de4420a7bdb888d1a13ef1",
                "aaf6e1eae9864fea9cedabc8054b1ef0",
                "bd18562c40264befa258c951edafa244",
                "0ae4f6fd8e114eaba1a94b9945f79b8d",
                "bcd1c074a9454d66ac4ee0cdb6aefd87",
                "c30fc4609f834b9fbee9b21573996522",
                "90514eda88db4e5a943065607fd1c7f0",
                "d6dfa08434574731b6b1efbb5bd19850",
                "ef4e5140d03f44aea6f3345fafdfddb1",
                "f903cff87c41405c925fb27e68f0ee5d",
                "0ce8643386c445b98cbc5c6dcaf90efc",
                "8bf62d08c8e6484da54f2979bec0db06",
                "46c8127358a748388280e10a5439bbf6",
                "98643865dfdc4ed88077ce1980a2f173",
                "41c94e9cf25743d4954abad7376ac79b",
                "7a100cd6cbe8472595a7991bcae5b743",
                "af8ede119da84d448b6892e442edec86",
                "63d1087741904573bc7043b0173ff1c5",
                "a20179b203114f2ea4c8da0e053370ee",
                "56f89d496ab740a3be0852576e34fae1",
                "03743474f0ab4f1eb32dcf94785282d2",
                "030928ab7fd54b409dc827e114704947",
                "82ba74b3d9a4413591fdf2bec7b16c87",
                "1f0b578163104d7a8383d9f8658288d6",
                "fd37c08d9f514f118575a2d095202c4a",
                "ead1be60915042f3a7ca86ae0e919d71",
                "105535a73df64956b2f66598a2fbe9fb",
                "f5fb36a2325f416a8fb33c5436720ed3",
                "0866eb2dc62141dcbc5e88e293d0c9a6",
                "0e8e7f95b9ca4eab8ca54a5ec9dcc0d0",
                "afb15ea7fe784d0a9698490fc72223b2",
                "8390b13d2f3041a18c2dfef85f45b33b",
                "839b194d8d794e408db7721cfbdb7ccd",
                "0819bac01f5040abae8777fdf56f2005",
                "961d3f14b24b4a0fbd70fc7b0bd609b7",
                "477fe81b8da543eab598f488fa10d80f",
                "76d1d0b851244380b07cc02f2514fc1f",
                "714855842cda4cbaa24930a9a4b4a415",
                "1ecb82718b8d4bbcb7be0cbf436da960",
                "ca20c53b83ec4041bead045f4707df11",
                "dd057ea7f9d14d7c8df380dd005bf8c6",
                "e2af5965fe6542ad9f99ba0f3cfe84a4",
                "959e1251fb4347298c6ff52a3b527fa7",
                "30a098b2dca64e30a57f1e387b484df9",
                "9c5f1aceafaa4baab48ef08d44cb2693",
                "4ffe7c6acf224cc38a91405272408877",
                "03f97eae392f46238a1ba8081b4cf602",
                "4d3f36076425474e9be309fa8422f922",
                "b62ca3e4a3824e82a1c0600de936f70c",
                "d14c4902485146b0b4edf6175563d5cc",
                "4447a74a96ce4ae3aaa2c2fadd88f816",
                "d01fdd4df6c44307a08d1a4482a6dc4a",
                "55f495be96cf4aabaa4d9d82b6695777",
                "9ed9e6a9430446ae81e733a5a8c3de02",
                "ad032d1bd7134b1f84ce967946f5cb16",
                "441fb75c32084e99bdf75944ca207a20",
                "679d9c604109435492300247b48fa9c7",
                "51da2f3fa99d49b1af70588b527567bb",
                "e7d41392dd29498abc04d1c5fd619620",
                "4203febf5f1d425fb52f70e23db35782",
                "7edb66e7d513413abf66ed5f3e822a81",
                "cc5b02d056fd49d8ba53af766a734777",
                "c424e47f021841049a1445119b757e58",
                "774f624e3c314db097e73e146551ebc6",
                "bafd57defc824fad8cc9c2969960d6c3"
            ],
            "examId": "8439a3532e534013b7b043940f72496c",
            "programType": "sap",
            "custom": false,
            "dateCompleted": "2014-02-19T20:23:26.422Z"
        },
        "lastUpdated": "2014-02-19T20:23:26+0000"
    },
    {
        "userId": "be5a7d28e9cd4166a5302dd38f342f15",
        "status": "closed",
        "dateCreated": "2014-02-18T01:49:02+0000",
        "id": "59072a348eb846d88b290369550718e2",
        "version": 61,
        "attributes": {
            "feedback": [
                {
                    "answerGiven": "5"
                },
                {
                    "answerGiven": "5"
                },
                {
                    "answerGiven": "5"
                },
                {
                    "extraFeedbackGiven": ""
                }
            ],
            "title": "PSSAP VIII",
            "elapsedSeconds": 1760,
            "score": "81.6",
            "questions": [
                "3ec2f3992f6043439a085b6aca3d496c",
                "ccfe13ee8725486484ba11a59ff023a7",
                "248b91cca21c4870a0636bba28302231",
                "8da703f1a80f4c2d8a6f9b71a9936127",
                "99e0c4b35e474d3caa94e4f839ee3cc7",
                "d70098687a914f14ad133c7bb0b0efab",
                "9734ceeb66004ceead386c702675a75e",
                "45f79779c67b4bf5b452747995598008",
                "0292f1c8ae83496b865cc44b3c9cc02a",
                "930f227b3bf94d92b500fadef3db0e01",
                "a3972964fab0456f9857fb5f103a48a0",
                "9a363ac6640d463b885194888c894d31",
                "22eeb1a86e43419da5b1dbd47d457e60",
                "fe1b714f3c184e1d908abd0db853883a",
                "67f82b739de34b8c9767ee1a01984f26",
                "3feec8d493444d6d97a495225ef0b41e",
                "02c3f4936e94452c80fa6d3925b8dd53",
                "890893eb1e7447008ca3b2a697078f4b",
                "226bde4c0c6442a3bdbed45cba0f79c4",
                "7e91abd6fc594db5a651911bd637f44f",
                "5942ebba2ec3400d93ded2eeb91d238c",
                "96691cffce08432c9ef188c96d191b81",
                "6dbcda2431c94e85a0dfc9ba65edc98c",
                "7ddced3226c0407ebdeaf6aa3190aa39",
                "343f3560dd9f4852b53ed75165cea7b0",
                "4074bd74ebd04cc9824ab4db18938bf0",
                "96365b90392945c9b4798b1434407392",
                "da1b72b918bc46e29cbe39872f147e01",
                "cefa1f78d6a64a24ad31d48f374faf87",
                "d138bf2559d849f686fb374dc8336868",
                "a6facfa567194bc69a07c3f8d4957757",
                "bf6511df879048fc9aec71a362ae7073",
                "a21e968fafee47b7b2b3bb91c8dc3faf",
                "90c8f91fe0434d8d9a85d498d9c9409f",
                "86146574f208473b83f9e23198235256",
                "2cdb86309a5b4e47adb540b8b337a44e",
                "e528cc258a804ce7a5427ecf4a4498dc",
                "5b29b0e163f04f19b9d03eeb77539677",
                "4314092e600940a5843e575c11f3cb1f",
                "9a87faee02a141d3a17ac7bb84cad701",
                "f7233284432d416baae13edead6c86d7",
                "59b7714477234a40a4e8969e8a959570",
                "f919f89db3614c8b93fd8c7c42b1fcbd",
                "732d796777404cb3a25f7b62a73c5425",
                "e533d5556b144500a58092945e3efe20",
                "05f3fe41a3fc48d7a00cb7422a24ca50",
                "af06dd151ae04e41bb35250bd809e907",
                "3ca1e2a91b8a4b2f859339cc151c952b",
                "b206b8224c794590b762b10683e27503",
                "f68ba41929c243e0920fb54dfb6e062f",
                "1dba6359a83f4633acd2fbf8675b7b53",
                "34c1441a06964a579f2e3e512031ec3c",
                "c32fc94bb35547989147c2706afe1f80",
                "6c27069f1dd848dfb13f8e20c8acb6ac",
                "770f209b8784400eb15b7d6c3d144985",
                "1649f65a86a24926a667771720fb569b",
                "55109c6ada3247099fb1a2065f5172fa",
                "81f3a547c413442dbd536b763f27ef6f",
                "b1cd95e5f2484315b15aed1a736b3bf7",
                "261ac2c8051944a0bd1675ede0bab21d"
            ],
            "examId": "477cc48c7ac2426a9b194352f21bebae",
            "programType": "sap",
            "custom": false,
            "dateCompleted": "2014-02-18T02:19:08.970Z"
        },
        "lastUpdated": "2014-02-18T02:18:39+0000"
    },
    {
        "userId": "be5a7d28e9cd4166a5302dd38f342f15",
        "status": "closed",
        "dateCreated": "2014-02-17T23:06:26+0000",
        "id": "a50a4e4049f1408989231c29f52da242",
        "version": 10,
        "attributes": {
            "feedback": [
                {
                    "answerGiven": "5"
                },
                {
                    "answerGiven": "5"
                },
                {
                    "answerGiven": "5"
                },
                {
                    "extraFeedbackGiven": ""
                }
            ],
            "title": "Evidence Review NEC",
            "elapsedSeconds": 221,
            "score": "100",
            "questions": [
                "aeaa2ab0bab441ef96579ea925eea34a",
                "50e4d47a8fc84a46ac61c116691d7ebe",
                "6f95b3533e0d402b84885630129f771d",
                "dc2b7a7487a44c64963258691be51933",
                "24b77fb46ce34b5b92017f1fa80d0e01",
                "c0b4f1b354cd4e298f4ead6ac8b1f47c"
            ],
            "examId": "78aba2edb427434eaa506d24365e2e16",
            "programType": "evidence",
            "custom": false,
            "dateCompleted": "2014-02-17T23:10:18.167Z"
        },
        "lastUpdated": "2014-02-17T23:10:17+0000"
    },
    {
        "userId": "be5a7d28e9cd4166a5302dd38f342f15",
        "status": "closed",
        "dateCreated": "2014-02-16T19:04:37+0000",
        "id": "83291ddebcec4ca180d7132262947eb8",
        "version": 92,
        "attributes": {
            "feedback": [
                {
                    "answerGiven": "5"
                },
                {
                    "answerGiven": "5"
                },
                {
                    "answerGiven": "5"
                },
                {
                    "extraFeedbackGiven": ""
                }
            ],
            "title": "PSSAP VII",
            "elapsedSeconds": 2773,
            "score": "100",
            "questions": [
                "ee6107aaeea34aa0a64cac54ff86feb5",
                "82afcd28911c441295e4ec70ff883811",
                "4aac61cc29ac47f39200f02bb49b9882",
                "e964303d8ad34399912f458b1e3211d8",
                "dbfc29c832104b34830a65dbb76acee2",
                "3f67d38e7bbb4db19fd711b979291ce4",
                "285b371309684fcd9927f919419c896b",
                "29b7d4da31ec472ab5a1a8c07f289a14",
                "baa9015e19534258b8c271e596258df6",
                "9410a77f26e8446fa01b64d69e7ad81f",
                "8d548734cad34e3894cf7be80b1f573d",
                "77e4740bfd784d34b2c244b5dc142766",
                "133d38a24e69465d8cd6cd3051080d71",
                "a16fbf2ffa644cc591fa98faedf7ecfc",
                "c1e4792cd1644f27b7ea25243ae9ed37",
                "238ddd5a39494363a745ac81376369f8",
                "4f74be45812043b2984f0d552461eb35",
                "79d40d8a98a04ad0904983180b19315e",
                "c347a67c5fd94192a253524008b2e8bf",
                "1b501376ced246ec858b504dadcfb01f",
                "fc40d72bf08a4f1db557a2204091257b",
                "d96f9f81591a4146b6359d5e40dbeb4a",
                "cce3d3ac73654b02958aee448e082192",
                "798e889dfccc4c20adee88dd5182db28",
                "a63178e189034dbaa82348483c49f4b7",
                "ec3b845a4b3f4c0f83c490f4a51bc052",
                "6bdda8dd487f4e86a3d03e138e5f800d",
                "2aea9e45f80346d18c275879d9153cf3",
                "f8366edc84994b4bb4d31c6319740a4c",
                "5e53b87837224468be28ba89b0eb76d0",
                "c5f178a2c67d4722ada42dc5018f54ed",
                "738179946dfe4a129e1db786dc40f7ae",
                "e3d035676d27491ba828d914e6368809",
                "fdf67dc4333249f5a62b3ba2e37938c3",
                "942e7c9ec5644556ad20f885fd8e400a",
                "b63b000c5c9c4502a93d9386203de5ac",
                "6dbbb67f904348f49d0e6ecb24ed2ca1",
                "b4d7f6f38615457eb25936e4f3893a04",
                "7b47c8a7157a4eebb5da004903ae6219",
                "3110da116d5d4236af0dcb7aa7427d62",
                "eea11d9d4aab4722bdb8c23262162a29",
                "a9b750340db7498a80cfc8f1deb353f0",
                "605e854b7e7d446d922f966af503078f",
                "873f4ef4e7974d0aa22162bf912107c1",
                "615178f99a8445feaa63c4c63040d267",
                "2c464e98a5fe4f19b63c836177bc97f2",
                "3a68782aa15b4e43884407ced802efbe",
                "7b09a86e6cea49b38a11c637ed1b85a3",
                "e1bbe2584db043bc9dba3873a93056d2",
                "147fa9f411674a828ee87a6679e630c6",
                "2abe60741cdb4a7ca4f13acc1b6ee0be",
                "066709d6042e4d7393d7bbac05f9d0ce",
                "64855057ccd5450cb854bf84205320ba",
                "7e5b0e665900475ead5512e5c3047e85",
                "788abcc47c0d45808e70fe5857bd89d9",
                "d53247b037aa485d826f336ab82eb398",
                "51c245527d0d4c91ba0e0c31239ee074",
                "81daeab90035446cab65b0653d97ec23",
                "6d8e124e3132404289b046b8010fd2da",
                "fd008c3744094dcabb1dfb4a0d99350b",
                "d7ccb463c3cd4222ba90fa9f6d256738"
            ],
            "examId": "df7670e368124e30964ccc6963f2c012",
            "programType": "sap",
            "custom": false,
            "dateCompleted": "2014-02-16T19:58:31.241Z"
        },
        "lastUpdated": "2014-02-16T19:58:02+0000"
    },
    {
        "userId": "be5a7d28e9cd4166a5302dd38f342f15",
        "status": "closed",
        "dateCreated": "2014-02-16T16:19:32+0000",
        "id": "f488192ea7a2482badc06d1b84ea5577",
        "version": 96,
        "attributes": {
            "feedback": [
                {
                    "answerGiven": "5"
                },
                {
                    "answerGiven": "5"
                },
                {
                    "answerGiven": "5"
                },
                {
                    "extraFeedbackGiven": ""
                }
            ],
            "title": "PSSAP VI",
            "elapsedSeconds": 1402,
            "score": "90",
            "questions": [
                "78aa48dfb3a048ca945ad5f6792d704c",
                "f2c92b7cd96243738c0431d0403b9f62",
                "c3c759aec52a40719030902f28766db7",
                "82fc2d6c3907470588d9972f61f140a7",
                "666a8483cebc4c3ca8494ce17d7e551e",
                "e7139c052cbf4bca8cf44aee17c9af4f",
                "0e8a638b958d4798bb2650bdb6fdb985",
                "3a105f27d55a4f9e8015ff7fe089b26c",
                "db2a33fb103f46c1b99a1597f8bd0747",
                "97bf265fb9e743a18dbba8dc0f9b14c8",
                "bb3dfe8bf1fb468c8cd6515415f8c0a0",
                "9a270f14962a423ea29fa670bd7dd450",
                "b212a1badd644089bd32113b181e6098",
                "da9d1d7fcea94c2d814de065e95f72c3",
                "46d13645b3ec4014b06016b1cc1a1b09",
                "cb7e7824c63543929c11c07817b616a0",
                "ca1bb0f5c2014522912aa9c7a459a0c2",
                "bcfc1186bf3048b687f7bc56e0ac2f97",
                "1757c558153d446b926301ecfbf5c77d",
                "69cd43b2a72543638d03d05e5eaa91a4",
                "7963bc7d43fc4f259311fb662b6b4980",
                "63298264d04541ce968a575bbefaf252",
                "662f12e560b04bd29b97eff14c4bcfe8",
                "25417627f23d453dbce7419989940378",
                "d5e7d1ff6df1458a9de78a79c78e1901",
                "3af9c7efea8a4cc895fe60a7483c3f1b",
                "7905de097fc9400b8245da231fcba833",
                "68006e404a164249a6dc441fab68f94f",
                "dab1c0781e924330ab3325bc51d8f3d5",
                "fb94ff2790a445d3818bf84dc4b04926",
                "67cba4873f2f4b9f976a0c11f6799285",
                "6b5e6ead665a414083eaa18e46829d06",
                "5e494cf02cad4af7a0b63e0a1c863e59",
                "d61dbfbd641446a98bc0413687f34918",
                "60e6afd5d0734caba6bec458a23e0698",
                "ba6bdde0f6f6480f925ce56ef7d46640",
                "fd6517befbe348fe83b122f3dcd4132d",
                "df4fd14ff0944be68631cfa2129d66fc",
                "46dfcbc00c594bb5acb4ef7d82809a60",
                "c74a7fd9da894eff854207b300df8fcf",
                "1c15671a12a6485a97ae3943cc003f50",
                "a21abd62ff994f6e97d269889f17436a",
                "598ce6dd62294b41a8b2d286000317af",
                "4cd49d641d2744c59dcd808fa9926944",
                "fb4d247f4b814d338a60385ce7be9040",
                "f2a491d2e6c6478e849082b44263d7a3",
                "0bd55014f00543109906d3c34f8019fe",
                "eaebf76f632b4d33b744fc7c08dc25c2",
                "9d1a52ef05b5414787f7568efb01c8af",
                "4e524d648f634d68a57001738bfeef06",
                "64ad805c46b5427eb03a61276d10188d",
                "055b81c251a14eb992e7bdb2e84de2b7",
                "8f8d87a227914856aee863ffe59422cb",
                "69a2da40d8ae4ae8ae8adf012843d713",
                "4a5ddac0dd1b4cb2b33f646c70b0ae50",
                "618fd155e43347639fe1919507a3b3b6",
                "6d3e4741bcd045cc895cb1af8b173ccd",
                "217f7aa6d76949f29e881a7f707d3d75",
                "9077ccd473644fa1aee2fb7cd953ace0",
                "67dbf8be376f4e65bd8d3d0144266ce6"
            ],
            "examId": "094dc642774e4d989b2ea8dbc31c8df5",
            "programType": "sap",
            "custom": false,
            "dateCompleted": "2014-02-16T17:19:12.370Z"
        },
        "lastUpdated": "2014-02-16T17:18:44+0000"
    },
    {
        "userId": "be5a7d28e9cd4166a5302dd38f342f15",
        "status": "abandoned",
        "dateCreated": "2014-02-16T16:18:58+0000",
        "id": "6b536f2ac8bd4d19848b9715dd027e8f",
        "version": 1,
        "attributes": {
            "title": "PSSAP IV",
            "elapsedSeconds": 13,
            "score": "0/60",
            "questions": [
                "c3a491cd08694a1abf982634853a79f3",
                "a69453cfb7ac43bb86da0006e4d34e04",
                "92c01e3dfd0c4645b326bd2f1eb9aa5e",
                "932530f6e7f74d72952f994cdf624461",
                "85230191e85b4f5c9e526292530fdef1",
                "35997405f7ae4654aaf02efdf5a3e814",
                "a9fdc511271342fb85568b22d70f0db8",
                "967423a1debc4f379750623689cf42f8",
                "169bf703b98b415eb53baad1afe4106c",
                "bc4d87d27b524844aeacca603d1bb291",
                "281fe2cca4984aecaf73b4d4ca36ce04",
                "71252f88261b4319892f5fc5e4f32e94",
                "c41db0ee8c1a4927a85a347662c7d71e",
                "4a40e084d68d4fbca89e205a76f3e6dd",
                "93cd90bd76fe4062b0528a44d224e4ec",
                "14263d94ee64439694041624ff47d404",
                "842dfb18c62544e687e415d1d9a1094a",
                "f8a2a9bb9dfd4e9cbac4728b590a2ba2",
                "5f78ad3ab0254cc981af64385b5de827",
                "19f7003848824ee2895808486dbc4cd3",
                "6d998ae2764648c8bee07b4eb99c55dc",
                "3fad67569cc94bd0a6cc3d4fbe9c5b20",
                "b97c59c90af140839bed6bc642fa4b2a",
                "cb2a8f80932a416a83b35030b50ae016",
                "8691100082a842ab83c3879d491b0f8d",
                "4b7eb41e3b75484484a5a9e22a31d0dc",
                "82642bcd26ae47e39acb3dedd4436d60",
                "075770cb510b48a28ac1fab542c0a2a4",
                "6bd01659940c4121afa100beb22544e9",
                "f0c7bb1fcd6446cebdc7cf55d9380d85",
                "18f182e1df1248ca8a7ce856dd77fcfa",
                "c647db6bc4294f1a8610c444f5f36cf2",
                "cb81999198c24cfabbad0233791e11e2",
                "10c99ea4140f45acb096c190692b5de9",
                "bceef2753df14e1ca1b46a2fd8e28c6a",
                "e1aaf3bc0b7e4984a696475dcdf11397",
                "0a2159140ffe4e36962a35fb385868ce",
                "365c7677d28c434899e7ba7d9f7caa93",
                "4f5028e59f6f4a56878f2bcd731bafd5",
                "eb33ed67bcd94d9cb319f7c6671f1fcb",
                "5c9c70997de5486f91646091afaf3909",
                "6cf554a0cfba4292b590aa46d6de6d98",
                "c415eac47c3a419bbbbed60a716b3056",
                "5ff46f3b4ccd4456ad804df5d26ac546",
                "6bbfd9d68b8e4b03a133012b42f0cec2",
                "364ccbad5d0c4c6a9d565de5bcec6824",
                "abf419df2eba4b098573e98b9073aece",
                "d1117bbf54704952a71be5080977544d",
                "04a9fadaa9d941fab6fc8fb7855390ad",
                "380c00ec65a34b3e8be8230cb71e81a5",
                "111850cb269f4c2badae0fb04d35d727",
                "70d2e3da92cc48078e9de1939a1322c5",
                "1ff9a3f318ca46f38d40dcdc3a555f2c",
                "8477ce5cd69843008147100cc019d319",
                "49c1fa2e63e247249e047384ec854391",
                "176677777d4f4fd1bb3bf8a64336cd2f",
                "324edfecfcfb417f9527b7eea1b60ec2",
                "b032be9511ef4070a23bf261c5378eda",
                "42eba8d7cdf34b4da99ab9b657cdf3c5",
                "0b034f22ddb2409fb24c35b3e5ca0924"
            ],
            "examId": "c7cf789dafbc4b2480178bd1b7125230",
            "programType": "sap",
            "custom": false
        },
        "lastUpdated": "2014-02-16T16:19:19+0000"
    },
    {
        "userId": "be5a7d28e9cd4166a5302dd38f342f15",
        "status": "closed",
        "dateCreated": "2014-02-12T22:22:55+0000",
        "id": "65981bb572994750a1ea17577ae98bb3",
        "version": 113,
        "attributes": {
            "feedback": [
                {
                    "answerGiven": "5"
                },
                {
                    "answerGiven": "5"
                },
                {
                    "answerGiven": "5"
                },
                {
                    "extraFeedbackGiven": ""
                }
            ],
            "title": "PSSAP V",
            "elapsedSeconds": 193,
            "score": "96.7",
            "questions": [
                "d7ed3840a98541c7aaf2471ca3cfcbfe",
                "0beb0298d9e04866a760dffeaf34d5ee",
                "6486c904f102445393e7ca127ad299f4",
                "4d078917529c45dcafbe1085ce084240",
                "a29ddced3d20440596c371a1ba0162ee",
                "c46e57c6e4a34be5a06f2638735f2ab8",
                "8355825e357046788bb9fcedc9131168",
                "086efe5dfeae45309d6abc58441a43c5",
                "72043de98f414ee0b4f63987d1db523c",
                "05fbb4d33a8d4af7b96e5280e0b3bf7f",
                "a3d72fb0de4c4c7cb4e44bb3e8559d3b",
                "4517000e6fdd4846b941035785d309c8",
                "788386b56c5242aeadb7f450da2bd805",
                "c42dc043b23a4154adfe94ee1546daa6",
                "11c1b8d5acb947de8b1bd0e81744c915",
                "453ea751c2d742d68dd2fd3f771ac613",
                "fd005645b92e42fc9fed50060efa96d0",
                "2a1395c667134cb9bb82b321b912e63f",
                "9f4e1ddd71ce4ce39727b2f2e6bf69bb",
                "d948801edc684a6cbebdb7d002127f95",
                "8947bff250df49a2933b18fc1b2f9883",
                "72f6a785e7d445248c1642add42c55a6",
                "e002aa94fc5c4fdea8751420efe9b064",
                "287a0c6152b640729977c2263d9d0f69",
                "269b558b50cb4495bad023ceba70383a",
                "bbdad397574a4d9abf94bf04ce017376",
                "be2843d2073248198a14e736215a0c90",
                "05b35a19f9e7405f84f311f64e5b74eb",
                "cc6bb3583fc04962ab48308e3d6c4a3f",
                "65214cb74a8d4a889b93f1da713731ed",
                "580a542798ba4d65ac96a71ac8c5c9fe",
                "969180a8eed146e98702e8fc97f0ea8e",
                "6c1898b04b584788a5ce758b0e7a5002",
                "f1eb3b7b9fff4c40a2666605a3ea00ca",
                "19a481a9796a41db8bfd41ccb09191ba",
                "c222b32f0377424ebc3d91d32b14e79c",
                "0cadea043f5d4b46ad2a60b7b28c9b48",
                "9ecd75570a4e4469aa5430577342c736",
                "b60bfbcc1a7945ea989f1b9363e5a76e",
                "46246766af384181bf0ccee353fb4189",
                "4b40f2e284924f3eba086d06eba18253",
                "690f0b92cafe4482b53c1482feed76a1",
                "0b34314bc44a4f36992f165c2ccf6f82",
                "640736120480455189e7582a6c8df4a1",
                "f8a328105f7644e88f01c930f6731012",
                "9343f91234304647af41b68fd946b82d",
                "40e127c6dc3e4af3830a9f6823143092",
                "75e07e1b964f4c498ee56146aa532dfd",
                "1c5108e22adb4857813f75c17fa263a5",
                "fb0603a518c246f781ec91e118a8b65a",
                "a2268a327bf6413d966d4cdb59a1c35f",
                "e29c86e09dc84024884dca725c4ac8ef",
                "cab379a6b3d4403fa1a05f8b92a42efe",
                "406296e0b1fb45d39357f6599dbe2978",
                "d9e4698bdada43d08d60e2d76a6c49bd",
                "c5726770674945beb8410af0562f8719",
                "7394b7f35c9d4f49aef74599b3af573c",
                "ea3052c2fdf74155977b0eb3cfb6cf42",
                "2460a512ce684afea422597a40ed300a",
                "03ba389245e8443b8a4add3a14145a4e",
                "f8dd15559ff44de6890eaedebf4b534b"
            ],
            "examId": "035e5a282ec948cb9c44a1e1a638c561",
            "programType": "sap",
            "custom": false,
            "dateCompleted": "2014-02-16T00:14:46.335Z"
        },
        "lastUpdated": "2014-02-16T00:14:17+0000"
    },
    {
        "userId": "be5a7d28e9cd4166a5302dd38f342f15",
        "status": "closed",
        "dateCreated": "2014-02-06T18:13:34+0000",
        "id": "02bb7e69d4cd46c786fc0d5f4b92c4cc",
        "version": 125,
        "attributes": {
            "feedback": [
                {
                    "answerGiven": "5"
                },
                {
                    "answerGiven": "5"
                },
                {
                    "answerGiven": "5"
                },
                {
                    "extraFeedbackGiven": ""
                }
            ],
            "title": "PSSAP VIII",
            "elapsedSeconds": 2541,
            "score": "95",
            "questions": [
                "3ec2f3992f6043439a085b6aca3d496c",
                "ccfe13ee8725486484ba11a59ff023a7",
                "248b91cca21c4870a0636bba28302231",
                "8da703f1a80f4c2d8a6f9b71a9936127",
                "99e0c4b35e474d3caa94e4f839ee3cc7",
                "d70098687a914f14ad133c7bb0b0efab",
                "9734ceeb66004ceead386c702675a75e",
                "45f79779c67b4bf5b452747995598008",
                "0292f1c8ae83496b865cc44b3c9cc02a",
                "930f227b3bf94d92b500fadef3db0e01",
                "a3972964fab0456f9857fb5f103a48a0",
                "9a363ac6640d463b885194888c894d31",
                "22eeb1a86e43419da5b1dbd47d457e60",
                "fe1b714f3c184e1d908abd0db853883a",
                "67f82b739de34b8c9767ee1a01984f26",
                "3feec8d493444d6d97a495225ef0b41e",
                "02c3f4936e94452c80fa6d3925b8dd53",
                "890893eb1e7447008ca3b2a697078f4b",
                "226bde4c0c6442a3bdbed45cba0f79c4",
                "7e91abd6fc594db5a651911bd637f44f",
                "5942ebba2ec3400d93ded2eeb91d238c",
                "96691cffce08432c9ef188c96d191b81",
                "6dbcda2431c94e85a0dfc9ba65edc98c",
                "7ddced3226c0407ebdeaf6aa3190aa39",
                "343f3560dd9f4852b53ed75165cea7b0",
                "4074bd74ebd04cc9824ab4db18938bf0",
                "96365b90392945c9b4798b1434407392",
                "da1b72b918bc46e29cbe39872f147e01",
                "cefa1f78d6a64a24ad31d48f374faf87",
                "d138bf2559d849f686fb374dc8336868",
                "a6facfa567194bc69a07c3f8d4957757",
                "bf6511df879048fc9aec71a362ae7073",
                "a21e968fafee47b7b2b3bb91c8dc3faf",
                "90c8f91fe0434d8d9a85d498d9c9409f",
                "86146574f208473b83f9e23198235256",
                "2cdb86309a5b4e47adb540b8b337a44e",
                "e528cc258a804ce7a5427ecf4a4498dc",
                "5b29b0e163f04f19b9d03eeb77539677",
                "4314092e600940a5843e575c11f3cb1f",
                "9a87faee02a141d3a17ac7bb84cad701",
                "f7233284432d416baae13edead6c86d7",
                "59b7714477234a40a4e8969e8a959570",
                "f919f89db3614c8b93fd8c7c42b1fcbd",
                "732d796777404cb3a25f7b62a73c5425",
                "e533d5556b144500a58092945e3efe20",
                "05f3fe41a3fc48d7a00cb7422a24ca50",
                "af06dd151ae04e41bb35250bd809e907",
                "3ca1e2a91b8a4b2f859339cc151c952b",
                "b206b8224c794590b762b10683e27503",
                "f68ba41929c243e0920fb54dfb6e062f",
                "1dba6359a83f4633acd2fbf8675b7b53",
                "34c1441a06964a579f2e3e512031ec3c",
                "c32fc94bb35547989147c2706afe1f80",
                "6c27069f1dd848dfb13f8e20c8acb6ac",
                "770f209b8784400eb15b7d6c3d144985",
                "1649f65a86a24926a667771720fb569b",
                "55109c6ada3247099fb1a2065f5172fa",
                "81f3a547c413442dbd536b763f27ef6f",
                "b1cd95e5f2484315b15aed1a736b3bf7",
                "261ac2c8051944a0bd1675ede0bab21d"
            ],
            "examId": "477cc48c7ac2426a9b194352f21bebae",
            "programType": "sap",
            "custom": false,
            "dateCompleted": "2014-02-10T16:29:42.543Z"
        },
        "lastUpdated": "2014-02-10T16:29:32+0000"
    },
    {
        "userId": "be5a7d28e9cd4166a5302dd38f342f15",
        "status": "closed",
        "dateCreated": "2014-02-05T15:50:57+0000",
        "id": "1faf438239ad46afbbb75d59846e1166",
        "version": 88,
        "attributes": {
            "feedback": [
                {
                    "answerGiven": "5"
                },
                {
                    "answerGiven": "5"
                },
                {
                    "answerGiven": "5"
                },
                {
                    "extraFeedbackGiven": ""
                }
            ],
            "title": "PSSAP IX",
            "elapsedSeconds": 1421,
            "score": "98.4",
            "questions": [
                "6843164031e94fa0a84e6d68be734542",
                "8c3827692e064f299dd822987d3aa7ef",
                "b040d5e66e99409389abe2434c682492",
                "a17ef03a0ddf45f1972bd518fe692fe4",
                "6481a8d2c14e48c593fa32ee592c7334",
                "23ebcc74b51b4ca9bffe15a20bc1c3cd",
                "40f8fae16a384d88bf477e08bd03666f",
                "c1cd3be5d4dc45648b8ce85fd74cdbab",
                "025aaba98ffc432abaa9a172e9c6c701",
                "d40ae1612d7c4034b408b3fa83638c15",
                "f409940007bf41308bb331eab324e0d7",
                "bf631fb9f91e4c75bceae5f612459d9b",
                "8c5da26d0cb94da88d79a8cb072e0738",
                "7949989375044210bf4dd5eda1ab0f83",
                "33a48d7f18814f05b958e61b61aa9d52",
                "ce92002901aa4fb0aca0b1a278b7def0",
                "0aa75027eecb4a7c95521a8832b4e08c",
                "27bd3612638c4ef7bdf95eadd0ab1461",
                "c5c895ee9ce245ca90577589f83dd2bc",
                "2e6dd772fcaf46468e87317ca5fd8215",
                "03f67cf4b873492da0341432db38cd44",
                "4739942283964202a580fe7d0924051c",
                "ddb3baf2d0f14658a0073176a799681f",
                "640b82c77433440587c737476fc3e001",
                "a3aefa5c80d7403ba285e2578a3bd165",
                "1d4124d8cdb245ae845a99b0682d8414",
                "62c630011efe466883f1c3443734cfeb",
                "ec7ed35525c243b5848703a6119dc609",
                "298bfe121768449ca3857e05c824f08b",
                "345b7df2404545d28d1b10d1af1437b2",
                "e9597766062745f2b639215c7ec6b7a6",
                "1244e8576496437a9fb2f4aaecbb42a3",
                "b761e170861440358a6b523dc25187ef",
                "41e41f0003b04d93a09425b26c06f813",
                "724530543feb4eeda01f1e17d2441f18",
                "122735c7a8284e65961209f0c9f1a1f6",
                "8c0461b6774f443e90a542844919b3cd",
                "571dcb1601644215b2f90da2b3fbb1b8",
                "4e58abdaa0f84c189a28b5fcd0a9ce50",
                "8615cc8655fd48f6b3d05c43caf71188",
                "e8c5be58b95041ca91a6840ccdf61862",
                "117620da4bb2438b8d3374e3a3b65253",
                "dc5d1dba3dc240fc99bb736012827a4d",
                "6d38bcdc9ded428a83b07a38e461fd9d",
                "57fbb4975e5d4260afb548df460ebb01",
                "afc937b91d374f46a5199b557aab1e50",
                "87284b6fb78d4480bdcb0a60655f3e27",
                "46d5bd3bb65546c9ae6bafb163fc3149",
                "8660d21a540047fdbcc34e00cd500425",
                "e914fcf2393d4448804a150459a1c743",
                "6fff8655ac944999a55421acdd1b8da1",
                "943205df5def4873b0862bb8597410d3",
                "12198e01f3944b6e9f7273f43c38c1a7",
                "1d3fb9b6e47741c0b8f33b6a437a0051",
                "3755221afb844e5aabc1a6059dcc6b43",
                "2d391eac8e134ec0b984e232983971f1",
                "9d18d08108f941f5bab4ad3f92b1af47",
                "f94648ffe7f04796a547a8d28411d20c",
                "9830eec6b9c54d68b86632a9ab438717",
                "17264cee23844c63b32a4077f483fcea",
                "ca097485f5204a9590edde16305aca9f",
                "d58c9b0ebe2f4c04bbd278e63e5b6521",
                "ad9107250bc64f7082fcff5626e60db7",
                "375dfe324c1345d484c3799bc03191eb",
                "693944f783924f07b4b4f1250b4365ae"
            ],
            "examId": "8361f1c8cf34417e8333f281ffe93f41",
            "programType": "sap",
            "custom": false,
            "dateCompleted": "2014-02-05T22:01:43.824Z"
        },
        "lastUpdated": "2014-02-05T22:01:44+0000"
    },
    {
        "userId": "be5a7d28e9cd4166a5302dd38f342f15",
        "status": "closed",
        "dateCreated": "2014-02-05T15:45:33+0000",
        "id": "344a40233f4e4bb1b5878355d24f36aa",
        "version": 12,
        "attributes": {
            "feedback": [
                {
                    "answerGiven": "5"
                },
                {
                    "answerGiven": "5"
                },
                {
                    "answerGiven": "5"
                },
                {
                    "extraFeedbackGiven": ""
                }
            ],
            "title": "Evidence Review CVC sepsis",
            "elapsedSeconds": 294,
            "score": "100",
            "questions": [
                "d21b980c62354078ab65731ef2b3c966",
                "44137a92ccca498a804d47a75813141c",
                "afbff524ff7648d59847339015c22d02",
                "7f4df0db72c5495b8a0c8472852d999b",
                "1c01e5e975f845bfb866d6565b7ea2d2",
                "3f775819127045cdbc21e132c4a99836",
                "59a81eb96c2a495f833b57ebf3407705"
            ],
            "examId": "f4bcbcb2be264a75a83773b42ad50de2",
            "programType": "evidence",
            "custom": false,
            "dateCompleted": "2014-02-05T15:50:41.598Z"
        },
        "lastUpdated": "2014-02-05T15:50:42+0000"
    },
    {
        "userId": "be5a7d28e9cd4166a5302dd38f342f15",
        "status": "abandoned",
        "dateCreated": "2014-02-05T13:52:18+0000",
        "id": "81feef2439bd4e96a4285dd215bfcbde",
        "version": 24,
        "attributes": {
            "title": "PSSAP IX",
            "elapsedSeconds": 948,
            "score": "12/65",
            "questions": [
                "6843164031e94fa0a84e6d68be734542",
                "8c3827692e064f299dd822987d3aa7ef",
                "b040d5e66e99409389abe2434c682492",
                "a17ef03a0ddf45f1972bd518fe692fe4",
                "6481a8d2c14e48c593fa32ee592c7334",
                "23ebcc74b51b4ca9bffe15a20bc1c3cd",
                "40f8fae16a384d88bf477e08bd03666f",
                "c1cd3be5d4dc45648b8ce85fd74cdbab",
                "025aaba98ffc432abaa9a172e9c6c701",
                "d40ae1612d7c4034b408b3fa83638c15",
                "f409940007bf41308bb331eab324e0d7",
                "bf631fb9f91e4c75bceae5f612459d9b",
                "8c5da26d0cb94da88d79a8cb072e0738",
                "7949989375044210bf4dd5eda1ab0f83",
                "33a48d7f18814f05b958e61b61aa9d52",
                "ce92002901aa4fb0aca0b1a278b7def0",
                "0aa75027eecb4a7c95521a8832b4e08c",
                "27bd3612638c4ef7bdf95eadd0ab1461",
                "c5c895ee9ce245ca90577589f83dd2bc",
                "2e6dd772fcaf46468e87317ca5fd8215",
                "03f67cf4b873492da0341432db38cd44",
                "4739942283964202a580fe7d0924051c",
                "ddb3baf2d0f14658a0073176a799681f",
                "640b82c77433440587c737476fc3e001",
                "a3aefa5c80d7403ba285e2578a3bd165",
                "1d4124d8cdb245ae845a99b0682d8414",
                "62c630011efe466883f1c3443734cfeb",
                "ec7ed35525c243b5848703a6119dc609",
                "298bfe121768449ca3857e05c824f08b",
                "345b7df2404545d28d1b10d1af1437b2",
                "e9597766062745f2b639215c7ec6b7a6",
                "1244e8576496437a9fb2f4aaecbb42a3",
                "b761e170861440358a6b523dc25187ef",
                "41e41f0003b04d93a09425b26c06f813",
                "724530543feb4eeda01f1e17d2441f18",
                "122735c7a8284e65961209f0c9f1a1f6",
                "8c0461b6774f443e90a542844919b3cd",
                "571dcb1601644215b2f90da2b3fbb1b8",
                "4e58abdaa0f84c189a28b5fcd0a9ce50",
                "8615cc8655fd48f6b3d05c43caf71188",
                "e8c5be58b95041ca91a6840ccdf61862",
                "117620da4bb2438b8d3374e3a3b65253",
                "dc5d1dba3dc240fc99bb736012827a4d",
                "6d38bcdc9ded428a83b07a38e461fd9d",
                "57fbb4975e5d4260afb548df460ebb01",
                "afc937b91d374f46a5199b557aab1e50",
                "87284b6fb78d4480bdcb0a60655f3e27",
                "46d5bd3bb65546c9ae6bafb163fc3149",
                "8660d21a540047fdbcc34e00cd500425",
                "e914fcf2393d4448804a150459a1c743",
                "6fff8655ac944999a55421acdd1b8da1",
                "943205df5def4873b0862bb8597410d3",
                "12198e01f3944b6e9f7273f43c38c1a7",
                "1d3fb9b6e47741c0b8f33b6a437a0051",
                "3755221afb844e5aabc1a6059dcc6b43",
                "2d391eac8e134ec0b984e232983971f1",
                "9d18d08108f941f5bab4ad3f92b1af47",
                "f94648ffe7f04796a547a8d28411d20c",
                "9830eec6b9c54d68b86632a9ab438717",
                "17264cee23844c63b32a4077f483fcea",
                "ca097485f5204a9590edde16305aca9f",
                "d58c9b0ebe2f4c04bbd278e63e5b6521",
                "ad9107250bc64f7082fcff5626e60db7",
                "375dfe324c1345d484c3799bc03191eb",
                "693944f783924f07b4b4f1250b4365ae"
            ],
            "examId": "8361f1c8cf34417e8333f281ffe93f41",
            "programType": "sap",
            "custom": false
        },
        "lastUpdated": "2014-02-05T15:45:19+0000"
    },
    {
        "userId": "be5a7d28e9cd4166a5302dd38f342f15",
        "status": "closed",
        "dateCreated": "2014-02-04T22:24:01+0000",
        "id": "4ec95745f49244c8b3a600490f43b0b1",
        "version": 18,
        "attributes": {
            "feedback": [
                {
                    "answerGiven": "5"
                },
                {
                    "answerGiven": "5"
                },
                {
                    "answerGiven": "5"
                },
                {
                    "extraFeedbackGiven": ""
                }
            ],
            "title": "Evidence Review Biliary atresia",
            "elapsedSeconds": 668,
            "score": "100",
            "questions": [
                "920004d4c829400c958ed882df8caa09",
                "cbc0f52e87e1455aa4082be4f16f2d7f",
                "a46e8372f19946148cf87d6e2946d072",
                "05868d6328384338b0824af4b89e06ac",
                "aa1798ef49ae4dc3ad3f7dff9b56a954",
                "6b6514d35b5b48e28027270dbe45da81",
                "c25c4ce510924ab1b960e8fe741337cf",
                "918350e94dff4997a8ff7669a8e894aa",
                "bba8b8cb4ea74cffb840a6530c027246"
            ],
            "examId": "0b1d9ce788cd4999b2e67bc7c4de802f",
            "programType": "evidence",
            "custom": false,
            "dateCompleted": "2014-02-05T13:51:54.234Z"
        },
        "lastUpdated": "2014-02-05T13:51:55+0000"
    },
    {
        "userId": "be5a7d28e9cd4166a5302dd38f342f15",
        "status": "abandoned",
        "dateCreated": "2014-02-04T22:15:34+0000",
        "id": "1238e72450844e4bb443bd4b4c3b16a1",
        "version": 22,
        "attributes": {
            "title": "Evidence Review Appendicitis",
            "elapsedSeconds": 439,
            "score": "10/12",
            "questions": [
                "d3556889e6e94bd2bdc16de9b5a679c0",
                "3ee02e979f424d9097e89df1be4c352a",
                "2e1f4fc506b346c18f7fef2d51f1d4f2",
                "bd87f8e7117347a4b021d37ad5de360b",
                "3d22755764f74b67845239b49bbb5e5d",
                "066f5eaf21414fbaa76cc668a5c7020a",
                "97b0682f4e05470288430550e7b795bd",
                "41b55834575f475698835d5e8c47bdde",
                "7b23ace38061495f86d022dc45cdfb63",
                "92d746e2fb99407a98ac90c666bef8ff",
                "e918a00abc3c420ab2832bd962781ea2",
                "25a566d8d4174009861d374c84443e51"
            ],
            "examId": "69b5c4d3b2754596b30e510a655e300d",
            "programType": "evidence",
            "custom": false
        },
        "lastUpdated": "2014-02-04T22:23:51+0000"
    },
    {
        "userId": "be5a7d28e9cd4166a5302dd38f342f15",
        "status": "abandoned",
        "dateCreated": "2014-02-04T22:14:12+0000",
        "id": "d8de2272234344e5ab7fed327e5d9717",
        "version": 1,
        "attributes": {
            "title": "PSSAP IV",
            "elapsedSeconds": 0,
            "questions": [
                "c3a491cd08694a1abf982634853a79f3",
                "a69453cfb7ac43bb86da0006e4d34e04",
                "92c01e3dfd0c4645b326bd2f1eb9aa5e",
                "932530f6e7f74d72952f994cdf624461",
                "85230191e85b4f5c9e526292530fdef1",
                "35997405f7ae4654aaf02efdf5a3e814",
                "a9fdc511271342fb85568b22d70f0db8",
                "967423a1debc4f379750623689cf42f8",
                "169bf703b98b415eb53baad1afe4106c",
                "bc4d87d27b524844aeacca603d1bb291",
                "281fe2cca4984aecaf73b4d4ca36ce04",
                "71252f88261b4319892f5fc5e4f32e94",
                "c41db0ee8c1a4927a85a347662c7d71e",
                "4a40e084d68d4fbca89e205a76f3e6dd",
                "93cd90bd76fe4062b0528a44d224e4ec",
                "14263d94ee64439694041624ff47d404",
                "842dfb18c62544e687e415d1d9a1094a",
                "f8a2a9bb9dfd4e9cbac4728b590a2ba2",
                "5f78ad3ab0254cc981af64385b5de827",
                "19f7003848824ee2895808486dbc4cd3",
                "6d998ae2764648c8bee07b4eb99c55dc",
                "3fad67569cc94bd0a6cc3d4fbe9c5b20",
                "b97c59c90af140839bed6bc642fa4b2a",
                "cb2a8f80932a416a83b35030b50ae016",
                "8691100082a842ab83c3879d491b0f8d",
                "4b7eb41e3b75484484a5a9e22a31d0dc",
                "82642bcd26ae47e39acb3dedd4436d60",
                "075770cb510b48a28ac1fab542c0a2a4",
                "6bd01659940c4121afa100beb22544e9",
                "f0c7bb1fcd6446cebdc7cf55d9380d85",
                "18f182e1df1248ca8a7ce856dd77fcfa",
                "c647db6bc4294f1a8610c444f5f36cf2",
                "cb81999198c24cfabbad0233791e11e2",
                "10c99ea4140f45acb096c190692b5de9",
                "bceef2753df14e1ca1b46a2fd8e28c6a",
                "e1aaf3bc0b7e4984a696475dcdf11397",
                "0a2159140ffe4e36962a35fb385868ce",
                "365c7677d28c434899e7ba7d9f7caa93",
                "4f5028e59f6f4a56878f2bcd731bafd5",
                "eb33ed67bcd94d9cb319f7c6671f1fcb",
                "5c9c70997de5486f91646091afaf3909",
                "6cf554a0cfba4292b590aa46d6de6d98",
                "c415eac47c3a419bbbbed60a716b3056",
                "5ff46f3b4ccd4456ad804df5d26ac546",
                "6bbfd9d68b8e4b03a133012b42f0cec2",
                "364ccbad5d0c4c6a9d565de5bcec6824",
                "abf419df2eba4b098573e98b9073aece",
                "d1117bbf54704952a71be5080977544d",
                "04a9fadaa9d941fab6fc8fb7855390ad",
                "380c00ec65a34b3e8be8230cb71e81a5",
                "111850cb269f4c2badae0fb04d35d727",
                "70d2e3da92cc48078e9de1939a1322c5",
                "1ff9a3f318ca46f38d40dcdc3a555f2c",
                "8477ce5cd69843008147100cc019d319",
                "49c1fa2e63e247249e047384ec854391",
                "176677777d4f4fd1bb3bf8a64336cd2f",
                "324edfecfcfb417f9527b7eea1b60ec2",
                "b032be9511ef4070a23bf261c5378eda",
                "42eba8d7cdf34b4da99ab9b657cdf3c5",
                "0b034f22ddb2409fb24c35b3e5ca0924"
            ],
            "examId": "c7cf789dafbc4b2480178bd1b7125230",
            "custom": false
        },
        "lastUpdated": "2014-02-04T22:15:27+0000"
    },
    {
        "userId": "be5a7d28e9cd4166a5302dd38f342f15",
        "status": "closed",
        "dateCreated": "2014-02-04T17:11:04+0000",
        "id": "0f409bf13d0440b2b4564ff9841ccc0b",
        "version": 61,
        "attributes": {
            "feedback": [
                {
                    "answerGiven": "5"
                },
                {
                    "answerGiven": "5"
                },
                {
                    "answerGiven": "5"
                },
                {
                    "extraFeedbackGiven": ""
                }
            ],
            "title": "PSSAP IV",
            "elapsedSeconds": 2254,
            "score": "80",
            "questions": [
                "c3a491cd08694a1abf982634853a79f3",
                "a69453cfb7ac43bb86da0006e4d34e04",
                "92c01e3dfd0c4645b326bd2f1eb9aa5e",
                "932530f6e7f74d72952f994cdf624461",
                "85230191e85b4f5c9e526292530fdef1",
                "35997405f7ae4654aaf02efdf5a3e814",
                "a9fdc511271342fb85568b22d70f0db8",
                "967423a1debc4f379750623689cf42f8",
                "169bf703b98b415eb53baad1afe4106c",
                "bc4d87d27b524844aeacca603d1bb291",
                "281fe2cca4984aecaf73b4d4ca36ce04",
                "71252f88261b4319892f5fc5e4f32e94",
                "c41db0ee8c1a4927a85a347662c7d71e",
                "4a40e084d68d4fbca89e205a76f3e6dd",
                "93cd90bd76fe4062b0528a44d224e4ec",
                "14263d94ee64439694041624ff47d404",
                "842dfb18c62544e687e415d1d9a1094a",
                "f8a2a9bb9dfd4e9cbac4728b590a2ba2",
                "5f78ad3ab0254cc981af64385b5de827",
                "19f7003848824ee2895808486dbc4cd3",
                "6d998ae2764648c8bee07b4eb99c55dc",
                "3fad67569cc94bd0a6cc3d4fbe9c5b20",
                "b97c59c90af140839bed6bc642fa4b2a",
                "cb2a8f80932a416a83b35030b50ae016",
                "8691100082a842ab83c3879d491b0f8d",
                "4b7eb41e3b75484484a5a9e22a31d0dc",
                "82642bcd26ae47e39acb3dedd4436d60",
                "075770cb510b48a28ac1fab542c0a2a4",
                "6bd01659940c4121afa100beb22544e9",
                "f0c7bb1fcd6446cebdc7cf55d9380d85",
                "18f182e1df1248ca8a7ce856dd77fcfa",
                "c647db6bc4294f1a8610c444f5f36cf2",
                "cb81999198c24cfabbad0233791e11e2",
                "10c99ea4140f45acb096c190692b5de9",
                "bceef2753df14e1ca1b46a2fd8e28c6a",
                "e1aaf3bc0b7e4984a696475dcdf11397",
                "0a2159140ffe4e36962a35fb385868ce",
                "365c7677d28c434899e7ba7d9f7caa93",
                "4f5028e59f6f4a56878f2bcd731bafd5",
                "eb33ed67bcd94d9cb319f7c6671f1fcb",
                "5c9c70997de5486f91646091afaf3909",
                "6cf554a0cfba4292b590aa46d6de6d98",
                "c415eac47c3a419bbbbed60a716b3056",
                "5ff46f3b4ccd4456ad804df5d26ac546",
                "6bbfd9d68b8e4b03a133012b42f0cec2",
                "364ccbad5d0c4c6a9d565de5bcec6824",
                "abf419df2eba4b098573e98b9073aece",
                "d1117bbf54704952a71be5080977544d",
                "04a9fadaa9d941fab6fc8fb7855390ad",
                "380c00ec65a34b3e8be8230cb71e81a5",
                "111850cb269f4c2badae0fb04d35d727",
                "70d2e3da92cc48078e9de1939a1322c5",
                "1ff9a3f318ca46f38d40dcdc3a555f2c",
                "8477ce5cd69843008147100cc019d319",
                "49c1fa2e63e247249e047384ec854391",
                "176677777d4f4fd1bb3bf8a64336cd2f",
                "324edfecfcfb417f9527b7eea1b60ec2",
                "b032be9511ef4070a23bf261c5378eda",
                "42eba8d7cdf34b4da99ab9b657cdf3c5",
                "0b034f22ddb2409fb24c35b3e5ca0924"
            ],
            "examId": "c7cf789dafbc4b2480178bd1b7125230",
            "programType": "sap",
            "custom": false,
            "dateCompleted": "2014-02-04T17:49:41.016Z"
        },
        "lastUpdated": "2014-02-04T17:49:41+0000"
    },
    {
        "userId": "be5a7d28e9cd4166a5302dd38f342f15",
        "status": "abandoned",
        "dateCreated": "2014-02-04T16:18:45+0000",
        "id": "99913051d4404afdbe4330ed221d50ca",
        "version": 1,
        "attributes": {
            "title": "PSSAP IX",
            "elapsedSeconds": 5,
            "score": "0/65",
            "questions": [
                "6843164031e94fa0a84e6d68be734542",
                "8c3827692e064f299dd822987d3aa7ef",
                "b040d5e66e99409389abe2434c682492",
                "a17ef03a0ddf45f1972bd518fe692fe4",
                "6481a8d2c14e48c593fa32ee592c7334",
                "23ebcc74b51b4ca9bffe15a20bc1c3cd",
                "40f8fae16a384d88bf477e08bd03666f",
                "c1cd3be5d4dc45648b8ce85fd74cdbab",
                "025aaba98ffc432abaa9a172e9c6c701",
                "d40ae1612d7c4034b408b3fa83638c15",
                "f409940007bf41308bb331eab324e0d7",
                "bf631fb9f91e4c75bceae5f612459d9b",
                "8c5da26d0cb94da88d79a8cb072e0738",
                "7949989375044210bf4dd5eda1ab0f83",
                "33a48d7f18814f05b958e61b61aa9d52",
                "ce92002901aa4fb0aca0b1a278b7def0",
                "0aa75027eecb4a7c95521a8832b4e08c",
                "27bd3612638c4ef7bdf95eadd0ab1461",
                "c5c895ee9ce245ca90577589f83dd2bc",
                "2e6dd772fcaf46468e87317ca5fd8215",
                "03f67cf4b873492da0341432db38cd44",
                "4739942283964202a580fe7d0924051c",
                "ddb3baf2d0f14658a0073176a799681f",
                "640b82c77433440587c737476fc3e001",
                "a3aefa5c80d7403ba285e2578a3bd165",
                "1d4124d8cdb245ae845a99b0682d8414",
                "62c630011efe466883f1c3443734cfeb",
                "ec7ed35525c243b5848703a6119dc609",
                "298bfe121768449ca3857e05c824f08b",
                "345b7df2404545d28d1b10d1af1437b2",
                "e9597766062745f2b639215c7ec6b7a6",
                "1244e8576496437a9fb2f4aaecbb42a3",
                "b761e170861440358a6b523dc25187ef",
                "41e41f0003b04d93a09425b26c06f813",
                "724530543feb4eeda01f1e17d2441f18",
                "122735c7a8284e65961209f0c9f1a1f6",
                "8c0461b6774f443e90a542844919b3cd",
                "571dcb1601644215b2f90da2b3fbb1b8",
                "4e58abdaa0f84c189a28b5fcd0a9ce50",
                "8615cc8655fd48f6b3d05c43caf71188",
                "e8c5be58b95041ca91a6840ccdf61862",
                "117620da4bb2438b8d3374e3a3b65253",
                "dc5d1dba3dc240fc99bb736012827a4d",
                "6d38bcdc9ded428a83b07a38e461fd9d",
                "57fbb4975e5d4260afb548df460ebb01",
                "afc937b91d374f46a5199b557aab1e50",
                "87284b6fb78d4480bdcb0a60655f3e27",
                "46d5bd3bb65546c9ae6bafb163fc3149",
                "8660d21a540047fdbcc34e00cd500425",
                "e914fcf2393d4448804a150459a1c743",
                "6fff8655ac944999a55421acdd1b8da1",
                "943205df5def4873b0862bb8597410d3",
                "12198e01f3944b6e9f7273f43c38c1a7",
                "1d3fb9b6e47741c0b8f33b6a437a0051",
                "3755221afb844e5aabc1a6059dcc6b43",
                "2d391eac8e134ec0b984e232983971f1",
                "9d18d08108f941f5bab4ad3f92b1af47",
                "f94648ffe7f04796a547a8d28411d20c",
                "9830eec6b9c54d68b86632a9ab438717",
                "17264cee23844c63b32a4077f483fcea",
                "ca097485f5204a9590edde16305aca9f",
                "d58c9b0ebe2f4c04bbd278e63e5b6521",
                "ad9107250bc64f7082fcff5626e60db7",
                "375dfe324c1345d484c3799bc03191eb",
                "693944f783924f07b4b4f1250b4365ae"
            ],
            "examId": "8361f1c8cf34417e8333f281ffe93f41",
            "programType": "sap",
            "custom": false
        },
        "lastUpdated": "2014-02-04T16:18:51+0000"
    },
    {
        "userId": "be5a7d28e9cd4166a5302dd38f342f15",
        "status": "abandoned",
        "dateCreated": "2014-02-03T21:53:34+0000",
        "id": "2817e53063de41d9ac4833627a8e020d",
        "version": 32,
        "attributes": {
            "title": "PSSAP IV",
            "elapsedSeconds": 1143,
            "score": "20/60",
            "questions": [
                "c3a491cd08694a1abf982634853a79f3",
                "a69453cfb7ac43bb86da0006e4d34e04",
                "92c01e3dfd0c4645b326bd2f1eb9aa5e",
                "932530f6e7f74d72952f994cdf624461",
                "85230191e85b4f5c9e526292530fdef1",
                "35997405f7ae4654aaf02efdf5a3e814",
                "a9fdc511271342fb85568b22d70f0db8",
                "967423a1debc4f379750623689cf42f8",
                "169bf703b98b415eb53baad1afe4106c",
                "bc4d87d27b524844aeacca603d1bb291",
                "281fe2cca4984aecaf73b4d4ca36ce04",
                "71252f88261b4319892f5fc5e4f32e94",
                "c41db0ee8c1a4927a85a347662c7d71e",
                "4a40e084d68d4fbca89e205a76f3e6dd",
                "93cd90bd76fe4062b0528a44d224e4ec",
                "14263d94ee64439694041624ff47d404",
                "842dfb18c62544e687e415d1d9a1094a",
                "f8a2a9bb9dfd4e9cbac4728b590a2ba2",
                "5f78ad3ab0254cc981af64385b5de827",
                "19f7003848824ee2895808486dbc4cd3",
                "6d998ae2764648c8bee07b4eb99c55dc",
                "3fad67569cc94bd0a6cc3d4fbe9c5b20",
                "b97c59c90af140839bed6bc642fa4b2a",
                "cb2a8f80932a416a83b35030b50ae016",
                "8691100082a842ab83c3879d491b0f8d",
                "4b7eb41e3b75484484a5a9e22a31d0dc",
                "82642bcd26ae47e39acb3dedd4436d60",
                "075770cb510b48a28ac1fab542c0a2a4",
                "6bd01659940c4121afa100beb22544e9",
                "f0c7bb1fcd6446cebdc7cf55d9380d85",
                "18f182e1df1248ca8a7ce856dd77fcfa",
                "c647db6bc4294f1a8610c444f5f36cf2",
                "cb81999198c24cfabbad0233791e11e2",
                "10c99ea4140f45acb096c190692b5de9",
                "bceef2753df14e1ca1b46a2fd8e28c6a",
                "e1aaf3bc0b7e4984a696475dcdf11397",
                "0a2159140ffe4e36962a35fb385868ce",
                "365c7677d28c434899e7ba7d9f7caa93",
                "4f5028e59f6f4a56878f2bcd731bafd5",
                "eb33ed67bcd94d9cb319f7c6671f1fcb",
                "5c9c70997de5486f91646091afaf3909",
                "6cf554a0cfba4292b590aa46d6de6d98",
                "c415eac47c3a419bbbbed60a716b3056",
                "5ff46f3b4ccd4456ad804df5d26ac546",
                "6bbfd9d68b8e4b03a133012b42f0cec2",
                "364ccbad5d0c4c6a9d565de5bcec6824",
                "abf419df2eba4b098573e98b9073aece",
                "d1117bbf54704952a71be5080977544d",
                "04a9fadaa9d941fab6fc8fb7855390ad",
                "380c00ec65a34b3e8be8230cb71e81a5",
                "111850cb269f4c2badae0fb04d35d727",
                "70d2e3da92cc48078e9de1939a1322c5",
                "1ff9a3f318ca46f38d40dcdc3a555f2c",
                "8477ce5cd69843008147100cc019d319",
                "49c1fa2e63e247249e047384ec854391",
                "176677777d4f4fd1bb3bf8a64336cd2f",
                "324edfecfcfb417f9527b7eea1b60ec2",
                "b032be9511ef4070a23bf261c5378eda",
                "42eba8d7cdf34b4da99ab9b657cdf3c5",
                "0b034f22ddb2409fb24c35b3e5ca0924"
            ],
            "examId": "c7cf789dafbc4b2480178bd1b7125230",
            "programType": "sap",
            "custom": false
        },
        "lastUpdated": "2014-02-04T16:18:41+0000"
    },
    {
        "userId": "be5a7d28e9cd4166a5302dd38f342f15",
        "status": "closed",
        "dateCreated": "2014-01-31T17:00:47+0000",
        "id": "c2690ab6c31541fb987a48d130b14903",
        "version": 82,
        "attributes": {
            "feedback": [
                {
                    "answerGiven": "5"
                },
                {
                    "answerGiven": "5"
                },
                {
                    "answerGiven": "5"
                },
                {
                    "extraFeedbackGiven": ""
                }
            ],
            "title": "PSSAP III",
            "elapsedSeconds": 2488,
            "score": "66.6",
            "questions": [
                "a50c6fc1da9a41cfaa92398f41f914c4",
                "161b13b46ff84bebad30a854c126bfc7",
                "25479b22928c4d7c89f30e9ca99783e0",
                "fc439dc9f93a48c2a797329264ef852d",
                "e2d7bc1c99de4420a7bdb888d1a13ef1",
                "aaf6e1eae9864fea9cedabc8054b1ef0",
                "bd18562c40264befa258c951edafa244",
                "0ae4f6fd8e114eaba1a94b9945f79b8d",
                "bcd1c074a9454d66ac4ee0cdb6aefd87",
                "c30fc4609f834b9fbee9b21573996522",
                "90514eda88db4e5a943065607fd1c7f0",
                "d6dfa08434574731b6b1efbb5bd19850",
                "ef4e5140d03f44aea6f3345fafdfddb1",
                "f903cff87c41405c925fb27e68f0ee5d",
                "0ce8643386c445b98cbc5c6dcaf90efc",
                "8bf62d08c8e6484da54f2979bec0db06",
                "46c8127358a748388280e10a5439bbf6",
                "98643865dfdc4ed88077ce1980a2f173",
                "41c94e9cf25743d4954abad7376ac79b",
                "7a100cd6cbe8472595a7991bcae5b743",
                "af8ede119da84d448b6892e442edec86",
                "63d1087741904573bc7043b0173ff1c5",
                "a20179b203114f2ea4c8da0e053370ee",
                "56f89d496ab740a3be0852576e34fae1",
                "03743474f0ab4f1eb32dcf94785282d2",
                "030928ab7fd54b409dc827e114704947",
                "82ba74b3d9a4413591fdf2bec7b16c87",
                "1f0b578163104d7a8383d9f8658288d6",
                "fd37c08d9f514f118575a2d095202c4a",
                "ead1be60915042f3a7ca86ae0e919d71",
                "105535a73df64956b2f66598a2fbe9fb",
                "f5fb36a2325f416a8fb33c5436720ed3",
                "0866eb2dc62141dcbc5e88e293d0c9a6",
                "0e8e7f95b9ca4eab8ca54a5ec9dcc0d0",
                "afb15ea7fe784d0a9698490fc72223b2",
                "8390b13d2f3041a18c2dfef85f45b33b",
                "839b194d8d794e408db7721cfbdb7ccd",
                "0819bac01f5040abae8777fdf56f2005",
                "961d3f14b24b4a0fbd70fc7b0bd609b7",
                "477fe81b8da543eab598f488fa10d80f",
                "76d1d0b851244380b07cc02f2514fc1f",
                "714855842cda4cbaa24930a9a4b4a415",
                "1ecb82718b8d4bbcb7be0cbf436da960",
                "ca20c53b83ec4041bead045f4707df11",
                "dd057ea7f9d14d7c8df380dd005bf8c6",
                "e2af5965fe6542ad9f99ba0f3cfe84a4",
                "959e1251fb4347298c6ff52a3b527fa7",
                "30a098b2dca64e30a57f1e387b484df9",
                "9c5f1aceafaa4baab48ef08d44cb2693",
                "4ffe7c6acf224cc38a91405272408877",
                "03f97eae392f46238a1ba8081b4cf602",
                "4d3f36076425474e9be309fa8422f922",
                "b62ca3e4a3824e82a1c0600de936f70c",
                "d14c4902485146b0b4edf6175563d5cc",
                "4447a74a96ce4ae3aaa2c2fadd88f816",
                "d01fdd4df6c44307a08d1a4482a6dc4a",
                "55f495be96cf4aabaa4d9d82b6695777",
                "9ed9e6a9430446ae81e733a5a8c3de02",
                "ad032d1bd7134b1f84ce967946f5cb16",
                "441fb75c32084e99bdf75944ca207a20",
                "679d9c604109435492300247b48fa9c7",
                "51da2f3fa99d49b1af70588b527567bb",
                "e7d41392dd29498abc04d1c5fd619620",
                "4203febf5f1d425fb52f70e23db35782",
                "7edb66e7d513413abf66ed5f3e822a81",
                "cc5b02d056fd49d8ba53af766a734777",
                "c424e47f021841049a1445119b757e58",
                "774f624e3c314db097e73e146551ebc6",
                "bafd57defc824fad8cc9c2969960d6c3"
            ],
            "examId": "8439a3532e534013b7b043940f72496c",
            "programType": "sap",
            "custom": false,
            "dateCompleted": "2014-02-03T19:24:02.219Z"
        },
        "lastUpdated": "2014-02-03T19:23:51+0000"
    },
    {
        "userId": "be5a7d28e9cd4166a5302dd38f342f15",
        "status": "closed",
        "dateCreated": "2014-01-31T16:53:22+0000",
        "id": "a8317f72a1194f4b97472ec00b4758cd",
        "version": 9,
        "attributes": {
            "feedback": [
                {
                    "answerGiven": "5"
                },
                {
                    "answerGiven": "5"
                },
                {
                    "answerGiven": "5"
                },
                {
                    "extraFeedbackGiven": ""
                }
            ],
            "title": "Random Course, 31 Jan  2014 11:53 ",
            "elapsedSeconds": 410,
            "score": "50",
            "questions": [
                {
                    "id": "3ec2f3992f6043439a085b6aca3d496c",
                    "response": {
                        "questionId": "3ec2f3992f6043439a085b6aca3d496c",
                        "count": null,
                        "status": "incorrect",
                        "lastUpdated": "2014-01-31T16:53:49+0000",
                        "parent": {
                            "id": "5fa52135add74b81adfc80665a37629f",
                            "dateCreated": "2014-01-31T16:53:49+0000",
                            "lastUpdated": "2014-01-31T16:53:49+0000",
                            "version": 0
                        },
                        "firstResponse": true,
                        "version": 0,
                        "id": "5fa52135add74b81adfc80665a37629f",
                        "session": "a8317f72a1194f4b97472ec00b4758cd",
                        "userId": "be5a7d28e9cd4166a5302dd38f342f15",
                        "dateCreated": "2014-01-31T16:53:49+0000",
                        "claimed": false,
                        "attributes": {
                            "claimed": false,
                            "answerGiven": "1",
                            "firstResponse": true
                        },
                        "answerGiven": "1"
                    }
                },
                {
                    "id": "066709d6042e4d7393d7bbac05f9d0ce",
                    "response": {
                        "questionId": "066709d6042e4d7393d7bbac05f9d0ce",
                        "count": null,
                        "status": "correct",
                        "lastUpdated": "2014-01-31T16:54:35+0000",
                        "parent": {
                            "id": "37b709a11b834c018174ccbb17a4a74e",
                            "dateCreated": "2014-01-31T16:54:35+0000",
                            "lastUpdated": "2014-01-31T16:54:35+0000",
                            "version": 0
                        },
                        "firstResponse": true,
                        "version": 0,
                        "id": "37b709a11b834c018174ccbb17a4a74e",
                        "session": "a8317f72a1194f4b97472ec00b4758cd",
                        "userId": "be5a7d28e9cd4166a5302dd38f342f15",
                        "dateCreated": "2014-01-31T16:54:35+0000",
                        "claimed": false,
                        "attributes": {
                            "claimed": false,
                            "answerGiven": "5",
                            "firstResponse": true
                        },
                        "answerGiven": "5"
                    }
                },
                {
                    "id": "f5fb36a2325f416a8fb33c5436720ed3",
                    "response": {
                        "questionId": "f5fb36a2325f416a8fb33c5436720ed3",
                        "count": null,
                        "status": "correct",
                        "lastUpdated": "2014-01-31T16:55:13+0000",
                        "parent": {
                            "id": "d00a82053db740d598aeb0fe47d76880",
                            "dateCreated": "2014-01-31T16:55:13+0000",
                            "lastUpdated": "2014-01-31T16:55:13+0000",
                            "version": 0
                        },
                        "firstResponse": true,
                        "version": 0,
                        "id": "d00a82053db740d598aeb0fe47d76880",
                        "session": "a8317f72a1194f4b97472ec00b4758cd",
                        "userId": "be5a7d28e9cd4166a5302dd38f342f15",
                        "dateCreated": "2014-01-31T16:55:13+0000",
                        "claimed": false,
                        "attributes": {
                            "claimed": false,
                            "answerGiven": "1",
                            "firstResponse": true
                        },
                        "answerGiven": "1"
                    }
                },
                {
                    "id": "69a2da40d8ae4ae8ae8adf012843d713",
                    "response": {
                        "questionId": "69a2da40d8ae4ae8ae8adf012843d713",
                        "count": null,
                        "status": "incorrect",
                        "lastUpdated": "2014-01-31T16:56:02+0000",
                        "parent": {
                            "id": "4c513a632f9e4e0cb2743ea5ccc1ec30",
                            "dateCreated": "2014-01-31T16:56:02+0000",
                            "lastUpdated": "2014-01-31T16:56:02+0000",
                            "version": 0
                        },
                        "firstResponse": true,
                        "version": 0,
                        "id": "4c513a632f9e4e0cb2743ea5ccc1ec30",
                        "session": "a8317f72a1194f4b97472ec00b4758cd",
                        "userId": "be5a7d28e9cd4166a5302dd38f342f15",
                        "dateCreated": "2014-01-31T16:56:02+0000",
                        "claimed": false,
                        "attributes": {
                            "claimed": false,
                            "answerGiven": "4",
                            "firstResponse": true
                        },
                        "answerGiven": "4"
                    }
                },
                {
                    "id": "b032be9511ef4070a23bf261c5378eda",
                    "response": {
                        "questionId": "b032be9511ef4070a23bf261c5378eda",
                        "count": null,
                        "status": "correct",
                        "lastUpdated": "2014-01-31T16:56:47+0000",
                        "parent": {
                            "id": "7af2d1690ed842fabfc8c9021fee0ae0",
                            "dateCreated": "2014-01-31T16:56:47+0000",
                            "lastUpdated": "2014-01-31T16:56:47+0000",
                            "version": 0
                        },
                        "firstResponse": true,
                        "version": 0,
                        "id": "7af2d1690ed842fabfc8c9021fee0ae0",
                        "session": "a8317f72a1194f4b97472ec00b4758cd",
                        "userId": "be5a7d28e9cd4166a5302dd38f342f15",
                        "dateCreated": "2014-01-31T16:56:47+0000",
                        "claimed": false,
                        "attributes": {
                            "claimed": false,
                            "answerGiven": "3",
                            "firstResponse": true
                        },
                        "answerGiven": "3"
                    }
                },
                {
                    "id": "d40ae1612d7c4034b408b3fa83638c15",
                    "response": {
                        "questionId": "d40ae1612d7c4034b408b3fa83638c15",
                        "count": null,
                        "status": "incorrect",
                        "lastUpdated": "2014-01-31T16:57:54+0000",
                        "parent": {
                            "id": "f9546701eb0e45c594a3b640784392b5",
                            "dateCreated": "2014-01-31T16:57:54+0000",
                            "lastUpdated": "2014-01-31T16:57:54+0000",
                            "version": 0
                        },
                        "firstResponse": true,
                        "version": 0,
                        "id": "f9546701eb0e45c594a3b640784392b5",
                        "session": "a8317f72a1194f4b97472ec00b4758cd",
                        "userId": "be5a7d28e9cd4166a5302dd38f342f15",
                        "dateCreated": "2014-01-31T16:57:54+0000",
                        "claimed": false,
                        "attributes": {
                            "claimed": false,
                            "answerGiven": "1",
                            "firstResponse": true
                        },
                        "answerGiven": "1"
                    }
                },
                {
                    "id": "8c0461b6774f443e90a542844919b3cd",
                    "response": {
                        "questionId": "8c0461b6774f443e90a542844919b3cd",
                        "count": null,
                        "status": "incorrect",
                        "lastUpdated": "2014-01-31T16:58:39+0000",
                        "parent": {
                            "id": "27f00de7c0544dc5bd8fa40f0ea01fda",
                            "dateCreated": "2014-01-31T16:58:39+0000",
                            "lastUpdated": "2014-01-31T16:58:39+0000",
                            "version": 0
                        },
                        "firstResponse": true,
                        "version": 0,
                        "id": "27f00de7c0544dc5bd8fa40f0ea01fda",
                        "session": "a8317f72a1194f4b97472ec00b4758cd",
                        "userId": "be5a7d28e9cd4166a5302dd38f342f15",
                        "dateCreated": "2014-01-31T16:58:39+0000",
                        "claimed": false,
                        "attributes": {
                            "claimed": false,
                            "answerGiven": "1",
                            "firstResponse": true
                        },
                        "answerGiven": "1"
                    }
                },
                {
                    "id": "e2d7bc1c99de4420a7bdb888d1a13ef1"
                }
            ],
            "examId": "Random Course, 31 Jan  2014 11:53",
            "programType": "custom",
            "custom": true,
            "dateCompleted": "2014-01-31T17:00:32.476Z"
        },
        "lastUpdated": "2014-01-31T17:00:30+0000"
    }
];
