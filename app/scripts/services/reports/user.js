/**
 * UserService
 *
 * @author: korzec
 * @created: 23/09/2014
 */
'use strict';

angular.module('PortalBrowserV2').factory('UserService',
    [
        'configuration',
        '$http',
        'sessionService',
        'GenericDataService',
        function (configuration, $http, sessionService, GenericDataService) {

            var UserService = function () {

                GenericDataService.apply(this, arguments);
                this.serviceUrl = configuration.dataHostURL + configuration.dataURI + '/userProxy';
                this.dataAttribute = 'users';
            };

            UserService.prototype = Object.create(GenericDataService.prototype);

            if (window.mockData) {
                UserService.prototype.mockData = window.mockData.users;
            }

            return UserService;

        }
    ]);

angular.module('PortalBrowserV2').service('userService',
    [
        'UserService',
        function (UserService) {
            return new UserService();
        }
    ]);