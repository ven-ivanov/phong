/**
 * report
 *
 * @author: korzec
 * @created: 23/09/2014
 */
'use strict';

angular.module('PortalBrowserV2').factory('UserPerformanceService',
    [
        'configuration',
        '$http',
        'sessionService',
        'GenericDataService',
        function (configuration, $http, sessionService, GenericDataService) {

            var UserPerformanceService = function () {

                GenericDataService.apply(this, arguments);
                this.serviceUrl = configuration.analyticsHostURL + configuration.analyticsURI + '/userPerformance';
                this.dataAttribute = 'userPerformance';
            };

            UserPerformanceService.prototype = Object.create(GenericDataService.prototype);

            if (window.mockData) {
                UserPerformanceService.prototype.mockData = window.mockData.userPerformance;
            }

            UserPerformanceService.prototype.getReport = UserPerformanceService.prototype.query;

            UserPerformanceService.prototype.getReportDownloadLink = function (params) {
                return this.serviceUrl + '/download?' + jQuery.param(params);
            };

            return UserPerformanceService;

        }
    ]);

angular.module('PortalBrowserV2').service('userPerformanceService',
    [
        'UserPerformanceService',
        function (UserPerformanceService) {
            return new UserPerformanceService();
        }
    ]);
