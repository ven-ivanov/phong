/**
 * UserExamService
 *
 * @author: korzec
 * @created: 23/09/2014
 */
'use strict';

angular.module('PortalBrowserV2').factory('UserExamService',
    [
        'configuration',
        '$http',
        'sessionService',
        'GenericDataService',
        function (configuration, $http, sessionService, GenericDataService) {

            var UserExamService = function () {

                GenericDataService.apply(this, arguments);
                this.serviceUrl = configuration.dataHostURL + configuration.dataURI + '/uxsessionProxy';
                this.dataAttribute = 'userExams';
            };

            UserExamService.prototype = Object.create(GenericDataService.prototype);

            if (window.mockData) {
                UserExamService.prototype.mockData = window.mockData.userExams;
            }

            return UserExamService;

        }
    ]);

angular.module('PortalBrowserV2').service('userExamService',
    [
        'UserExamService',
        function (UserExamService) {
            return new UserExamService();
        }
    ]);