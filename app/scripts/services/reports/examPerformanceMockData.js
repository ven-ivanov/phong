window.mockData = window.mockData || {};

window.mockData['examPerformance']   = {
        "clientName": "ProfMed",
        "editorName": "pam pom",
        "examTitle": "PSSAP V",
        "dateCourseCreated": "2014-01-22T10:09:11.000Z",
        "dateCourseReleased": "2014-01-22T10:09:26.000Z",
        "usersTaken": 17,
        "usersCompleted": 12,
        "certificatesClaimed": 171,
        "averageFirstScore": 70.41,
        "responsesData": [
            {
                "qId": "03ba389245e8443b8a4add3a14145a4e",
                "firstSixWords": "At laparotomy you find a through",
                "correctFirstAttempt": 88.31,
                "correctAnyAttempt": 90.25
            },
            {
                "qId": "05b35a19f9e7405f84f311f64e5b74eb",
                "firstSixWords": "A two-month-old infant undergoes a laparoscopic",
                "correctFirstAttempt": 89.40,
                "correctAnyAttempt": 90.06
            },
            {
                "qId": "05fbb4d33a8d4af7b96e5280e0b3bf7f",
                "firstSixWords": "A 12-year-old girl has a biopsy-proven",
                "correctFirstAttempt": 85.16,
                "correctAnyAttempt": 88.38
            },
            {
                "qId": "086efe5dfeae45309d6abc58441a43c5",
                "firstSixWords": "An ultrasound demonstrates a large loculated",
                "correctFirstAttempt": 88.53,
                "correctAnyAttempt": 89.17
            },
            {
                "qId": "0b34314bc44a4f36992f165c2ccf6f82",
                "firstSixWords": "After fluid resuscitation the next most",
                "correctFirstAttempt": 89.40,
                "correctAnyAttempt": 90.72
            },
            {
                "qId": "0beb0298d9e04866a760dffeaf34d5ee",
                "firstSixWords": "A 17-year-old boy undergoes an ileocolectomy",
                "correctFirstAttempt": 84.61,
                "correctAnyAttempt": 88.46
            },
            {
                "qId": "0cadea043f5d4b46ad2a60b7b28c9b48",
                "firstSixWords": "He remains stable following the injury",
                "correctFirstAttempt": 85.52,
                "correctAnyAttempt": 90.13
            },
            {
                "qId": "11c1b8d5acb947de8b1bd0e81744c915",
                "firstSixWords": "Which of the following is the",
                "correctFirstAttempt": 86.45,
                "correctAnyAttempt": 89.67
            },
            {
                "qId": "19a481a9796a41db8bfd41ccb09191ba",
                "firstSixWords": "Which of the following regarding foregut",
                "correctFirstAttempt": 89.40,
                "correctAnyAttempt": 90.72
            },
            {
                "qId": "1c5108e22adb4857813f75c17fa263a5",
                "firstSixWords": "A CT scan was obtained which",
                "correctFirstAttempt": 88.81,
                "correctAnyAttempt": 90.13
            },
            {
                "qId": "2460a512ce684afea422597a40ed300a",
                "firstSixWords": "You recommend total thyroidectomy and explain",
                "correctFirstAttempt": 88.15,
                "correctAnyAttempt": 90.13
            },
            {
                "qId": "269b558b50cb4495bad023ceba70383a",
                "firstSixWords": "Which of the following statements regarding",
                "correctFirstAttempt": 87.5,
                "correctAnyAttempt": 90.13
            },
            {
                "qId": "287a0c6152b640729977c2263d9d0f69",
                "firstSixWords": "For this patient with hypochloremic alkalosis,",
                "correctFirstAttempt": 86.84,
                "correctAnyAttempt": 89.47
            },
            {
                "qId": "2a1395c667134cb9bb82b321b912e63f",
                "firstSixWords": "The next best step in management",
                "correctFirstAttempt": 84.86,
                "correctAnyAttempt": 90.13
            },
            {
                "qId": "406296e0b1fb45d39357f6599dbe2978",
                "firstSixWords": "The next best step in management",
                "correctFirstAttempt": 88.0794701986755,
                "correctAnyAttempt": 90.06622516556291
            },
            {
                "qId": "40e127c6dc3e4af3830a9f6823143092",
                "firstSixWords": "After intubation and fluid resuscitation to",
                "correctFirstAttempt": 87.01,
                "correctAnyAttempt": 90.25
            },
            {
                "qId": "4517000e6fdd4846b941035785d309c8",
                "firstSixWords": "At surgery, a left mesocolic hernia",
                "correctFirstAttempt": 84.61,
                "correctAnyAttempt": 89.74
            },
            {
                "qId": "453ea751c2d742d68dd2fd3f771ac613",
                "firstSixWords": "Invasive hemodynamic monitoring reveals low systemic",
                "correctFirstAttempt": 86.45,
                "correctAnyAttempt": 89.67
            },
            {
                "qId": "46246766af384181bf0ccee353fb4189",
                "firstSixWords": "Which of the following statements regarding",
                "correctFirstAttempt": 88.74,
                "correctAnyAttempt": 90.72
            },
            {
                "qId": "4b40f2e284924f3eba086d06eba18253",
                "firstSixWords": "Which of the following statements regarding",
                "correctFirstAttempt": 88.07,
                "correctAnyAttempt": 90.72
            },
            {
                "qId": "4d078917529c45dcafbe1085ce084240",
                "firstSixWords": "Microlaryngoscopy reveals a laryngoesophageal interarytenoid cleft",
                "correctFirstAttempt": 87.74,
                "correctAnyAttempt": 89.67
            },
            {
                "qId": "580a542798ba4d65ac96a71ac8c5c9fe",
                "firstSixWords": "Which of the following statements about",
                "correctFirstAttempt": 85.33,
                "correctAnyAttempt": 90
            },
            {
                "qId": "640736120480455189e7582a6c8df4a1",
                "firstSixWords": "Which of the following regarding electrical",
                "correctFirstAttempt": 87.5,
                "correctAnyAttempt": 90.13
            },
            {
                "qId": "6486c904f102445393e7ca127ad299f4",
                "firstSixWords": "Recurrent upper GI bleeding following sclerotherapy",
                "correctFirstAttempt": 85.53,
                "correctAnyAttempt": 89.93
            },
            {
                "qId": "65214cb74a8d4a889b93f1da713731ed",
                "firstSixWords": "Which of the following statements about",
                "correctFirstAttempt": 88,
                "correctAnyAttempt": 90
            },
            {
                "qId": "690f0b92cafe4482b53c1482feed76a1",
                "firstSixWords": "Based on current information you can",
                "correctFirstAttempt": 88.07,
                "correctAnyAttempt": 90.72
            },
            {
                "qId": "6c1898b04b584788a5ce758b0e7a5002",
                "firstSixWords": "Which of the following is a",
                "correctFirstAttempt": 90.67,
                "correctAnyAttempt": 90.67
            },
            {
                "qId": "72043de98f414ee0b4f63987d1db523c",
                "firstSixWords": "The next best step in management",
                "correctFirstAttempt": 88.05,
                "correctAnyAttempt": 89.94
            },
            {
                "qId": "72f6a785e7d445248c1642add42c55a6",
                "firstSixWords": "Which of the following statements regarding",
                "correctFirstAttempt": 90.07,
                "correctAnyAttempt": 90.07
            },
            {
                "qId": "7394b7f35c9d4f49aef74599b3af573c",
                "firstSixWords": "The patient stabilized with ECMO but",
                "correctFirstAttempt": 88.07,
                "correctAnyAttempt": 90.06
            },
            {
                "qId": "75e07e1b964f4c498ee56146aa532dfd",
                "firstSixWords": "After intravenous fluid resuscitation and nasogastric",
                "correctFirstAttempt": 88.81,
                "correctAnyAttempt": 90.13
            },
            {
                "qId": "788386b56c5242aeadb7f450da2bd805",
                "firstSixWords": "Which of the following regarding the",
                "correctFirstAttempt": 87.82,
                "correctAnyAttempt": 89.74
            },
            {
                "qId": "8355825e357046788bb9fcedc9131168",
                "firstSixWords": "An eight-day old, 32-week estimated gestational",
                "correctFirstAttempt": 85.25,
                "correctAnyAttempt": 89.74
            },
            {
                "qId": "8947bff250df49a2933b18fc1b2f9883",
                "firstSixWords": "The next best step in management",
                "correctFirstAttempt": 88.38,
                "correctAnyAttempt": 90.32
            },
            {
                "qId": "9343f91234304647af41b68fd946b82d",
                "firstSixWords": "The histopathology of NID is best",
                "correctFirstAttempt": 88.0794701986755,
                "correctAnyAttempt": 90.72847682119205
            },
            {
                "qId": "969180a8eed146e98702e8fc97f0ea8e",
                "firstSixWords": "There is a continuous air leak",
                "correctFirstAttempt": 89.40,
                "correctAnyAttempt": 90.72
            },
            {
                "qId": "9ecd75570a4e4469aa5430577342c736",
                "firstSixWords": "Which of the following regarding retroperitoneal",
                "correctFirstAttempt": 84.96,
                "correctAnyAttempt": 90.84
            },
            {
                "qId": "9f4e1ddd71ce4ce39727b2f2e6bf69bb",
                "firstSixWords": "The next best step in management",
                "correctFirstAttempt": 88.81,
                "correctAnyAttempt": 89.47
            },
            {
                "qId": "a2268a327bf6413d966d4cdb59a1c35f",
                "firstSixWords": "Optimal initial treatment for a large",
                "correctFirstAttempt": 90.32,
                "correctAnyAttempt": 90.32
            },
            {
                "qId": "a29ddced3d20440596c371a1ba0162ee",
                "firstSixWords": "A prenatal diagnosis of 21-hydroxylase deficiency",
                "correctFirstAttempt": 85.80,
                "correctAnyAttempt": 88.38
            },
            {
                "qId": "a3d72fb0de4c4c7cb4e44bb3e8559d3b",
                "firstSixWords": "Which of the following statements regarding",
                "correctFirstAttempt": 83.34,
                "correctAnyAttempt": 88.46
            },
            {
                "qId": "b60bfbcc1a7945ea989f1b9363e5a76e",
                "firstSixWords": "Which of the following statements regarding",
                "correctFirstAttempt": 88.15,
                "correctAnyAttempt": 90.78
            },
            {
                "qId": "bbdad397574a4d9abf94bf04ce017376",
                "firstSixWords": "Which of the following statements regarding",
                "correctFirstAttempt": 84.31,
                "correctAnyAttempt": 90.19
            },
            {
                "qId": "be2843d2073248198a14e736215a0c90",
                "firstSixWords": "Which of the following statements regarding",
                "correctFirstAttempt": 84.86,
                "correctAnyAttempt": 90.13
            },
            {
                "qId": "c222b32f0377424ebc3d91d32b14e79c",
                "firstSixWords": "For a stable one-hour-old newborn with",
                "correctFirstAttempt": 85.43,
                "correctAnyAttempt": 90.72
            },
            {
                "qId": "c42dc043b23a4154adfe94ee1546daa6",
                "firstSixWords": "Which of the following statements regarding",
                "correctFirstAttempt": 85.98,
                "correctAnyAttempt": 89.80
            },
            {
                "qId": "c46e57c6e4a34be5a06f2638735f2ab8",
                "firstSixWords": "Inhalation injury necessitates intubation and mechanical",
                "correctFirstAttempt": 87.42,
                "correctAnyAttempt": 88.67
            },
            {
                "qId": "c5726770674945beb8410af0562f8719",
                "firstSixWords": "The next best step in management",
                "correctFirstAttempt": 89.47,
                "correctAnyAttempt": 90.13
            },
            {
                "qId": "cab379a6b3d4403fa1a05f8b92a42efe",
                "firstSixWords": "The next best step in management",
                "correctFirstAttempt": 88.23,
                "correctAnyAttempt": 90.19
            },
            {
                "qId": "cc6bb3583fc04962ab48308e3d6c4a3f",
                "firstSixWords": "Which of the following statements regarding",
                "correctFirstAttempt": 88.07,
                "correctAnyAttempt": 90.06
            },
            {
                "qId": "d7ed3840a98541c7aaf2471ca3cfcbfe",
                "firstSixWords": "Which of the following statements regarding",
                "correctFirstAttempt": 86.45,
                "correctAnyAttempt": 89.67
            },
            {
                "qId": "d948801edc684a6cbebdb7d002127f95",
                "firstSixWords": "The best management of this retroperitoneal",
                "correctFirstAttempt": 84.76,
                "correctAnyAttempt": 90.06
            },
            {
                "qId": "d9e4698bdada43d08d60e2d76a6c49bd",
                "firstSixWords": "The next best step in management",
                "correctFirstAttempt": 86.84,
                "correctAnyAttempt": 90.13
            },
            {
                "qId": "e002aa94fc5c4fdea8751420efe9b064",
                "firstSixWords": "Which of the following statements regarding",
                "correctFirstAttempt": 88.81,
                "correctAnyAttempt": 90.13
            },
            {
                "qId": "e29c86e09dc84024884dca725c4ac8ef",
                "firstSixWords": "Esophagram suggests recurrence of distal narrowing",
                "correctFirstAttempt": 89.47,
                "correctAnyAttempt": 90.13
            },
            {
                "qId": "ea3052c2fdf74155977b0eb3cfb6cf42",
                "firstSixWords": "In the postoperative period, the infant&#x27;s",
                "correctFirstAttempt": 88.07,
                "correctAnyAttempt": 90.06
            },
            {
                "qId": "f1eb3b7b9fff4c40a2666605a3ea00ca",
                "firstSixWords": "Which of the following statements regarding",
                "correctFirstAttempt": 89.47,
                "correctAnyAttempt": 90.78
            },
            {
                "qId": "f8a328105f7644e88f01c930f6731012",
                "firstSixWords": "After intravenous hydration, correction of his",
                "correctFirstAttempt": 89.47,
                "correctAnyAttempt": 90.78
            },
            {
                "qId": "f8dd15559ff44de6890eaedebf4b534b",
                "firstSixWords": "However, your pathologist reports that the",
                "correctFirstAttempt": 86.27,
                "correctAnyAttempt": 89.54
            },
            {
                "qId": "fb0603a518c246f781ec91e118a8b65a",
                "firstSixWords": "A four-month-old infant boy with jaundice",
                "correctFirstAttempt": 84.31,
                "correctAnyAttempt": 89.54
            },
            {
                "qId": "fd005645b92e42fc9fed50060efa96d0",
                "firstSixWords": "For this patient with sickle cell",
                "correctFirstAttempt": 88.74,
                "correctAnyAttempt": 90.06
            }
        ]
};