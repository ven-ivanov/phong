'use strict';

/**
 * @ngdoc service
 * @name PortalBrowserV2.example
 * @description
 * # example
 * Service in the PortalBrowserV2.
 */
angular.module('PortalBrowserV2')
  .service('example', function () {
    // AngularJS will instantiate a singleton by calling "new" on this function
  });
