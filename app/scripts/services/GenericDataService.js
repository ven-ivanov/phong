/**
 * GenericDataService.js
 *
 * @author: korzec
 * @created: 25/09/2014
 */
'use strict';

angular.module('PortalBrowserV2').factory('GenericDataService', function (configuration, $log, $http, sessionService, $timeout) {

    var GenericDataService = function () {

        this.serviceUrl = configuration.dataHostURL + configuration.dataURI + '/generic';
        this.dataAttribute = 'generic';
    };

    GenericDataService.prototype.query = function (params, callback) {
        var self = this;

        var queryParams = _.extend({userSessionToken: sessionService.getUserSessionToken(), token: sessionService.getPortalToken()}, params);

        if (this.mockData) {
            return $timeout(function () {
                $log.info('mock http response data', self.dataAttribute);
                callback(null, self.mockData);
            }, 2000);
        }

        $http({
            method: 'GET',
            url: this.serviceUrl + '/query',
            params: queryParams
        }).success(function (data /*, status*/) {
                if (callback) {
                    callback(null, data[self.dataAttribute]);
                }
            }).error(function (data /*, status*/) {
                if (callback) {
                    callback('service error', data);
                }
            });
    };

    return GenericDataService;

});
