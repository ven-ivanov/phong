/**
 * authentication API
 *
 * @author: korzec
 * @created: 23/09/2014
 */
'use strict';

angular.module('PortalBrowserV2').factory('Authenticationv2',
    [
        'configuration',
        '$http',
        'GenericDataService',
        function (configuration, $http, GenericDataService) {

            var Authenticationv2 = function () {

                GenericDataService.apply(this, arguments);
                this.serviceUrl = configuration.dataHostURL + configuration.dataURI + '/registration'
                this.dataAttribute = 'user';
            };

            Authenticationv2.prototype.authenticate = function(params, callback) {
                var self = this;

                var requestData = _.cloneDeep(params);

                $http({
                    method: 'POST',
                    url: this.serviceUrl + '/authenticate',
                    data: requestData
                }).success(function(data /*, status*/ ) {
                    if (callback) {
                        callback(null, data[self.dataAttribute]);
                    }
                }).error(function(data /*, status*/ ) {
                    if (callback) {
                        callback('service error', data);
                    }
                });
            };

            return Authenticationv2;

        }
    ]);

angular.module('PortalBrowserV2').service('authenticationv2',
    [
        'Authenticationv2',
        function (Authenticationv2) {
            return new Authenticationv2();
        }
    ]);
