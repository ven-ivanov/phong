'use strict';

var app = angular.module('PortalBrowserV2');

/**
 * Service for constructing $resource URLs, parameters, methods, etc
 */
app.factory('ContributorResourceService', ['$resource' ,'$location',function ($resource, $location) {

    var API_PREFIX = 'http://'+ $location.host() + ':3000'; // '//88.80.131.131:3000';
    /**
     * Perform a query, expect array response
     * @TODO Fix this JSHint error and remove the ignore:line
     */
    var query = function (url, params) {  // jshint ignore:line
        return $resource(API_PREFIX + url).query(params).$promise;
    };

    /**
     * Get request, non-array expected
     */
    var getRequest = function (url, params) {
        return $resource(API_PREFIX + url ).get(params).$promise;
    };

    /**
     * General post request
     */
    var post = function (url, params, body, headers) {
        if (!headers) {
            return new ($resource(API_PREFIX + url, params))(body).$save();
        }
        // Specify put request with potential custom headers
        return new ($resource(API_PREFIX + url, params, {
            postWithHeaders: {method: 'POST', headers: headers}
        }))(body).$postWithHeaders();
    };

    /**
     * General PUT request
     * @TODO Fix this JSHint error and remove the ignore:line
     */
    var put = function (url, params, body, headers) { // jshint ignore:line
        // Specify put request with potential custom headers
        return new ($resource(API_PREFIX + url, params, {
            update: {method: 'PUT', headers: headers}
        }))(body).$update();
    };

    /**
     * Update request via POST
     */
 /*   var patch = function (url, params, body, headers) {
        return new ($resource(API_PREFIX + url, params, {
            update: {method: 'PATCH', headers: headers}
        }))(body).$update();
    };*/

    /**
     * Post with array body
     * @TODO Fix this JSHint error and remove the ignore:line
     */
    var postArray = function (url, params, body) { // jshint ignore:line
        return new ($resource(API_PREFIX + url, params, {
            postArray: {
                method: 'POST',
                transformRequest: function (data) {
                    var thisData = angular.copy(data), response = [];
                    // Iterate and send as array
                    angular.forEach(thisData, function (item) {
                        response.push(item);
                    });
                    return JSON.stringify(response);
                }
            }
        }))(body).$postArray();
    };

    /**
     * Initiate a delete request
     */
    /*var deleteRequest = function (url, params, headers) {
        return new ($resource(API_PREFIX + url, params, {
            doDelete: {method: 'DELETE', headers: headers}
        }))().$doDelete();
    };*/
    /**
     * ContributorResourceService
     */
    var Service = {
        // General resource object
        resource: {type: '', url: '', params: '', methods: ''},
        /**
         * Contributor Subview
         */
        contributor: {
            // Standard request URLs
            requests: {
                invitation: '/PortalServiceV2/invitation/save',
                registration: '/PortalServiceV2/registration/authenticate',
                emailDuplicate: '/PortalServiceV2/registration/unique/email',
                disClosure:'/PortalServiceV2/contributor/disclosure/:contributorId'
            },
            /**
             * create new staff
             * @param params
             */
            invite: function (params) {
                return post(Service.contributor.requests.invitation, {}, params);
            },
            register: function (params) {
                return post(Service.contributor.requests.registration, {}, params);
            },
            checkEmailDuplicate: function (params) {
                return getRequest(Service.contributor.requests.emailDuplicate, params );
            },
            disclosure:function(params){
                return getRequest(Service.contributor.requests.disClosure, params);
            },
            saveDisclosure: function(params,body){
              return post(Service.contributor.requests.disClosure+'/save',params,body);
            }
        }
    };
    return Service;
}]);
