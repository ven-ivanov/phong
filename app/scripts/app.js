// jshint ignore: start
'use strict';

angular
    .module('PortalBrowserV2', [
        'ngRoute',
        'ngResource',
        'ngSanitize',
        'angularMoment',
        'angular-momentjs',
        'angularLoad',
        'angularSpinner',
        'ui.bootstrap',
        'ui.utils',
        'ui.select',
        'ui.bootstrap.datetimepicker',
        'uiSwitch',
        'xeditable',
        'checklist-model',
        'generic.filters',
        'generic.alerts',
        'pretty-checkable'
    ])
    .provider('configuration', function () {
        // default values
        console.log(' Setup App Constants  ');

        var config = new Config();

        return {
            $get: function () {
                return config;
            }
        };
    })
    .run(function ($log, $location, $rootScope, $templateCache, angularLoad, configuration) {

        $rootScope.$on('$routeChangeStart', function (evt, next, current) {
            $log.info('routeChangeStart', current, next);
        });

        $rootScope.$on('$routeChangeSuccess', function (evt, next, current) {
            $log.info('routeChangeSuccess', current, next);
        });

    })
    .constant('_START_REQUEST_', '_START_REQUEST_')
    .constant('_END_REQUEST_', '_END_REQUEST_')
    .config(function ($routeProvider, configurationProvider) {

        var config = configurationProvider.$get();

        // v2 routes should be common to all configs
        // app should check access rights on route change
        // old route config
        $routeProvider
            .when('/', {
                templateUrl: 'views/ContributorInvitation.html',
                controller: 'ContributorInvitationController'
            })
            .when('/contributor/signup', {
                templateUrl: 'views/ContributorRegistration.html',
                controller: 'ContributorRegistrationController'
            })
            .when('/provider/signup', {
                templateUrl: 'views/ProviderRegistration.html',
                controller: 'ProviderRegistrationController'
            })
            .when('/contributor/disclosure', {
                templateUrl: 'views/ContributorDisclosure.html',
                controller: 'ContributorDisclosureController'
            })
            .when('/signinv2', {
                templateUrl: 'views/signinv2.html',
                controller: 'Signinv2Ctrl'
            })
            .when('/teamv2', {
                templateUrl: 'views/teamv2.html',
                controller: 'Teamv2Ctrl'
            })
            .when('/adminv2', {
                templateUrl: 'views/adminv2.html',
                controller: 'Adminv2Ctrl'
            })
            .when('/profilev2', {
                templateUrl: 'views/profilev2.html',
                controller: 'Profilev2Ctrl'
            })
            .otherwise({
                redirectTo: '/'
            });

    })
    // Declare an http interceptor that will signal the start and end of each request
    .config(['$httpProvider', function ($httpProvider) {
        var $http,
            interceptor = ['$q', '$injector', function ($q, $injector) {
                var notificationChannel;

                function success(response) {
                    // get $http via $injector because of circular dependency problem
                    $http = $http || $injector.get('$http');
                    // don't send notification until all requests are complete
                    if ($http.pendingRequests.length < 1) {
                        // get requestNotificationChannel via $injector because of circular dependency problem
                        notificationChannel = notificationChannel || $injector.get('requestNotificationChannel');
                        // send a notification requests are complete
                        notificationChannel.requestEnded();
                    }
                    return response;
                }

                function error(response) {
                    // get $http via $injector because of circular dependency problem
                    $http = $http || $injector.get('$http');
                    // don't send notification until all requests are complete
                    if ($http.pendingRequests.length < 1) {
                        // get requestNotificationChannel via $injector because of circular dependency problem
                        notificationChannel = notificationChannel || $injector.get('requestNotificationChannel');
                        // send a notification requests are complete
                        notificationChannel.requestEnded();
                    }
                    return $q.reject(response);
                }

                return function (promise) {
                    // get requestNotificationChannel via $injector because of circular dependency problem
                    notificationChannel = notificationChannel || $injector.get('requestNotificationChannel');
                    // send a notification requests are complete
                    notificationChannel.requestStarted();
                    return promise.then(success, error);
                }
            }];

        $httpProvider.interceptors.push(interceptor);
    }])
    .filter('startFrom', function () {
        return function (input, start) {
            start = +start; //parse to int
            return input.slice(start);
        }
    }).directive('showonhoverparent',
   function() {
      return {
         link : function(scope, element, attrs) {
            element.parent().bind('mouseenter', function() {
                element.show();
            });
            element.parent().bind('mouseleave', function() {
                 element.hide();
            });
       }
   };
})
// declare the notification pub/sub channel
    .factory('requestNotificationChannel', ['$rootScope', function($rootScope){
        // private notification messages
        var _START_REQUEST_ = '_START_REQUEST_';
        var _END_REQUEST_ = '_END_REQUEST_';

        // publish start request notification
        var requestStarted = function() {
            $rootScope.$broadcast(_START_REQUEST_);
        };
        // publish end request notification
        var requestEnded = function() {
            $rootScope.$broadcast(_END_REQUEST_);
        };
        // subscribe to start request notification
        var onRequestStarted = function($scope, handler){
            $scope.$on(_START_REQUEST_, function(event){
                handler();
            });
        };
        // subscribe to end request notification
        var onRequestEnded = function($scope, handler){
            $scope.$on(_END_REQUEST_, function(event){
                handler();
            });
        };

        return {
            requestStarted:  requestStarted,
            requestEnded: requestEnded,
            onRequestStarted: onRequestStarted,
            onRequestEnded: onRequestEnded
        };
    }]);
