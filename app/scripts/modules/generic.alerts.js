/*
 * allows to add alerts with timeouts and broadcasts new alerts through root scope
 * @author konrad.korzec@roundpoint.com
 * @created: 29/09/2014
 *
 * TODO: can add a html template here too
 */
//<div ng-controller="AlertsController" class="alerts" ng-cloak>
//    <div ng-repeat="alert in alerts" class="alert alert-{{alert.type}}" type="alert.type" close="closeAlert($index)">
//        {{alert.msg}}
//        <span href class="close" ng-click="closeAlert($index)"></span>
//    </div>
//    <div ng-hide="true">
//        <span ng-init="draftSaved='Draft saved'"></span>
//    </div>
//</div>
(function () {
    'use strict';
    var app = angular.module('generic.alerts', []);

    app.service('AlertsService', function ($rootScope, $timeout, $log) {
        var controller = 'AlertsService';
        $log.log(controller, 'starting');
        var service = this;
        this.list = [];
        this.defaultDelay = 10000;
        this.close = function (index) {
            service.list.splice(index, 1);
        };
        this.add = function (alert, timeout) {
            $log.log('AlertsService', 'showing alert', alert);
            service.list.push(alert);
            $rootScope.$broadcast('alert', alert);
            var delay;
            if (timeout !== 0) {
                delay = timeout || service.defaultDelay;
                $timeout(function () {
                    service.list.splice(service.list.indexOf(alert), 1);
                }, delay);
            }
        };
        this.success = function (msg, delay) {
            service.add({
                type: 'success',
                msg: msg
            }, delay);
        };
        this.error = function (msg, delay) {
            service.add({
                type: 'error',
                msg: msg
            }, delay);
        };
        this.warning = function (msg, delay) {
            service.add({
                type: 'warning',
                msg: msg
            }, delay);
        };
        return this;
    });

    app.controller('AlertsController', function ($scope, $log, $location, AlertsService) {
        var controller = 'AlertsController';
        $log.log(controller, 'starting');
        $scope.alerts = AlertsService.list;
        AlertsService.scope = $scope;
        // AlertsService.add({msg:'Application started', type:'success'}, 0);
        $log.log(controller, 'alerts', $scope.alerts);
        $scope.closeAlert = AlertsService.close;
        var updateAlerts = function () {
            $log.log(controller, 'alerts', AlertsService.list);
            if (!$scope.$$phase) { $scope.$apply(); }
        };
        $scope.$on('alert', updateAlerts);
        $scope.alert = function (message) {
            AlertsService.add({msg: message});
        };
        $scope.$on('draftSaved', function (event/*, data, statusText, xhr*/) {
            $log.log(controller, event.name);
            AlertsService.success($scope.draftSaved);
        });
    });
}());
