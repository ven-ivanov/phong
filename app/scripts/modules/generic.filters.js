/**
 * filters
 *
 * @author: korzec
 * @created: 25/09/2014
 */

(function () {
    'use strict';
    var app = angular.module('generic.filters', []);

    app.filter('startFrom', function () {
        return function (input, start) {
            if (!input) {
                return;
            }
            start = +start; //parse to int
            return input.slice(start);
        };
    });

    app.filter('toDate', function () {
        return function (input /*, other*/) {
            if (input) {
                return new Date(input.replace(/\s/, 'T'));
            } else {
                return input;
            }
        };
    });

    app.filter('numberOfPages', function () {
        return function (input, other) {
            if (input && input.length && other){
                return Math.ceil(input.length / other);
            } else {
                return 0;
            }
        };
    });

    app.filter('integer', function () {
        return function (input) {
            return parseInt(input);
        };
    });

    app.filter('inMonths', function () {
        return function (input, month) {

            var compare = new Date(Date.now() + 60 * 60 * 24 * 30.5 * 1000 * month);

            //return true if input is ahad in time, false if it falls into the period between now and <month> later
            return input < compare;

        };
    });
}());
